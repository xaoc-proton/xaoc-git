
if(process.argv[2] == undefined){ console.log('ERROR: invalid project name'); return; }

console.log('START install.g');

PROJECT = process.argv[2];
PROJECT_PORT_SFX = process.argv[3] || 100;
PROJECT_LINK = __dirname+'/../project/'+PROJECT+'/';

fs = require("fs");
async = require("async");
md5 = require('md5');
zip = require('adm-zip');
exec = require('child_process').exec;
moment = require("moment");
moment.locale('ru');

// переменные фронта (для предотвращения ошибок переиспользования функций)
window = {}; document = {}; $ = function(){return{off:function(){return this},on:function(){return this},one:function(){return this}}};
// структура ответов клиенту
OK = ()=>{}, ERR = ()=>{}, 
// async операции
aW = ()=>{}, aP = ()=>{}
// local router
R = ()=>{};

XAOC_LINK = __dirname+'/../';
SYS = require('../core/sys.js').init();

var w = [],
	prefix = require('../config.js').windows ? '*' : '"*"';

if(!fs.existsSync(PROJECT_LINK)){

	if(!fs.existsSync(__dirname+'/../project')) fs.mkdirSync(__dirname+'/../project');
	if(!fs.existsSync(__dirname+'/../project/__cache')) fs.mkdirSync(__dirname+'/../project/__cache');
	
	fs.mkdirSync(PROJECT_LINK);
	
	SYS.walkDir(__dirname+'/dummy').forEach((f)=>{
		if(f.dir){
			fs.mkdirSync(f.path.replace(__dirname+'/dummy', PROJECT_LINK));
		}else{
			fs.writeFileSync(f.path.replace(__dirname+'/dummy', PROJECT_LINK), fs.readFileSync(f.path).toString().replace(/__PROJECT_NAME__/g, PROJECT).replace(/__PROJECT_PORT__/g, PROJECT_PORT_SFX));
		}
	});
	
	w.push((cb)=>{
		exec(
			'chcp 65001 & '+
			'cd "'+PROJECT_LINK+'"'+' && '+
			'hg init'+' && '+
			'echo [auth] >> .hg/hgrc'+' && '+
			'echo x.prefix = '+prefix+' >> .hg/hgrc'+' && '+
			'echo x.username = '+XAOC_CONFIG.hgUser+' >> .hg/hgrc'+' && '+
			'echo x.password = '+XAOC_CONFIG.hgPass+' >> .hg/hgrc'+' && '+
			'echo [paths] >> .hg/hgrc'+' && '+
			'echo default = '+XAOC_CONFIG.hgPathX+PROJECT+' >> .hg/hgrc'+' && '+
			'echo [subpaths] >> .hg/hgrc '+' && '+
			'hg pull',
		(err, r, e)=>{
			if(err || e) console.log(err, e);
			cb();
		});
	});
	
	w.push((cb)=>{
		
		/*exec(
			'cd "'+PROJECT_LINK+'"'+' && '+
			'hg cat -r tip .hgsub',
		(err, r, e)=>{*/
		fs.readFile(PROJECT_LINK+'/.hgsub', (err, r)=>{
			
			var e = err;
			r = r.toString();

			if(err || e){
				console.log(err, e);
				cb();
			}else{
				
				var subrepo = r.split('/').filter(_=>_.indexOf(' = blocks')!=-1).map(_=>_.replace(' = blocks',''));
				var s = '', ss = '', sss = '';

				subrepo.forEach((sr)=>{
					
					fs.mkdirSync(PROJECT_LINK+'/blocks/'+sr);
					
					sss += ' && hg init blocks/'+sr;
					
					s += ' && cd "'+PROJECT_LINK+'/blocks/'+sr+'"'+' && '+
						'echo [auth] >> .hg/hgrc'+' && '+
						'echo x.prefix = '+prefix+' >> .hg/hgrc'+' && '+
						'echo x.username = '+XAOC_CONFIG.hgUser+' >> .hg/hgrc'+' && '+
						'echo x.password = '+XAOC_CONFIG.hgPass+' >> .hg/hgrc'+' && '+
						'echo [paths] >> .hg/hgrc'+' && '+
						'echo default = '+XAOC_CONFIG.hgPathXB+sr+' >> .hg/hgrc';
					
					ss += 'echo '+XAOC_CONFIG.hgPathX+PROJECT+'/blocks/'+sr+' = '+XAOC_CONFIG.hgPathXB+sr+' >> .hg/hgrc'+' && ';
				});
				
				exec(
					'chcp 65001 & cd "'+PROJECT_LINK+'"'+sss+s+' && cd "'+PROJECT_LINK+'" && '+ss+'hg update -r tip -C'+s, 
				(err, r, e)=>{
						
					if(err || e) console.log(err, e);
					
					cb();
				});
			}
		});
	});

}else{
	
	if((PROJECT_PORT_SFX+'').indexOf('--') != -1){
		
		var BLOCK = PROJECT_PORT_SFX.substr(2);
		
		if(!fs.existsSync(PROJECT_LINK+'/blocks/'+BLOCK)){
		
			w.push((cb)=>{		
				
				fs.mkdirSync(PROJECT_LINK+'/blocks/'+BLOCK);
				
				exec(
					'chcp 65001 & '+
					'cd "'+PROJECT_LINK+'"'+' && '+
					'hg init blocks/'+BLOCK+' && '+
					'echo blocks/'+BLOCK+' = blocks/'+BLOCK+' >> .hgsub'+' && '+
					'echo '+XAOC_CONFIG.hgPathX+PROJECT+'/blocks/'+BLOCK+' = '+XAOC_CONFIG.hgPathXB+BLOCK+' >> .hg/hgrc'+' && '+
					'cd "'+PROJECT_LINK+'/blocks/'+BLOCK+'"'+' && '+
					'echo [auth] >> .hg/hgrc'+' && '+
					'echo x.prefix = '+prefix+' >> .hg/hgrc'+' && '+
					'echo x.username = '+XAOC_CONFIG.hgUser+' >> .hg/hgrc'+' && '+
					'echo x.password = '+XAOC_CONFIG.hgPass+' >> .hg/hgrc'+' && '+
					'echo [paths] >> .hg/hgrc'+' && '+
					'echo default = '+XAOC_CONFIG.hgPathXB+BLOCK+' >> .hg/hgrc'+' && '+
					'cd "'+PROJECT_LINK+'"'+' && '+
					'hg -R blocks/'+BLOCK+' pull',
				(err, r, e)=>{
					
					if(err || e) console.log(err, e);
					
					exec(
						'cd "'+PROJECT_LINK+'"'+' && '+
						'hg -R blocks/'+BLOCK+' branch '+PROJECT+' && '+
						'hg -R blocks/'+BLOCK+' commit -m '+"'"+PROJECT+"'",
					(err, r, e)=>{
						
						if(err || e) console.log(err, e);
						
						cb();
					});
				});
			});		
		}
	}
}

CONFIG = require(PROJECT_LINK+'/config.js');

async.waterfall(w, ()=>{

	INSTALL = require(PROJECT_LINK+'/install/install.js');
	DB = require('../core/db.js');
	ROUTER = require('../core/process.js');
	require(PROJECT_LINK+'/custom.js');

	var config = JSON.parse(JSON.stringify( CONFIG.mysql ));
	delete config.database;
	var db = mysqlLib.createConnection(config);
	var sql = "CREATE DATABASE IF NOT EXISTS `"+PROJECT+"`; CREATE DATABASE IF NOT EXISTS `"+PROJECT+"_logs`; CREATE DATABASE IF NOT EXISTS `"+PROJECT+"_cache`;";

	db.query(sql, (err, data)=>{ db.destroy();
		
		if(err){
			console.log(err);
		}else{
			DB.getMysqlLogs((db)=>{
				sql = "CREATE TABLE IF NOT EXISTS `__access_web` (`id` int(11) NOT NULL AUTO_INCREMENT, `add_time` bigint(15) NOT NULL, `ip` varchar(30) NOT NULL, `url` varchar(1000) NOT NULL, `headers` text NOT NULL, `alert` varchar(50) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8; CREATE TABLE IF NOT EXISTS `__access_socket` (`id` int(11) NOT NULL AUTO_INCREMENT, `add_time` bigint(15) NOT NULL, `ip` varchar(30) NOT NULL, `msg` varchar(1000) NOT NULL, `headers` text NOT NULL, `user` varchar(50) NOT NULL, `auth` varchar(50) NOT NULL, `session` varchar(50) NOT NULL, `alert` varchar(50) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8; CREATE TABLE IF NOT EXISTS `__access_actions` (`id` int(11) NOT NULL AUTO_INCREMENT, `add_time` bigint(15) NOT NULL, `action` varchar(50) NOT NULL, `msg` varchar(1000) NOT NULL, `user` varchar(50) NOT NULL, `auth` varchar(50) NOT NULL, `session` varchar(50) NOT NULL, `alert` varchar(50) NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
				db.query(sql, (err, data)=>{
					console.log('END install.g');
					if(INSTALL && typeof INSTALL.init == 'function'){
						INSTALL.init();
					}
				});
			});
		}
	});
});