
XAOC_CONFIG = require('./../../config.js');

exports.mongo = {url: XAOC_CONFIG.mongo.url+PROJECT};

exports.mysql = Object.assign(JSON.parse(JSON.stringify(XAOC_CONFIG.mysql)), {database : PROJECT});
exports.mysqlLogs = Object.assign(JSON.parse(JSON.stringify(XAOC_CONFIG.mysql)), {database : PROJECT+'_logs'});
exports.mysqlCache = Object.assign(JSON.parse(JSON.stringify(XAOC_CONFIG.mysql)), {database : PROJECT+'_cache'});

//exports.redis = XAOC_CONFIG.redis;

exports.DEBUG = true;
exports.needlogin = true;
exports.guest = {auto: true, roles: [], access: {}};
exports.indexHTML = 'index.html';
exports.indexScript = "PROJECT = '"+PROJECT+"'; HTML_FORM = 'formMain';";

exports.server_url = 'http://localhost:3__PROJECT_PORT__';
exports.server_port = 3__PROJECT_PORT__;
exports.socket_port = 8__PROJECT_PORT__;