/* project local script (e.c. cron-script) */

customGlobal = function(data, callback){try{

	DB.getMongo((db)=>{
		
		var conn = {db: db, user: {key: DB.__content.contentUser+''}};
		
		conn.db.collection('__content')
		.findOne(DB.__content._id, {__custom: 1}, (err, __content)=>{	

			var w = [], context = {save: []};
			if(BLOCK.action) w = Object.keys(BLOCK.action)
				.map(_=>Object.assign({__path: _}, DB.requireFile('action', _)))
				.filter(_=>_.customGlobal)
				.sort((a,b)=>((b.zIndex||0) - (a.zIndex||0)))
				.map(_=>(cb)=>{
					console.log('customGlobal ::', _.info || _.__path);
					_.f.call(context, conn, {parent: __content}, {customGlobal: true}, (data)=>{
						if(data && data.err) console.log(data.err);
						cb();
					});
				});
			
			async.waterfall(w.concat([]), ()=>{
				DB.saveFields(conn, context.save, ()=>{
					if(callback) callback();
				});
			});		
		});
	});		
	
}catch(e){
	console.log(e);
	if(callback) callback();
}}

customLocal = function(data, callback){try{
	
	DB.getMongo((db)=>{
		
		var conn = {db: db, user: {key: DB.__content.contentUser+''}};
		
		conn.db.collection('__content')
		.findOne(DB.__content._id, {__custom: 1}, (err, __content)=>{	

			var w = [], context = {save: []};
			if(BLOCK.action) w = Object.keys(BLOCK.action)
				.map(_=>Object.assign({__path: _}, DB.requireFile('action', _)))
				.filter(_=>_.customLocal)
				.sort((a,b)=>((b.zIndex||0) - (a.zIndex||0)))
				.map(_=>(cb)=>{
					console.log('customLocal ::', _.info || _.__path);
					_.f.call(context, conn, {parent: __content}, {customLocal: true}, (data)=>{
						if(data && data.err) console.log(data.err);
						cb();
					});
				});
			
			async.waterfall(w.concat([]), ()=>{
				DB.saveFields(conn, context.save, ()=>{
					if(callback) callback();
				});
			});		
		});
	});

}catch(e){
	console.log(e);
	if(callback) callback();
}}

cronGlobal = function(data, callback){try{

	DB.getMongo((db)=>{
		
		var conn = {db: db, user: {key: DB.__content.contentUser+''}};
		
		conn.db.collection('__content')
		.findOne(DB.__content._id, {__cron: 1}, (err, __content)=>{	

			var w = [], context = {save: []};
			if(BLOCK.action) w = Object.keys(BLOCK.action)
				.map(_=>Object.assign({__path: _}, DB.requireFile('action', _)))
				.filter(_=>_.cronGlobal)
				.sort((a,b)=>((b.zIndex||0) - (a.zIndex||0)))
				.map(_=>(cb)=>{
					console.log('cronGlobal ::', _.info || _.__path);
					_.f.call(context, conn, {parent: __content}, {cronGlobal: true}, (data)=>{
						if(data && data.err) console.log(data.err);
						cb();
					});
				});
			
			async.waterfall(w.concat([]), ()=>{
				DB.saveFields(conn, context.save, ()=>{
					if(callback) callback();
				});
			});		
		});
	});		
	
}catch(e){
	console.log(e);
	if(callback) callback();
}}

cronLocal = function(data, callback){try{
	
	DB.getMongo((db)=>{
		
		var conn = {db: db, user: {key: DB.__content.contentUser+''}};
		
		conn.db.collection('__content')
		.findOne(DB.__content._id, {__cron: 1}, (err, __content)=>{	

			var w = [], context = {save: []};
			if(BLOCK.action) w = Object.keys(BLOCK.action)
				.map(_=>Object.assign({__path: _}, DB.requireFile('action', _)))
				.filter(_=>_.cronLocal)
				.sort((a,b)=>((b.zIndex||0) - (a.zIndex||0)))
				.map(_=>(cb)=>{
					console.log('cronLocal ::', _.info || _.__path);
					_.f.call(context, conn, {parent: __content}, {cronLocal: true}, (data)=>{
						if(data && data.err) console.log(data.err);
						cb();
					});
				});
			
			async.waterfall(w.concat([]), ()=>{
				DB.saveFields(conn, context.save, ()=>{
					if(callback) callback();
				});
			});		
		});
	});	

}catch(e){
	console.log(e);
	if(callback) callback();
}}