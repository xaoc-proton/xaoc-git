
require(PROJECT_LINK+'/custom.js');

function init(){
	
	console.log('START install.l');
	
	DB.getMongo((db) => {
		
		var conn = {db: db, processInstall: true};
		
		DB.getStaticMysql(conn, ['mysql', 'mysqlLogs'], ()=>{
		
			if(process.argv[3] && process.argv[3] == 'drop'){
					
				var drop = [];
				
				drop.push((cb)=>{ conn.db.dropDatabase(()=>{ cb() }) });
				drop.push((cb)=>{ DB.dropMysqlDB(()=>{ cb() }) });
				
				async.waterfall(drop, (err, res)=>{
					conn.db.close();
					console.log('END drop');
					process.argv[3] = undefined;
					DB.destroyStaticMysql(conn, ['mysql', 'mysqlLogs'], ()=>{
						init();
					});
				});
			}else{
				
				DB.prepareGlobalData(()=>{
					
					if(DB.__content.contentUser) conn.user = {
						key: DB.__content.contentUser+'', 
						roles: {},
						config: {},
					};
					
					var context = {save: []}
					
					conn.db.collection('__content').stats((err, data)=>{
						
						if(data && data.count){

							var prepareContent = [];

							prepareContent = prepareContent.concat(BLOCK.__install
							.sort((a,b)=>((b.req.zIndex||0) - (a.req.zIndex||0)))
							.map(_=>(cb)=>{
								console.log('prepareContent ::', _.req.info || _.__path);
								_.req.f.call(context, conn, {type: 'prepareContent'}, cb);
							}));
							
							async.waterfall(prepareContent, (err, res)=>{
								
								DB.saveFields(conn, context.save, ()=>{
									console.log('END prepareContent');
									process.exit();
								});
							});
						}else{
							
							var createContent = [], $contentSet = {roleGroups: {}};
							
							createContent.push((cb)=>{
								
								DB.addComplex(conn, {col: 'pp'}, [], {
									first_name: 'Системный', second_name: 'Пользователь',
								}, (pp)=>{
						
									DB.addComplex(conn, {col: 'user'}, {_id: pp._id, col: 'pp'}, {name: 'Системный'}, (user)=>{
										
										DB.__content.contentUser = user._id;
										$contentSet.contentUser = user._id;
										cb();
									});
								});
							});
							
							createContent = createContent.concat(BLOCK.__install
							.sort((a,b)=>((b.req.zIndex||0) - (a.req.zIndex||0)))
							.map(_=>(cb)=>{
								console.log('createContent ::', _.req.info || _.__path);
								_.req.f.call(context, conn, {type: 'createContent', $contentSet: $contentSet}, cb);
							}));
							
							async.waterfall(createContent, (err, res)=>{
								
								conn.db.collection('__content').insert($contentSet, (err)=>{
									
									if(err){
										console.log('__content insert error', err);
										process.exit();
									}else{
										DB.saveFields(conn, context.save, ()=>{
											console.log('END createContent');
											conn.db.close();
											DB.destroyStaticMysql(conn, ['mysql', 'mysqlLogs'], ()=>{
												init();
											});
										});
									}
								});
							});
						}
					});
				});
			}
			
		});
	});
}
exports.init = init;