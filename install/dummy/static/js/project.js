/* project local js-script */

window.labelType = 'placeholder';

$.notify.defaults( {style: 'help'} );
$.notify.addStyle('help', {
	html: "<div><span data-notify-text/></div>",
	classes: {
		base: {
			"color": "#018da8",
			"padding": "6px",
			"font-size": "14px",
			"border": "1px solid",
			"background-color": "black",
			"background-image": "url(/XAOC/images/clear-black-back.png)",
			"cursor": "pointer",
			"text-align": "left",
			"z-index": "9999",
		},
	}
});

function onReady(){
	if(!window.isMobile) if(window.innerWidth < 1000){
		$('body').addClass('isMobile');
	}else{
		$('body').removeClass('isMobile');
	}
}

window.select2_task = function(p){
	
	function s2_task (state) {
		if (!state.id) { return state.text; }
		var $state = $('<div class="select2_taskItem" style="background-image: url(static/img/iconTask_'+(state.element ? state.element.value.toLowerCase() : state.id)+'.png)">'+state.text+'</div>');
		return $state;
	};	
	
	p.templateResult = s2_task;
	p.templateSelection = s2_task;
	p.minimumResultsForSearch = Infinity;
	p.dropdownCssClass = 'select2_task';
}

window.select2_obj = function(p){
	
	function s2_obj (state) {
		if (!state.id) { return state.text; }
		var $state = $('<img src="'+state.icon+'"><div><div>'+state.text+'</div>'+(state.context?'<p>'+state.context+'</p></div>':''));
		return $state;
	};
	
	p.templateResult = s2_obj;
	p.templateSelection = s2_obj;
	p.minimumResultsForSearch = 3;
	p.dropdownCssClass = 'select2_obj';
}

window.select2_obj_inline = function(p){
	
	function select2_obj_inline (state) {
		if (!state.id) { return state.text; }
		var $state = $('<img src="'+state.icon+'"><div><div>'+state.text+'</div>'+(state.context?'<p>'+state.context+'</p></div>':''));
		return $state;
	};
	
	p.templateResult = select2_obj_inline;
	p.templateSelection = select2_obj_inline;
	p.minimumResultsForSearch = 3;
	p.dropdownCssClass = 'select2_obj_inline';
}

$(document).on('contextmenu', 'body', function(e){
	e.preventDefault();
});

$(document).on('contextmenu', '[help]', function(e){
	e.preventDefault();
	var help = $(this).attr('help');
	if(help[0] == '['){
		help = JSON.parse(help);
		if(help[0] == 'lst' && window.LST[help[1]]){
			help = window.LST[help[1]].filter(function(h){ return h.v == help[2]})[0].help;
		}
	}
	$(this).notify(help,  { position: $(this).attr('helpPosition') || "top", style: "help" });
});

function pmt(rate, nper, pv, fv, type) {
	if (!fv) fv = 0;
	if (!type) type = 0;

	if (rate == 0) return -(pv + fv)/nper;
	
	var pvif = Math.pow(1 + rate, nper);
	var pmt = rate / (pvif - 1) * -(pv * pvif + fv);

	if (type == 1) {
		pmt /= (1 + rate);
	};

	return pmt;
}

function addCloseEvent($p){

	function event(e){

		var $e = $(e.target);
		while( $e.length && $e[0] != $p[0] && !$e.hasClass('toggleCloseEvent')) $e = $e.parent();
		
		if($e[0] != $p[0] || $e.hasClass('toggleCloseEvent')){
			$p.removeClass('activeCloseEvent');
		}else{
			$(document).one('click', event);
		}
	}

	if(!$p.hasClass('activeCloseEvent')){
		$p.addClass('activeCloseEvent');
		setTimeout(function(){ $(document).one('click', event) }, 0);
	}
}

$(document).on('click', '.tutorial-link', function(e){
	var $link = $(this);
	var link = $link.attr('link');
	if(link){
		if($('#tutorial').attr('currentTutorial')){
			if($link.hasClass('tutorial-active')){ // это tutorial_hello
				window.setTutorialComplete({}, function(){
					$link.removeClass('tutorial-active');
					$link.trigger('click');
				});
			}else{
				$.notify('Необходимо закончить текущее обучение');
			}
		}else{
			wsSendCallback( {action: 'tutorial_status', link: link}, function(completeData){
				if(!$link.hasClass('no-delete')) $link.remove();
				locationQuery({form: 'formTutorial', container: 'subFormTutorial', history: false});
			});
		}
	}
});