
var page = require('webpage').create(), fs = require('fs'), system = require('system');

page.settings.javascriptEnabled = true;
page.onConsoleMessage = function(msg) {
  console.log(msg);
}
page.onLoadFinished = function() {
  setTimeout(function(){
	  fs.write(system.args[2], page.content, 'w');
	  phantom.exit();
  }, system.args[3]);
};
page.open(system.args[1], function(status) {
	page.evaluate(function() {});
});