
cluster = require('cluster');
fs = require("fs");
moment = require("moment");
moment.locale('ru');

	console.log(moment().format(), " :: ", "Process "+process.pid+" started");
	
	Error.stackTraceLimit = 20; // пока не будет более оптимальной системы трейса ошибок - лучше меньше не делать...
	
	// переменные фронта (для предотвращения ошибок переиспользования функций)
	window = {}; document = {}; $ = function(){return{off:function(){return this},on:function(){return this},one:function(){return this}}};
	// структура ответов клиенту
	OK = ()=>{}, ERR = ()=>{}, 
	// async операции
	aW = ()=>{}, aP = ()=>{}
	// local router
	R = ()=>{};
	
	if(process.argv[2] == undefined){ console.log('ERROR: invalid project name'); return; }
	PROJECT = process.argv[2];
	PROJECT_LINK = __dirname+'/project/'+PROJECT+'/';
	XAOC_LINK = __dirname+'';
	
	if(!fs.existsSync(__dirname+'/project/__cache/'+PROJECT)) fs.mkdirSync(__dirname+'/project/__cache/'+PROJECT);
	if(!fs.existsSync(__dirname+'/project/__cache/'+PROJECT+'/_auth')) fs.mkdirSync(__dirname+'/project/__cache/'+PROJECT+'/_auth');
	if(!fs.existsSync(__dirname+'/project/__cache/'+PROJECT+'/_sub')) fs.mkdirSync(__dirname+'/project/__cache/'+PROJECT+'/_sub');
	
	SERVER_START_TIME = Date.now();
	CONFIG = require(PROJECT_LINK+'config.js');
	
	// процессы мастера не ограничиваем (там нагрузку дает только инсталляция, которая контроллируется легче)
	CONFIG.mysqlLogs.connectionLimit = 1;
	CONFIG.mysqlCache.connectionLimit = 1;
	
	async = require("async");
	md5 = require('md5');
	express = require('express');
	helmet = require('helmet');
	proxy = require('http-proxy-middleware');
	bodyParser = require('body-parser');
	cookieParser = require('cookie-parser');
	busboy = require("connect-busboy");
	ws = require("ws");
	webPush = require('web-push');
	
	APP = express();
	APP.use(helmet());
	APP.use(bodyParser.urlencoded({extended: true}));
	APP.use(bodyParser.json({limit: 1000*1024}));
	APP.use(bodyParser.raw());
	APP.use(cookieParser());
	APP.use(busboy({limits: {fileSize: 10 * 1024 * 1024}}));
	
	function processReq(req, res){try{
		
		var p = req.url.split('?');
		
		function send(req, res, file){try{
			
			if(file.indexOf('.xml') != -1){
				res.setHeader('Content-Type', 'application/xml; charset=windows-1251');
			}
			res.sendFile(unescape(file), (err)=>{try{
				if(err){
					// без !res._header иногда появляется ошибка повторной отправки header (кто отправляет - не отловил)
					if(XAOC_CONFIG.underNginx !== true && !res._header) res.status(500).send('Путь не найден');
					DB.accessLog(req, false, 'notexist');
				}
			}catch(e){ console.log(e) }});
		}catch(e){ console.log(e) }}

		if(p[0] && p[0] != '/'){

			//if(p[0].substring(0,11) == '/forms/snapshot' && p[0].substr(-5) != '.html') p[0] = p[0].substring(11);
			
			if(p[0].indexOf('config.js')!=-1){
				res.status(500).send('Доступ к файлу запрещен');
				DB.accessLog(req, false, 'config');
			}else{

				if(p[0].substring(0,6) == '/XAOC/'){
					
					send(req, res, __dirname + '/libs' + p[0].substring(5));
				
				}else{
					if(req.cookies['xaoc_'+PROJECT+'_role'] && XAOC_CONFIG.customPath[req.cookies['xaoc_'+PROJECT+'_role']+p[0]]){
						
						send(req, res, PROJECT_LINK+XAOC_CONFIG.customPath[req.cookies['xaoc_'+PROJECT+'_role']+p[0]]);
					
					}else{

						if(
							XAOC_CONFIG.fileAccess.file.indexOf(p[0]) != -1 ||
							XAOC_CONFIG.fileAccess.dir.filter(_=>p[0].indexOf(_)==0).length
						){
							
							send(req, res, PROJECT_LINK + p[0]);
						
						}else{
							
							var path = p[0].split('/');
							
							if(path[1] && XAOC_CONFIG.customPath[path[1]]){
								
								send(req, res, XAOC_CONFIG.customPath[path[1]] + p[0].substring(path[1].length+1) );
							
							}else{
								
								//if(p[0].substring(0,6) == '/link/' && req.cookies['xaoc_'+PROJECT+'_session']){
								if(path[1] == 'link' && path.length == 4){
									
									//DB.session.get(req.cookies['xaoc_'+PROJECT+'_session'], p[0].replace('/link/','').split('.')[0], (err, file)=>{
									DB.session.get(path[2], path[3].split('.')[0], (err, file)=>{

										if(file && file.value){
											
											send(req, res, PROJECT_LINK+file.value);
										
										}else{
											res.status(500).send('Доступ к файлу запрещен');
											DB.accessLog(req, false, 'private');
										}
									});
								}else{
									res.status(500).send('Доступ к файлу запрещен');
									DB.accessLog(req, false, 'private');
								}
							}
						}
					}
				}
			}
		}else{
			
			if(p[0] == '/'){try{ // indexHTML

				var html = fs.readFileSync(PROJECT_LINK + CONFIG.indexHTML, 'utf8');

				if(CONFIG.indexScript) html = html.replace('</head>','<script>'+CONFIG.indexScript.toString()+'</script></head>');
				res.send(html);
				
			}catch(e){
				console.log(e);
				res.status(500).send('Путь не найден');
				DB.accessLog(req, false, 'notexist');				
			}}else{
				send(req, res, PROJECT_LINK + p[0]);
			}
			
			DB.accessLog(req);
		}
	}catch(e){
		console.log(e);
		res.status(500).send('error');
	}}
	
	function processWS(conn){

		conn.lastAction = {time: Date.now(), msg: ''};

		conn.on("error", function (err){
			console.log(moment().format(), " :: ", conn.xaoc.id, 'Connection error', err);
		});

		conn.on("close", function (code, reason){
			
			if(conn.user){
				console.log(moment().format(), " :: ", conn.xaoc.id, conn.user.session, 'Connection close', code, reason);
				
				var msg = {session: 'close'};
				
				if(CONFIG.DEBUG){
					if(CONFIG.DEBUG) delete require.cache[require.resolve('./core/process.js')];
					require('./core/process.js').route(conn, msg, ()=>{
						delete SUB.users[ conn.xaoc.id ];
						delete process.sessions[ conn.xaoc.id ];
					});
				}else{
					ROUTER.route(conn, msg, ()=>{
						delete SUB.users[ conn.xaoc.id ];
						delete process.sessions[conn.xaoc.id];
					});
				}
			}else if(conn.sw){ // service-worker ???
				console.log(moment().format(), " :: ", conn.xaoc.id, conn.sw, 'Connection close', code, reason);
			}else
			{
				if(conn.xaoc.id)
				{
					delete SUB.users[ conn.xaoc.id ];
					delete process.sessions[ conn.xaoc.id ];
				}
				// при перезапуске сервера, если были подключены клиенты, то это сработает для каждого с кодом 1001 (первое подключение клиента будет с последней ошибкой 1006)
				console.log(moment().format(), " :: ", conn.xaoc.id, 'Unknown connection close', code, reason, conn);
			}
		});

		conn.on("message", async function (msg) {
			
			// "дешевая" заглушка для тестирования блокирования критичного функционала
			if((!conn.user || conn.user.auth !== true) && conn.lastAction.msg == msg && conn.lastAction.time + 1000 > Date.now()){
				conn.sendText( {answer: {err: 'Слишком много запросов'}} );
			}else{
				if(msg == 'ping'){
					conn.sendText('pong');
					if(process.send) process.send({type: 'user', user: conn.user});
				}else{
					if(XAOC_CONFIG.debugConsole) console.log(moment().format(), " :: ", conn.xaoc.id, conn.user&&conn.user.session?conn.user.session:'-', msg.length < 500 ? msg : msg.substr(0, 500)+'...');
					
					const callback = function(data){
						conn.sendText(data);
					}
					
					const msgObj = JSON.parse(msg);
					const baseAction = ['save', 'show'].includes((msgObj||{}).action);
					
					if(CONFIG.DEBUG)
					{
						delete require.cache[require.resolve('./core/process.js')];
						
						if(msgObj.action != undefined && msgObj.code && !baseAction)
							callback( await require('./core/process.js').action.call(conn, msgObj.action, msgObj) );
						else
							require('./core/process.js').route(conn, msg, callback);
					}
					else
					{
						if(msgObj.action != undefined && msgObj.code && !baseAction)
							callback( await ROUTER.action.call(conn, msgObj.action, msgObj) );
						else
							ROUTER.route(conn, msg, callback);
					}
				}
			}
			conn.lastAction = {time: Date.now(), msg: msg};
		});
	}
	
	SYS = require('./core/sys.js').init();
	DB = require('./core/db.js');
	ROUTER = require('./core/process.js');
	require(PROJECT_LINK+'/custom.js');
		
	DB.prepareGlobalData(()=>{

		process.sessions = {};
		
		const w = [];
		const data = Object.assign(DB.createContext({
			user: {key: DB.__content.contentUser+''}
		}), {
			systemCall: true
		});
		
		w.push(cb=>{
			DB.session.dropAll();
			
			DB.getMysqlCache((db)=>{
				db.query("DROP TABLE IF EXISTS `sub`; CREATE TABLE IF NOT EXISTS `sub` ( `id` varchar(50) DEFAULT '', `key` varchar(200) DEFAULT '', `session` varchar(50) DEFAULT '', `field` varchar(10) DEFAULT '', `form` varchar(50) DEFAULT '', `sub` varchar(100) DEFAULT '', KEY `main` (`id`, `key`), KEY `field` (`session`, `field`), KEY `form` (`session`, `form`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8; ", ()=>
				{
					db.destroy();
					cb();
				});
			});
		});
		
		w.push(cb=>{ DB.getMongo(db=>
		{
			data.conn.db = db;
			
			data.conn.db.collection('__content')
			.findOne(DB.__content._id, {__custom: 1, __cron: 1}, (err, __content)=>
			{
				data.__content = __content;
				cb();
			});
		}) });
		
		// global-обработчики использовались, когда был cluster (формально, можно объединить с local-обработчиками)
		if(typeof customGlobal == 'function') w.push(cb=>{ customGlobal(data.conn, data, cb) });
		if(typeof cronGlobal == 'function') w.push(cb=>{ cronGlobal(data.conn, data, cb) });
		if(typeof customLocal == 'function') w.push(cb=>{ customLocal(data.conn, data, cb) });
		if(typeof cronLocal == 'function') w.push(cb=>{ cronLocal(data.conn, data, cb) });
		
		if(process.argv[3] && process.argv[3] == 'prodcache')
		{
			w.push(cb=>{
				
				CONFIG.DEBUG = true;
				CONFIG.prodcache = true;
				
				(async ()=>{
					await runProdcache(data);
					cb('prodcache ready');
				})();
			});
		}
		
		w.push(data.saveAll);
		
		async.waterfall(w, (err)=>{							
			
			if(err)
			{
				console.log(err);
				process.exit(); // в том числе закончит процесс для "prodcache ready"
			}
			
			APP.all('/*', function (req, res, next) { processReq(req, res, next) });
			
			var server = APP.listen(CONFIG.server_port, function () {
				console.log(moment().format(), " :: ", 'WEB-server start at :'+CONFIG.server_port);
			});
			var wsProxy = proxy('ws://localhost:'+CONFIG.socket_port+'/ws', {changeOrigin:true});
			APP.use(wsProxy);
			server.on('upgrade', wsProxy.upgrade);

			const wss = new ws.Server({ port: CONFIG.socket_port });
			wss.on('connection', function connection(conn)
			{
				// if(conn.sw){ // service-worker ???
				
				conn.xaoc = {id: ((Date.now() % 10000000000)+Math.random()).toString()};
				process.sessions[conn.xaoc.id] = conn;
				SUB.users[ conn.xaoc.id ] = {};
				
				conn.sendText = function(msg){
					conn.send( msg && typeof msg == 'object' ? JSON.stringify(msg) : msg);
				}
				processWS(conn);
			});
			console.log(moment().format(), " :: ", 'SOCKET-server start at :'+CONFIG.socket_port);
		});
		
		process.on('uncaughtException', (err, origin) => {
			lo("uncaughtException: err, origin", {err, origin});
		});
		process.on('unhandledRejection', (reason, promise) => {
			lo("unhandledRejection: reason, promise", {reason, promise});
			
			// TypeError: Cannot read property 'initial' of undefined at Object.self.findOne
			// - скорее всего вызвал this.findOne без await
			
		});		
		// пусть лежит, пока не придумал как использовать...
		// инициируется через throw new Error(...);
		// upd: почти придумал (через предварительное сохранение callback упавшей функции), но непонятно как сопоставить колбэки одной и той же функции, вызванной разными пользователями
		/*process.on('uncaughtException', (err, origin) => {
			console.log('uncaughtException err, origin', err, origin);
			process.exit();
		});
		process.on('rejectionHandled', (err, origin) => {
			console.log('rejectionHandled err, origin', err, origin);
			process.exit();
		});
		process.on('unhandledRejection', (err, origin) => {
			console.log('unhandledRejection err, origin', err, origin);
			process.exit();
		});*/
	});

function runProdcache(data){ return new Promise((resolve, reject)=>
{
	const roles = [''].concat(Object.values(LST['user~roles'].list.lst).filter(role=>role.v).map(role=>role.v));
	
	const w = [];
	
	for(var r in roles)
	{	
		const rr = roles[r], accessList = SYS.get(LST, 'user~roles.lst.'+rr+'.access') || {};
		
		function prepareFakeUser(conn)
		{
			conn.xaoc = {id: 'prodcache'};
			SUB.users[ conn.xaoc.id ] = {};
			conn.user = {roles: [rr], role: rr, access: {}, config: {}};
			for(var key in accessList) if(conn.user.access[key] == undefined) conn.user.access[key] = accessList[key];
			return conn;
		}
		
		(fs.readdirSync(PROJECT_LINK+'/forms')||[]).filter(_=>_.indexOf('form') == 0 && _.indexOf('.js') != -1)
		.concat(Object.keys(BLOCK.form)||[]).forEach(_=>{ w.push(cb=>{
			const u = prepareFakeUser(data.conn);
			ROUTER.route(u , {action: 'form', query: {form: _.replace('.js', '')}}, res=>{
				console.log(rr, '->', _, ' : ', res.err ? res.err : (res.answer || 'ready') );
				cb();
			});
		}) });
	}
	
	async.parallel(w, (err)=>{
		
		if(err) reject(err);
		
		if(CONFIG.readme){

			var sidebar = '';
			
			for(var r in BLOCK.__readme){try{
				
				var readme = '', f = r.replace(/\//g,'');
				let config = JSON.parse(fs.readFileSync(BLOCK.__readme[r].__path+'/config.json')).readme||{};
				delete BLOCK.__readme[r].__path;
				
				let title = config.title || r;
				let titleLink = (title||'').replace(/ /g, '-');
				
				sidebar += '* ['+title+']('+f+'#'+titleLink+')\r\n';
				readme += '## '+title+'\r\n\r\n';
				
				if(config.info) readme += '> '+config.info+'\r\n\r\n';
				if(config.todo){
					readme += '`План разработки:`\r\n';
					config.todo.forEach(_=>{ readme += '- ['+(_.ready?'x':' ')+'] '+_.text+'\r\n' });
					readme += '\r\n';
				}
				if(config.img) readme += '![](img/'+(config.img===true?(f+'.png'):config.img)+')\r\n\r\n';
				
				for(var rr in BLOCK.__readme[r]){
					
					let config = BLOCK.__readme[r][rr];
					let file = fs.readFileSync(config.__path).toString();
					let title = config.title || rr;
					let titleLink = (title||'').replace(/ /g, '-');
					
					sidebar += '	* ['+title+']('+f+'#'+titleLink+')\r\n';
					readme += '### '+title+'\r\n\r\n';
					
					if(config.img) readme += '![](img/'+config.img+')\r\n\r\n';
					
					(file.match( /(?<=\/\/\<rm)([\s\S]*?)(?=\/\/\>)/g )||[]).forEach(_=>{
						var k = _.indexOf('}>');
						var c = {};
						try{ c = JSON.parse(_.substr(0,k+1).trim()) }catch(e){}
						
						if(c.title) readme += '#### '+c.title+'\r\n';

						readme += '\r\n'+_.substr(k+2)+'\r\n';
						
					});
				}
				
				fs.writeFileSync(PROJECT_LINK+'/docs/'+f+'.md', readme);
			}catch(e){ console.log('BLOCK.__readme err', r, e) }}
			
			fs.writeFileSync(PROJECT_LINK+'/docs/_sidebar.md', sidebar);
		}
		
		resolve();
	});
})}