/*
clone xaoc
npm install
clone project
add config.js
add forms/cache
add upload/public
add upload/private
add __install/install
*/


### Инсталляция фреймворка

Шаг 1

```bash
hg clone https://bitbucket.org/VanesStalkers/xaoc
```

Шаг 2

```bash
npm install
```

Шаг 3 (создание файла конфига - config.js в корне папки фреймворка)

hgUser и hgPass необходимо заполнить своими учетными данными
```js
exports.hgUser = '';
exports.hgPass = '';
exports.hgPathX = 'https://bitbucket.org/VanesStalkers/xaoc_';
exports.hgPathXB = 'https://bitbucket.org/VanesStalkers/xblock_';

exports.mongo = {url: 'mongodb://127.0.0.1:27017/'};

exports.mysql = { host : '127.0.0.1', user : 'root', password : '', multipleStatements: true };
exports.pgsql = {user: 'postgres', password: 'postgres', host: '127.0.0.1', database: 'kkt'}

exports.redis = {host: '127.0.0.1', port: '6379'};

exports.debugCSS = true;
exports.debugConsole = true;
exports.enableCache = false;
exports.disableWorkerAutoRestart = false;

exports.winPhantom = true;
exports.enableTelegram = true;

exports.customPath = {};
exports.fileAccess = {
	dir: [
		'/static/',
		'/forms/cache/',
		'/upload/public/',
		'/docs/',
	],
	file: [
		'/favicon.ico',
		'/service-worker.js',
		'/robots.txt',
		'/sitemap.xml'
	],
};

exports.mapYandexApiKey = 'fd06b643-8f36-4d78-9597-c602130fec4e';

exports.dadata = {
	token: 'db7846a39163109392a710bd15d0204bf019c29e',
	key: 'bfdb936a852599bfeda6bd1cc08320db533526c6',
}
```

### Создание проекта

Далее везде **XXX** - название проекта


!> Логика инсталлятора предполагает проверку на наличие папки ./project/XXX Если в процессе инсталляции произошла ошибка, то для полноценного запуска процесса с самого начала перед выполнением команды install рекомендуется удалить папку ./project/XXX


```bash
node install/install.js XXX
```

Если у проекта есть файлы инсталляции, то нужно повторно проинсталлировать проект с параметром **drop**.

```bash
node install/install.js XXX drop
```

Принудительное создание кэш-файлов шаблонизатора

```bash
node server XXX prodcache
```

Запуск сервера

```bash
node server XXX
```

### Описание фреймворка

В основе фреймворка лежит идеология веб-платформы Meteor.JS (https://www.meteor.com). Поэтому если Вам не знакомы основные принципы работы с JavaScript full-stack платформами, то рекомендуется ознакомиться с документацией Meteor.JS (https://docs.meteor.com/#/full/).
Фреймворк был разработан для ускорения процесса прототипирования бизнес-систем и сокращения времени создания MVP. На эти задачи нацелены все главные особенности фреймворка:

#### Компиляция страниц-разделов системы на основе единственного файла шаблона верстки

!> Файл шаблона верстки - это файл с типом (в большинстве случаев) form или html, имеющий внутри себя параметр-функцию tpl, при вызове возвращающей js-объект, являющийся шаблоном для формирования верстки

Внутри одного файла могут быть описаны все компоненты страницы:
- верстка
- стили
- клиентские скрипты
- серверные скрипты (в том числе взаимодействие с БД)

Данной таблице соответствует примерно такая верстка:
<img width="122" src="https://bitbucket.org/VanesStalkers/xaoc/downloads/table_img.jpg" style="width: 100%; height: 200px;">
```js
exports.tpl = (_, d)=>{ return [

	_.if(_.editMode, ()=>[
		["div",{"class": "modal fade","role": "dialog","id": "carModal"},[
		
			_.c({name: 'tmp_obj_car', col: 'tmp_obj', config: {form: _.__.global.form}, process: {
				links: (cb)=>{ cb({
					tmp_obj_car: false,
					user: '__tmp_obj_car',
				})},
				tpl: (_, d)=>{ return [
					_.html('car~modal', _, d),
				]},
			}}),
		]],
	]),

	["div",{"class": "ibox"},[
		["div",{"class": "ibox-content"},[
			["div",{"class": "bPageFilters"},[
				["div",{"class": "row"},[
					["div",{"class": "col-xs-4"},[
						_.f({name: 'find', stype: 'filter', value: '', placeholder: "Номер"}),
					]],
					["div",{"class": "col-xs-1"},[
						['button', {text: 'Найти', class: 'btn btn-primary'}, [
							_.f({name: 'find_btn', type: 'action', front: {
								onClick: (_)=>{
									var filter = {};
									_.closest('.ibox').find('.filter-field').map(function(i,f){
										if(f.name == 'find_from' || f.name == 'find_to'){
											filter[f.name] = f.value.replace( /(\d{2})\.(\d{2})\.(\d{4})/, "$3-$2-$1");
										}else{
											filter[f.name] = f.value;
										} 
									});
									filter.force = true;
									reloadComplex(_.closest('.ibox').find('tbody'), filter);
								}
							}}),
						]],
					]],
					_.if(_.editMode, ()=>[
						["div",{"class": "col-xs-7 text-right"},[
							["div",{"class": "btn btn-primary","data-toggle": "modal","data-target": "#carModal"},[
								["span",{"text": "Новый автомобиль"}]
							]]
						]],
					]),
				]]
			]],
			["div",{"class": "table-responsive"},[
				["table",{id: "carTable", "class": "table table-hover table-striped table-bordered"},[
					["thead",{},[
						["tr",{},[
							["td",{},[
								["span",{"text": "Добавлен"}],
							]],
							["td",{},[
								["span",{"text": "Гос.номер"}],
							]],
							["td",{},[
								["span",{"text": "Тип"}],
							]],
							["td",{},[
								["span",{"text": "Статус"}],
							]],
							["td",{},[
								["span",{"text": "Класс брони"}],
							]],
							["td",{},[
								["span",{"text": "Топливо"}],
							]],
						]]
					]],
					["tbody",{class: '*css*', style:()=>{/*css
						.*css* .btn-lastitem {
							position: absolute;
							right: 20px;
							bottom: 10px;
							font-size: 0px;
						}
						.*css* .btn-lastitem:after {
							content: 'Показать следующий 10 записей';
							font-size: 16px;
						}
					css*/}},[
						_.c({name: 'car', add: false, config: d.config, filter: {l: -100, showOnButton: true}, process: {
							parentDataNotRequired: true,
							id: (__, code, callback)=>{
								
								var field = __.fields[code];
								var data = __.data[field.parent];

								var sql = "SELECT car__add_time.id _id FROM car__add_time";
								var escape = [], join = [], where = [];
								
								where.push("car__add_time.f > 0");

								if(field.filter.find){
									join.push("LEFT JOIN car__gosnum ON car__gosnum.id = car__add_time.id");
									where.push("car__gosnum.f LIKE ?");
									escape.push('%'+field.filter.find+'%');
								}
								
								if(join.length) sql += " "+join.join(' ');
								if(where.length) sql += " WHERE "+where.join(' AND ');

								sql += ' ORDER BY car__add_time.f';
								
								DB.selectWithFilter(__, code, callback, sql, escape);
							},
							tpl: (_, d)=>{ return [
								
								["tr",{class: 'h', query: '{"form":"car~main", "container":"formContent", "filter":{"id":"'+d._id+'"}}'},[
									["td",{},[
										_.f({name: 'add_time', type: 'datetime-', value: ''}),
									]],
									["td",{},[
										_.f({name: 'gosnum', type: 'text-', value: ''}),
									]],
									["td",{},[
										_.f({name: 'type', type: 'select--', lst: 'car~type', value: ''}),
									]],
									["td",{},[
										_.f({name: 'status', type: 'select--', lst: 'car~status', value: ''}),
									]],
									["td",{},[
										_.f({name: 'armor', type: 'select--', lst: 'car~armor', value: ''}),
									]],
									["td",{},[
										_.f({name: 'fuel', type: 'select--', lst: 'car~fuel', value: ''}),
									]],
								]],
							]},
						}}, {tag: 'tr'}),
					]]
				]]
			]],
			["hr"],
			["div",{"class": "row m-top-lg form-inline"},[
				
			]]
		]]
	]]
]} 
```

### Структура проекта

#### Структура папок проекта

```text
.
└── blocks
    └── __install // базовый компонент для первичной инсталляции и обновления проекта
        ├── files
		├── install
		├── update
        └── func~install.js
	└── __user // универсальный компонент для работы с пользователями (устанавливается по дефолту в каждый проект из xaoc\install\dummy)
        └── fields	
			├── html~login.js
			├── html~pswd.js
			└── config.json // наличие этого файла указывает на то, что папку можно рассматривать как отдельный компонент
	└── ...
└── forms // в эту папку можно складывать шаблоны, которые не относятся к какому-либо из компонентов
	└── cache // тут результаты компиляции шаблонов
└── actions // в эту папку можно складывать методы и функции, которые не относятся к какому-либо из компонентов
└── install
	└── install.js // файл инсталляции проекта
└── static
└── upload
	├── private // папка для сохранения файлов c параметром privy == true
	└── public // базовая папка для сохранения файлов
```

#### Структура компонентов

Список файлов компонента worker

```text
.
└── blocks
    └── worker
        ├── action~reg.js
		├── action~search.js
		├── config.json
		├── form~list.js
		├── form~list_cli.js
		├── form~main.js
		├── func~install.js
		├── html~modal.js
		├── html~table.js
		├── lst~type.js
		├── lst~workday.js
		├── tutorial~info.js
        └── lst	
			├── html~login.js
			├── html~pswd.js
			└── config.json
		└── print
			├── form~proxy.js
			├── form~permit.js
			├── config.json
			└── static
				└── img
```

При запуске сервера опрашиваются все компоненты и формируется route-map.

!> Обычно тип файла записан в названии файла в формате [тип]~[название].js, при этом в route-map тип подменяется на название компонента (ссылкой для action~search.js будет action: 'worker~search')

- action и func (аналог action с рядом различий)

```js

	// к action можно обращаться как с сервера через глобальный метод ROUTER.route

	ROUTER.route(conn, {action: 'visit~status', status: 'ready'}, ()=>{
		callback({status: 'ok', msg: 'Ключ добавлен', type: 'keys_update'});
	}, {parent: visit});
```
```js
	// так и с клиента, через одну из двух конструкций
	
	// при нажатии кнопки происходит вызов серверного метода print~chrome (как указано в параметре name)
	["button",{"type": "button","class": "btn btn-xs", text: 'Выгрузить в файл'},[
		_.f({name: 'print~chrome', type: 'action', front: {
			onAction: ($e)=>{
				$.notify('Началось формирование файла...', "info");
			},
		}}),
	]],
	
	["button",{"type": "button","class": "btn btn-xs", text: 'Выгрузить в файл'},[
		_.f({name: 'print', sfx: 'chrome', type: 'action', front: {
			onClick: ($e)=>{
				$e.attr('disabled', 'disabled');
				$.notify('Началось формирование файла...', "info");
				
				// метод вызывается явным образом через сокет wsSendCallback				
				wsSendCallback({action: 'print~chrome', code: $e.attr('code'), file: $e.find('> a').attr('href'), url: {form:"car~list_cli"}}, function(){
					$e.removeAttr('disabled');
				});
			},
		}}),
	]],
```

- form

```js

// Форма является самостоятельной страницей и может быть открыта в браузере по прямой ссылке 
// например, http://localhost:3100/#{"form":"worker~main"}
// также она может быть открыта в качестве второстепенной формы, например
// http://localhost:3100/#{"form":"formMain","subform":{"form":"worker~list_cli","container":"formContent"}}

// формы имеют свой метод access, описывающие логику доступа пользователя к форме

exports.access = (__, data, callback)=>SYS.requireFile('func', '__user~access').f(__, {
	allow: ['worker']
}, callback);

// формы имеют свой метод id, для определения корневой коллекции и _id элемента коллекции для отрисовки данных

exports.id = (__, code, callback)=>{
	__.fields[code].col = 'user';
	__.queryIds[code] = [__.user.key];
	callback();
}

// формы имеют свой метод tpl, для создания шаблона-верстки

exports.tpl = (_, d)=>{
	
	_.__.global.form = 'worker';

return [

	["div",{},[
		
		_.html('worker~table', _, Object.assign(d, {__config: {partnerLink: true}})),
	
	]],
]}

// формы могут иметь свои методы func, script и style для описания статического контента страницы (стилей и js-скриптов)

exports.func = ()=>{

}

exports.script = ()=>{

}

exports.style = ()=>{/*

*/}
```

```js
	
	// форма может быть вызвана явным образом через метод _.form внутри шаблона верстки любой другой формы
	
	['div', {id: 'formContent', class: 'role-'+_.__.user.role+' wrapper wrapper-content animated fadeIn *css*', style:()=>{/*css
		.*css* .tutorial-active, .*css* .el.tutorial-active .select2-selection {
			-webkit-animation: tutorial_active 1s infinite alternate;
			-moz-animation: tutorial_active 1s infinite alternate;
			-ms-animation: tutorial_active 1s infinite alternate;
			-o-animation: tutorial_active 1s infinite alternate;
			animation: tutorial_active 1s infinite alternate;
		}
		.*css*.role-monitor {
			position: fixed;
			z-index: 99999999999;
			background-color: #ccc;
			width: 90%;
			height: 90%;
			top: 5%;
			left: 5%;
		}
	css*/}}, [
  
		_.form({name: 'worker~list'}),
		
	]],
```

- html

```js

	// html - это шаблон верстки, который может быть импортирован методом _.html во внутрь любого другого шаблона верстки

	["div",{},[
		
		_.html('worker~table', _, Object.assign(d, {__config: {partnerLink: true}})),
	
	]],

```
```js

// шаблоны верстки обязательно имеют метод tpl
	
exports.tpl = (_, d)=>{ return [
	
	["div",{"class": "modal-dialog","role": "document"},[
		["div",{"class": "modal-content"},[
			["div",{"class": "modal-header"},[
				["button",{"type": "button","class": "close","data-dismiss": "modal","aria-label": "Close"},[
					["span",{"aria-hidden": "true"},[
						["i",{"class": "fas fa-times"}]
					]]
				]],
				["h4",{"class": "modal-title"},[
					["span",{"text": _.config.simple ? (d.edit?"Карточка контактного лица":"Создать контактное лицо") : (d.edit?"Карточка сотрудника":"Создать сотрудника")}]
				]]
			]],
			["div",{"class": "modal-body"},[
			]],
		]],
	]],
	
]};

// шаблоны верстки могут иметь свои методы func, script и style для описания статического контента страницы (стилей и js-скриптов)

exports.func = ()=>{

}

exports.script = ()=>{

}

exports.style = ()=>{/*

*/}
```

- lst

Отдельный типы файлов для описания статичных справочников:
- используется одновременно и на клиенте и на сервере (а меняется в одном месте)
- на сервере все справочники сохраняются в глобальный объект LST
- на клиенте импортируется файл, который создается в момент компиляции внутри компонента в папке lst

```js

// Обычно имеет тип multi, чтобы иметь возможность добавлять логику на этапе компиляции для создания разных справочников для сервера и клиента

exports.config = {
	multi: true,
}

exports.lst = ()=>{
	var lst = [
		{v: '', l: '\xa0'},
		{v: 'head', l: 'Руководитель'},
		{v: 'buh', l: 'Бухгалтер'},
		{v: 'manager', l: 'Менеджер'},
	];
}
```
скомпилированный файл импорта для браузера
```js
	var lst = [
		{v: '', l: '\xa0'},
		{v: 'head', l: 'Руководитель'},
		{v: 'buh', l: 'Бухгалтер'},
		{v: 'manager', l: 'Менеджер'},
	];
if(typeof exports != 'undefined') exports.lst = lst;
if(typeof window != 'undefined') window.LST['worker~type'] = lst;
```

JS-объекты 1-в-1 соответствуют HTML-тегам 

Преобразование js-объектов в html-теги (файл xaos.js, функция tplToHtml):
```js
	// oo - объект из js-верстки
	var $newparent = $('<'+oo[0]+' />', oo[1]);
```

Данный блок верстки
```js
	["div",{"class": "table-responsive"},[
		["table",{id: "carTable", "class": "table table-hover table-striped table-bordered"},[
			["thead",{},[
				["tr",{},[
				]],
			]],
		]],
	]],
```
соответствует приведенному ниже html-коду
```html
	<div class="table-responsive">
		<table id="carTable" class="table table-hover table-striped table-bordered">
			<thead>
				<tr>
				</tr>
			</thead>
		</table>
	</div>
```

Автоматические классы для CSS-стилей создаются через конструкцию
```js
	["tbody",{class: '*css*', style:()=>{/*css
		.*css* .btn-lastitem {
			position: absolute;
			right: 20px;
			bottom: 10px;
			font-size: 0px;
		}
		.*css* .btn-lastitem:after {
			content: 'Показать следующий 10 записей';
			font-size: 16px;
		}
	css*/}},[
	]],
```
которая после компиляции шаблона преобразуется в html-элемент и css-стили
```html
	<tbody class="_16_">
	</tbody>
```
```css
	._16_ .btn-lastitem {
		position: absolute;
		right: 20px;
		bottom: 10px;
		font-size: 0px;
	}
	._16_ .btn-lastitem:after {
		content: 'Показать следующий 10 записей';
		font-size: 16px;
	}
```

Объекту _ доступны методы для организации логики формирования шаблона верстки
```js
	
	// _.if - вызывает функцию из второго параметра, если первый параметр === true
	_.if(_.editMode, ()=>[

	// _.f - рисует элемент с одним из типов (полный список типов в функции elementToHtml в файл xaos.js)
	_.f({name: 'find', type: 'input'...}),
	
	// _.c - рисует вложенную сущность из коллекции [name]
	_.c({name: 'car'...}),
	
```

Клиентские скрипты добавляются в параметр front элементов и блоков, или в методе _.script
```js
	['button', {text: 'Найти', class: 'btn btn-primary'}, [
		_.f({name: 'find_btn', type: 'action', front: {
			onClick: (_)=>{
				var filter = {};
				_.closest('.ibox').find('.filter-field').map(function(i,f){
					if(f.name == 'find_from' || f.name == 'find_to'){
						filter[f.name] = f.value.replace( /(\d{2})\.(\d{2})\.(\d{4})/, "$3-$2-$1");
					}else{
						filter[f.name] = f.value;
					} 
				});
				filter.force = true;
				reloadComplex(_.closest('.ibox').find('tbody'), filter);
			}
		}}),
	]],
```
результатом компиляции блока выше является html-верстка и js-скрипт
```html
	<button class='btn btn-primary' onclick='f_76a69245ff521c1da9912d25200ab088'>
		Найти
	</button>
```
```js

// метод выполняется на клиенте и поэтому он может взаимодействовать с объектами JQuery и DOM-деревом страницы

window.f_76a69245ff521c1da9912d25200ab088 = function(_){
	var filter = {};
	_.closest('.ibox').find('.filter-field').map(function(i,f){
		if(f.name == 'find_from' || f.name == 'find_to'){
			filter[f.name] = f.value.replace( /(\d{2})\.(\d{2})\.(\d{4})/, "$3-$2-$1");
		}else{
			filter[f.name] = f.value;
		} 
	});
	filter.force = true;
	reloadComplex(_.closest('.ibox').find('tbody'), filter);
}
```

Серверные скрипты добавляются в параметр process элементов и блоков
Основным серверными методом для блоков являются метод id, который возвращает идентификаторы элементов коллекции, которые необходимо отрисовать на странице
```js

// в данном примере в методе id формируется SQL-запрос с учетом выбранных фильтров
// метод выполняется на сервере и поэтому он может взаимодействовать с объетом DB

_.c({name: 'car', add: false, config: d.config, filter: {l: -100, showOnButton: true}, process: {
	parentDataNotRequired: true,
	id: (__, code, callback)=>{
		
		var field = __.fields[code];
		var data = __.data[field.parent];

		var sql = "SELECT car__add_time.id _id FROM car__add_time";
		var escape = [], join = [], where = [];
		
		where.push("car__add_time.f > 0");

		if(field.filter.find){
			join.push("LEFT JOIN car__gosnum ON car__gosnum.id = car__add_time.id");
			where.push("car__gosnum.f LIKE ?");
			escape.push('%'+field.filter.find+'%');
		}
		
		if(join.length) sql += " "+join.join(' ');
		if(where.length) sql += " WHERE "+where.join(' AND ');

		sql += ' ORDER BY car__add_time.f';
		
		DB.selectWithFilter(__, code, callback, sql, escape);
	},
```


### Взаимодействие с БД

Фрейморк использует для работы одновременно две СУБД:
- MongoDB (для хранения данных)
- MysqlDB (для хранения таблиц индексов)

#### Создание Mysql-индексов для Mongo-сущностей

Обычно создание Mysql-индексов делается через режим prepareContent при запуске install-методов компонентов проекта.

Install-методы вызываются каждый раз при запуске через консоль команды.
```bash
node install/install.js XXX 		// в данном случае произойдет вызов метода с type = prepareContent
node install/install.js XXX drop	// в данном случае произойдет два последовательных вызов метода сначала с type = createContent, а затем с type = prepareContent
```
```js

// файл /blocks/worker/func~install.js

exports.install = true; // объявление install-метода

exports.f = function(conn, data, callback){
	
	switch(data.type){ // проверка на тип вызова

		case 'createContent': // вызов c 'drop' (обычно первичная инсталляция проекта)
			callback();
			break;
		
		case 'prepareContent': // вызов без 'drop' (апдейт проекта без сброса БД)
			
			DB.createColKeys(conn, {
				col: 'worker', 				// указываем сущность
				fields: [					// список полей
					'add_time|int', 		// через | указываем специальный тип (в данном случае тип поля в mysql)
					'code', 
					'delete_time|int'
				], 
				links: [					// список связанных сущностей
					'__client', 
					'__partner', 
					'__pp'
				], force: true}, (msg)=>{
				console.log(msg);
				callback();
			});
				
			break;
		
		default: callback(); break;
	}
}

```

Результат выполнения createColKeys из примера выше аналогичен вызову SQL-команд

```sql

CREATE TABLE `worker__add_time` (
  `id` varchar(24) NOT NULL,
  `f` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `worker__add_time` (`id`, `f`) VALUES ... // все значения всех элементов из коллекции worker в mongoDB

ALTER TABLE `worker__add_time`
  ADD PRIMARY KEY (`id`),
  ADD KEY `f` (`f`);
COMMIT;

CREATE TABLE `worker__code` (
  `id` varchar(24) NOT NULL,
  `f` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `worker__code` (`id`, `f`) VALUES ...

ALTER TABLE `worker__code`
  ADD PRIMARY KEY (`id`),
  ADD KEY `f` (`f`);
COMMIT;

CREATE TABLE `worker__delete_time` (
  `id` varchar(24) NOT NULL,
  `f` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `worker__delete_time` (`id`, `f`) VALUES ...

ALTER TABLE `worker__delete_time`
  ADD PRIMARY KEY (`id`),
  ADD KEY `f` (`f`);
COMMIT;

CREATE TABLE `worker_links__client` (
  `id` varchar(24) NOT NULL,
  `p` varchar(24) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `worker_links__client` (`id`, `p`) VALUES ... // все текущие связи между коллекциями worker и client в mongoDB

ALTER TABLE `worker_links__client`
  ADD KEY `id` (`id`),
  ADD KEY `p` (`p`);
COMMIT;

CREATE TABLE `worker_links__partner` (
  `id` varchar(24) NOT NULL,
  `p` varchar(24) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `worker_links__partner` (`id`, `p`) VALUES ...

ALTER TABLE `worker_links__partner`
  ADD KEY `id` (`id`),
  ADD KEY `p` (`p`);
COMMIT;

CREATE TABLE `worker_links__pp` (
  `id` varchar(24) NOT NULL,
  `p` varchar(24) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `worker_links__pp` (`id`, `p`) VALUES ...

ALTER TABLE `worker_links__pp`
  ADD KEY `id` (`id`),
  ADD KEY `p` (`p`);
COMMIT;

```

Для синхронизации данных между mysql и mongo используются методы addComplex, deleteComplex, saveField, saveFields, которые отвечают за все виды обновления сущностей в проекте.

#### Метод saveField

Вызывается каждый раз при изменении значения поля клиентом в интерфейсе

```js
	$(document).on('change', 'input, textarea, select', function(e) { // строка 452 в xaoc.js
		...
		wsSendCallback( extend({action: 'save'}, data), function(answer){}, function(answer){
```

Данный метод можно вызывать напрямую в серверных методах

```js
	DB.saveField(conn, field, parent, saveData, callback);

	DB.saveField(conn, {name: 'lastDay'}, {col: 'request_app', _id: _._id}, {value: checkDay}, ()=>{ cb() });
```

#### Метод saveFields

Синтаксический сахар для множественного использования saveField. Вызывается автоматически после любых (почти любых) серверных методов, вызывая saveField для каждого элемента из массива this.save
```js

// пример наполнения this.save

this.save.push({c: 'trip_item', n: 'code', id: res.item._id, v: res.stockItem.code});
this.save.push({c: 'trip_item', n: 'place', id: res.item._id, v: 'trip'});
if(ready) this.save.push({c: 'visit', n: 'pack_status', id: res.visit._id, v: 'ready'});
this.save.push({c: 'item', n: 'place', id: res.stockItem._id, v: 'trip'});
```

#### Метод addComplex

Вызывается каждый раз нажатии кнопки создания сущности клиентом в интерфейсе

```js
	$(document).on('click', '.complex-controls > .control-add', function() { // строка 608 в xaoc.js
		...
		wsSendCallback( extend({action: 'add'}, data), function(answer){
```

Данный метод можно вызывать напрямую в серверных методах

```js
	DB.addComplex(conn, field, parents, values, callback);

	DB.addComplex(conn, {col: 'trip_item', _id: res.item._id}, {col: 'item', _id: res.stockItem._id}, (trip_item)=>{ ...
	
	DB.addComplex(conn, {col: 'visit'}, [{_id: _._id, col: 'point', childLink: false}, {_id: msg.contract, col: 'contract', childLink: false}, res.request], _.visit, (visit)=>{ ...
```

#### Метод deleteComplex

Вызывается каждый раз нажатии кнопки удаления сущности клиентом в интерфейсе

```js
	$(document).on('click', '.item-controls > div.btn-delete', function() { // строка 668 в xaoc.js
		...
		wsSendCallback( extend({action: 'delete'}, data), function(data){
```

Данный метод можно вызывать напрямую в серверных методах

```js
	DB.deleteComplex(conn, field, parents, callback);

	DB.deleteComplex(conn, {_id: tmp_obj._id, col: 'tmp_obj'}, [{_id: conn.user.key, col: 'user'}], ()=>{ ...
```