
exports.redis = function(session, codes, callback){
	DB.redis.del((codes||[]).map(c=>session+'__'+c), ()=>{
		if(typeof callback == 'function') callback();
	});
}

exports.files = function(session, codes, callback){

	(codes||[]).forEach((c)=>{
		let path = PROJECT_LINK+'/../__cache/'+PROJECT+'/'+session+'/'+c+'.json';
		if(fs.existsSync(path)) fs.unlinkSync(path);
	});
	
	if(typeof callback == 'function') callback();
}