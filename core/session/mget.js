
exports.redis = function(session, codes, callback){
	
	DB.redis.mget(codes.map(c=>session+'__'+c), (err, data)=>{
		if(err){
			callback(err, []);
		}else{
			if(!data){
				callback(null, []);
			}else{
				callback(null, data.map((d)=>{try{ return JSON.parse(d) }catch(e){ console.log(e, d) }}));
			}
		}
	});
}

exports.files = function(session, codes, callback){
	
	var res = [];
	
	(codes||[]).forEach((code)=>{
		var path = PROJECT_LINK+'/../__cache/'+PROJECT+'/'+session+'/'+code+'.json';
		if(!fs.existsSync(path)){
			res.push(false);
		}else{try{
			res.push(JSON.parse( fs.readFileSync(path) ));
		}catch(e){ console.log(e); res.push(false) }}
	});
	
	callback(null, res);
}