const f = function(conn, code, res, _callback){ return new Promise((resolve, reject)=>
{
	if(!res) res = {};
	
	const callback = (err)=>
	{
		if(typeof _callback == 'function') _callback( ...(
			DB.asyncCB.indexOf(_callback.toString()) != -1 ? [] : [err] // для inline-вызова в async-функциях
		));
		else if(err) reject(err); else resolve(res);
	};

	DB.session.get(conn.user.session, code, (err, field)=>{
		if(field && field.parent){
			DB.session.get(conn.user.session, field.parent, (err, parent)=>{
				if(parent && parent.col){
					res.field = field;
					res.parent = parent;
					callback();
				}else{
					callback({status: 'err', err: 'Ошибка получения родителя'});
				}
			});
		}else{
			callback({status: 'err', err: (field ? 'У поля отсутствует обязательный родитель' : 'Ошибка получения поля')})
		}
	});
}).catch(err => { throw err }) }

exports.redis = f;
exports.files = f;