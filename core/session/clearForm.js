/*
	*caller					*data
	user~session_close 	::	undefined 										удаляем все
	getForm				::	{}												удаляем все
	getSubForm			::	{form: field.form}								удаляем form
	showComplex			::	{complex: code, form: field.form}				удаляем complex
							{complex: showData.code, form: field.form}		удаляем complex
	deleteComplex		::	{complex: deleteData.code, form: field.form}	удаляем complex 
*/

const f = function(conn, data, callback){try{
	
	const user = conn.user;

	if(data && data.complex) // showComplex и deleteComplex
	{
		var del = [], parents = user.forms && user.forms[data.form] ? user.forms[data.form].parents : {};
		
		function toDelele(code){
			
			del.push(code);
			
			if(parents[code]){
				
				parents[code].forEach((c)=>{
					
					toDelele(c);
					
					user.forms[data.form].parents[c] = undefined;
				});
			}
		}
		
		toDelele(data.complex);
		
		if(del.length)
		{
			Object.entries( SUB.users[ conn.xaoc.id ] )
			.filter(([key, value]) => del.includes(value.code))
			.forEach(([key, value]) =>
			{
				delete SUB.users[ conn.xaoc.id ][ key ];
				delete SUB.fields[ key ][ conn.xaoc.id ];
				
				if(!Object.keys(SUB.fields[ key ]).length)
					delete SUB.fields[ key ];
			})
			
			DB.session.del(user.session, del);

			if(typeof callback == 'function') callback();
		}else{
			if(typeof callback == 'function') callback();
		}
	}
	else
	{
		if((data||{}).form == undefined) 						// user~session_close или getForm
		{
			Object.entries( SUB.users[ conn.xaoc.id ] )
			.forEach(([key, value]) => 
			{
				delete SUB.users[ conn.xaoc.id ][ key ];
				delete SUB.fields[ key ][ conn.xaoc.id ];
				if(!Object.keys(SUB.fields[ key ]).length)
					delete SUB.fields[ key ];
			})
		}
		else													// getSubForm
		{
			Object.entries( SUB.users[ conn.xaoc.id ] )
			.filter(([key, value]) => value.form == data.form)
			.forEach(([key, value]) =>
			{
				delete SUB.users[ conn.xaoc.id ][ key ];
				delete SUB.fields[ key ][ conn.xaoc.id ];
				if(!Object.keys(SUB.fields[ key ]).length)
				{
					delete SUB.fields[ key ];
				}
			})
		}
		
		DB.session.get(user.session, 'forms', (err, forms)=>{
			
			var del = ['forms'];
			
			for(var f in forms){
				
				if(data == undefined || data.form == undefined || data.form == f){				
					for(var i = 1; i < forms[f].count; i++){
						del.push((forms[f].code?forms[f].code+'_':'')+i);
					}
				}
			}

			if(del.length) DB.session.del(user.session, del);
			
			if(typeof callback == 'function') callback();
		});
	}
}catch(e){ console.log(e); if(typeof callback == 'function') callback() }}

exports.redis = f;
exports.files = f;