
const f = function(conn, cb){
	
	var s = SYS.get(conn, 'user.session');
	
	// ��� ������������� ������ ��� �������������� ������, ������� ����� ���� ���������� � ������� ��������
	if((s+'').indexOf(process.pid+'_') !== 0){ // ��������� �������, �������� ������
		console.log('ERROR session init', s, process.pid);
		s = '';
	}
	
	DB.session.get(s, '__', (err, session)=>{
		if(session){
			conn.user.config.editMode = session.config ? session.config.editMode : false;
			cb();
		}else{
			process.sessionCount = (process.sessionCount||0) + 1;
			conn.user.session = process.pid + '_' + SERVER_START_TIME + '_' + process.sessionCount;
			cb(true);
		}
	});
}

exports.redis = function(conn, callback){
	
	f(conn, (newSession)=>{
		
		if(newSession){
			
			DB.session.set(conn.user.session, '__', {user: conn.user.key}, ()=>{
				if(typeof callback == 'function') callback();
			});
		}else{
			if(typeof callback == 'function') callback();
		}
	});
}

exports.files = function(conn, callback){
	
	f(conn, (newSession)=>{
		
		if(newSession){
			
			fs.mkdirSync(PROJECT_LINK+'/../__cache/'+PROJECT+'/'+conn.user.session);
			
			DB.session.set(conn.user.session, '__', {user: conn.user.key}, ()=>{
				if(typeof callback == 'function') callback();
			});
		}else{
			if(typeof callback == 'function') callback();
		}
	});
}