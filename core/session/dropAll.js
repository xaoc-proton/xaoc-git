
exports.redis = function(){
	DB.redis.flushdb();
}

exports.files = function(){
	var path = PROJECT_LINK+'/../__cache/'+PROJECT;
	(fs.readdirSync(path)||[]).forEach((s)=>{ if(s){
		(fs.readdirSync(path+'/'+s)||[]).forEach(f=>fs.unlinkSync(path+'/'+s+'/'+f));
		if(s[0] != '_') fs.rmdirSync(path+'/'+s);
	} });
}