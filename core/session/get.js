
exports.redis = function(session, code, callback){
	DB.redis.get(session+'__'+code, (err, data)=>{
		if(err){
			callback(err);
		}else{
			if(!data){
				callback(null, false);
			}else{try{
				callback(null, JSON.parse(data));
			}catch(e){ console.log(e, data); callback(null, false) }}
		}
	});
}

exports.files = function(session, code, callback){
	var path = PROJECT_LINK+'/../__cache/'+PROJECT+'/'+session+'/'+code+'.json';
	if(!fs.existsSync(path)){
		callback(null, false);
	}else{try{
		callback(null, JSON.parse( fs.readFileSync(path) ));
	}catch(e){ console.log(e); callback(null, false) }}
}