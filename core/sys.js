
LST = {};
BLOCK = {};
SUB = {
	users: {},
	fields: {},
};

exports.init = ()=>{ // вызывается при инициализации процесса
	
	String.prototype.firstUpper = function(){
		return this ? SYS.firstUpper(this) : this;
	}
	
	Number.prototype.format = function(format, data){
		
		var res = this;

		switch(format){
			
			case 'RUB':
				res = this.toLocaleString('ru-RU', data || {style:'currency',currency:'RUB',currencyDisplay:'name'});
				if(!data || data.currencyDisplay == 'none') res = res.replace(/[^0-9.,]/g, '');
				res = res.replace(/,/g, ' ').replace(/\./g, ',');
				break;
			
			case 'priceWord':
				res = (SYS.priceToWord(this.valueOf())+'').firstUpper();
				break;
		}

		return res;
	}
	
	// наполняем window необходимыми параметрами для импорта клиентских func
	// * выход из скрипта через throw new Error - лучше не придумал...
	try{ require(XAOC_LINK+'/libs/xaoc.js') }catch(e){ console.log(e.message) }
	
	OK = function(d)
	{
		if(typeof d == 'string') d = {msg: d};
		return Object.assign({status: 'ok'}, d)
	};
	ERR = function(d,e)
	{
		if(e) console.log(e);
		if(d) d = {err: d+''};
		return Object.assign({status: 'err'}, d)
	};
	
	R = function()
	{
		const cb = arguments[2];
		const errorCallback = arguments[0].db.errorCallback;
		arguments[2] = function(...args){try{
			cb(...args);
		}catch(e){ errorCallback( ERR('Ошибка выполнения действия (route)', e) ) }}
		
		ROUTER.route(...arguments);
	}

	aW = function(conn)
	{
		this.errorCallback = conn.db.errorCallback;
		this.list = [];
		this.push = f=>{ this.list.push(f||[]); return this; }
		this.concat = l=>{ this.list = this.list.concat(l||[]); return this; }
		this.run = cb=>{
			async.waterfall(this.list, (...args)=>{try{
				if(typeof cb == 'function') cb(...args);
			}catch(e){ this.errorCallback( ERR('Ошибка выполнения действия (async)', e) ) }});
		}
	};
	
	aP = function(conn)
	{
		this.errorCallback = conn.db.errorCallback;
		this.list = [];
		this.push = f=>{ this.list.push(f||[]); return this; }
		this.concat = l=>{ this.list = this.list.concat(l||[]); return this; }
		this.run = cb=>{
			async.parallel(this.list, (...args)=>{try{
				if(typeof cb == 'function') cb(...args);
			}catch(e){ this.errorCallback( ERR('Ошибка выполнения действия (async)', e) ) }});
		}
	};

	// для добавлении новых констант, их необходимо предварительно объявить в server.js
	
	return this
}


exports.get = require('lodash/get');
exports.set = require('lodash/set');
exports.unset = require('lodash/unset');
exports.cloneDeep = require('lodash/cloneDeep');

exports.isNum = function(str){
	return /^\d+$/.test(str||'')
}

exports.cronProcess = function(timeout, data, p){
	
	var t = timeout*1;
	
	if(data.nullFirstDelay){
		data.nullFirstDelay = undefined;
		t = 0;
	}
	
	data.tic = (data.tic || 0) + 1;
	
	if(typeof p === 'function' && timeout >= 0){
		setTimeout(function(){
			if(data.waitForReady){
				p(data, ()=>{
					SYS.cronProcess(timeout, data, p);
				});
			}else{
				p(data);
				SYS.cronProcess(timeout, data, p);
			}
		}, t > 2147483647 ? 2147483647 : t);
	}
}

exports.walkDir = function(dir, result){
	if(!result) result = [];
	fs.readdirSync(dir).forEach(f=>{
		let dirPath = dir+'/'+f;
		let isDirectory = fs.statSync(dirPath).isDirectory();
		result.push({path: dirPath, dir: isDirectory});
		if(isDirectory) exports.walkDir(dirPath, result);
	});
	return result;
}

exports.dateFromExcel = function(date){try{
	
	var res;
	
	if((date+'').length < 6){
		res = moment((date*1-25569)*86400000);
	}else{
		res = moment(date);
	}
	
	return res || moment();
	
}catch(e){ console.log(e); return moment() }}

exports.getList = function(conn, key){
	return LST[key] || false;
}
exports.getLabel = function(conn, key){
	var l = LST['labels'] || false;
	return SYS.get(l, `list.obj[${key}].l`) || '';
}

exports.getFileProjectData = function(type, name)
{
	const result = {
		type: type,
		name: name,
		path: PROJECT_LINK, 
		multi: false,
	};

	if(BLOCK.__alias[result.type+'~'+result.name])
		result.name = BLOCK.__alias[result.type+'~'+result.name];
	
	result.name += result.name.indexOf('.js') == -1 ? '.js' : '';
	
	if(BLOCK.__customName[result.name])
	{
		result.path = BLOCK.__customName[result.name].path;
		result.type = BLOCK.__customName[result.name].name;
	}
	else
	{
		if(result.type == 'lst')
		{
			result.path = LST[result.name.replace('.js', '')].path;
		}
		else if(BLOCK[result.type] && BLOCK[result.type][result.name])
		{
			result.path = BLOCK[result.type][result.name].path;
			result.multi = BLOCK[result.type][result.name].multi;
		}
		else
		{
			switch(result.type)
			{
				case 'action': 
				case 'func':
					result.path += '/actions/'+result.name;
					break; 
				case 'lst':
					result.path += '/static/lst/'+result.name;
					break; 
				case 'form': 
				case 'forms':
					result.type = 'form';
					result.path += '/forms/'+result.name;
					break;
				case 'html':
				case 'complex': 
					result.path += '/forms/'+result.type+'/'+result.name;
					break;
				case 'cache':
					result.path += '/forms/cache/'+result.name;
					break;
				case 'custom':
				default:
					result.path = result.name;
			}
		}
	}
	
	return result;
}

exports.requireFile = function(type, name, hideError)
{	
	const file = SYS.getFileProjectData(type, name);
	let res = false;
	
	try
	{	
		// можно при желании вставить проверку на валидность пути
		// /^[0-9a-z_~\/]+$/i.test(msgData.action)
		if(CONFIG.DEBUG) delete require.cache[ require.resolve(file.path) ];
		res = require(file.path);
		file.multi = SYS.get(res, 'config.multi');
	}
	catch(err)
	{
		if(err.code == 'MODULE_NOT_FOUND'){
			if(hideError !== true) console.log('ERROR :: Отсутствует файл "'+file.path+'"');
		}else{
			console.log(err);
		}
		
		switch(file.type){ case 'html': case 'complex': case 'form':
			res = {empty: true, tpl: ()=>{return []}};
		break; case 'lst':
			res = [{v: '', l: '\xa0'}];
		break; }
	}

	return file.multi ? res[file.type] : res;
}

exports.updateBLOCK = function(){try{
	
	BLOCK = {
		form: {},
		__alias: {},
		__install: [],
		__readme: {},
		__customName: {},
	}
	
	var path = PROJECT_LINK+'blocks';
	var blocks = fs.readdirSync(path);
	var actions = {};
	
	function checkBlock(b){
				
		if(b != '.hgempty'){
			
			var block = fs.readdirSync(path+'/'+b);
			
			if(block && block.length){
				
				XAOC_CONFIG.fileAccess.dir.push('/blocks/'+b+'/static/');
				XAOC_CONFIG.fileAccess.dir.push('/blocks/'+b+'/lst/');
				
				block.forEach((f)=>{try{
					
					var file = f.split('~');
					
					if(file[1]){
						
						var link = path+'/'+b+'/'+f
						
						if(CONFIG.DEBUG) delete require.cache[require.resolve(link)];
						var req = require(link);
						
						if(req.config && req.config.multi){
							
							var sfx = req.config.sfx ? req.config.sfx === true ? file[0]+'_' : req.config.sfx+'_' : '';
							
							Object.keys(req).forEach((key)=>{ if(key != 'config'){ if(key == 'lst'){
								
								var lst = req[key].toString();
								
								lst = lst.substring(lst.indexOf('{')+1, lst.lastIndexOf('}'));
								
								lst += "if(typeof exports != 'undefined') exports.lst = lst;\n"
								lst += "if(typeof window != 'undefined') window.LST['"+b+'~'+sfx+file[1].replace('.js','')+"'] = lst;"
								
								if(!fs.existsSync(path+'/'+b+'/lst/')) fs.mkdirSync(path+'/'+b+'/lst/');
								fs.writeFileSync(path+'/'+b+'/lst/'+sfx+file[1], lst);
							}else{

								checkData(b, key, sfx+file[1], Object.assign({path: link}, req.config), req[key]);
							
							}}});
						}else{

							checkData(b, file[0], file[1], {path: link}, req);
						
						}
					}else{
						if(fs.existsSync(PROJECT_LINK+'/blocks/'+b+'/'+f+'/config.json')) checkBlock(b+'/'+f);
					}
				}catch(e){console.log(e)}});
			}
		}
	}
	
	function checkData(b, type, fname, link, req){
		
		let name;
		
		if(SYS.get(req, 'config.customType'))
		{
			name = type+'.js';

			if(typeof req.config.customName == 'function') name = req.config.customName(type);
			else if(typeof link.customName == 'function' && req.config.customName !== true) name = link.customName(type);
			name += name.indexOf('.js') == -1 ? '.js' : '';
			
			name = b+'~'+name;
			
			link.name = type+'';
			type = req.config.customType;
			
			BLOCK.__customName[name] = link;
		}else{
			name = b+'~'+fname;
		}
		
		if(!BLOCK[type]) BLOCK[type] = {};
		
		if(!req.config) req.config = {};
		if(req.access) req.config.access = true;
		BLOCK[type][name] = Object.assign(link, req.config);

		if(type == 'form'){
			if(!BLOCK.html) BLOCK.html = {};
			BLOCK.html[name] = BLOCK[type][name];
		}
		if(type == 'action') actions[name] = {req: req, __path: link.path};
		
		if(req.api && req.api.alias) req.api.alias.forEach((_)=>{
			BLOCK.__alias[type+'~'+_] = name;
		});
		
		if(req.install === true) BLOCK.__install.push({req: req, __path: link.path});
		if(req.readme){
			if(!BLOCK.__readme[b]) BLOCK.__readme[b] = {__path: link.path.substr(0, link.path.lastIndexOf('/'))};
			BLOCK.__readme[b][name] = Object.assign({__path: link.path}, req.readme);
		}
	}
	
	if(blocks && blocks.length){	
		
		blocks.forEach((b)=>{ checkBlock(b) });

		BLOCK.__prepareUser = Object.keys(actions)
			.map(_=>actions[_]).filter(_=>_.req.prepareUser)
			.sort((a,b)=>((b.req.zIndex||0) - (a.req.zIndex||0)));
	
	}
	
}catch(e){console.log(e)}}

exports.updateLST = function(config){try
{
	if(!config) config = {};
	
	function prepareLST(key, link)
	{	
		if(key && link)
		{
			let realKey = key, 
				branch = 'rep'; // базовый справочник - тот, что в репозитории 
			
			if(key.slice(-4) == '_ver')
			{ // пока что для версионности использую такой суффикс в названии файла
				const keyArr = key.split('__');
				realKey = keyArr.shift(),
				branch = keyArr.pop().replace('_ver','');
			}
			
			if(!LST[realKey]) LST[realKey] = {branch: {}, list: {obj: {}}};
			
			LST[realKey].branch[branch] = {
				label: ({
					rep: 'Базовая', user: 'Пользовательская', sys: 'Системная',
				})[branch],
				path: link+'',
			};
			
			// данные об активных ветках справочников храним только в БД
			if(!DB.__content.lstBranch[realKey] || DB.__content.lstBranch[realKey] == branch)
			{ // если справочник не пытались редактировать вручную, то переменной в DB.__content.lstBranch не будет
				delete require.cache[require.resolve(link)]; // обновляем всегда, вне зависимости от значения CONFIG.DEBUG
				Object.assign(LST[realKey].list, require(link));
				
				LST[realKey].date = fs.statSync(link).mtime; // это значение уйдет на фронт, и по нему будет определяться, нужно ли забрать с сервера новую версию справочника
				LST[realKey].path = link;
				
				// если дефолтное значение не установлено в явном виде, то берем его из первого элемента справочника
				LST[realKey].defval = SYS.get(LST, realKey+'.list.lst[0]') ? SYS.get(LST, realKey+'.list.lst[0].v')||'' : false;
				
				(SYS.get(LST, realKey+'.list.lst')||[]).forEach(l =>
				{
					LST[realKey].list.obj[l.v] = l;
					if(l.defval) LST[realKey].defval = l.v; // дефолтное значение указано в явном виде
				});
			}
		}
	}
	
	function checkLST(pathArr){
		
		pathArr.forEach((p)=>{try{
			
			var path = PROJECT_LINK + p;
			
			if(fs.existsSync(path)){
				
				var files = fs.readdirSync(path);
				
				if(files && files.length){

					files.forEach(l =>
					{
						if(l != 'custom' && l != '.hgempty')
						{
							var link = path+l;
							var key = l.split('.js')[0];
							
							if(p.indexOf('/blocks/')==0){
								key = p.replace(/\/blocks\//,'').replace(/\/lst\//,'')+'~'+key;
								if(key.indexOf('/') != -1 && key.indexOf('/static~') != -1){
									key = key.split('/');
									key = key[1]+'_'+key[0]+key[2].replace('static', '');
								}
							}
							
							if(!config.lst || key == config.lst)
							{
								prepareLST(key, link);
								
								// LST[key] может не быть, если в prepareLST обрабатывалась version справочника
								if(LST[key] && LST[key].list.custom)
								{ // дочерний список	
									for(var c in LST[key].list.custom)
									{	
										var ckey = 'custom/'+c;
										var link = PROJECT_LINK+'/static/lst/'+ckey+'.js';
										
										var customList = "var lst = "
										customList += JSON.stringify( LST[key].list.custom[c]( LST[key].list.lst) )+';\n';
										customList += "if(typeof exports != 'undefined') exports.lst = lst;\n"
										customList += "if(typeof window != 'undefined') window.LST["+"'"+ckey+"'"+"] = lst;"
										fs.writeFileSync(link, customList);
										
										prepareLST(ckey, link);
									}
								}
							}
						}
					});
				}
			}
		}catch(err){ console.log(err) }});
	}
	
	if(config.prepare){
		prepareLST(config.prepare.key, config.prepare.link);
	}else{
		checkLST(['/static/lst/'].concat(XAOC_CONFIG.fileAccess.dir.filter(_=>
			_.indexOf('/blocks/') == 0 && _.indexOf('/lst/') != -1
		)));
	}

}catch(e){ console.log(e) }}

exports.getUploadLink = function(privy, col, val, sfx){
	
	if(!val) val = '';
	
	var d = new Date();
	var link = '/upload/'+(privy?'private':'public')+'/';
	link += col+'/';
	if(!fs.existsSync(PROJECT_LINK+link)) fs.mkdirSync(PROJECT_LINK+link);
	link += d.getFullYear()+'/';
	if(!fs.existsSync(PROJECT_LINK+link)) fs.mkdirSync(PROJECT_LINK+link);
	link += (d.getMonth()+1)+'/';
	if(!fs.existsSync(PROJECT_LINK+link)) fs.mkdirSync(PROJECT_LINK+link);
	link += d.getDate()+'/';							
	if(!fs.existsSync(PROJECT_LINK+link)){
		fs.mkdirSync(PROJECT_LINK+link);
		fs.chmodSync(PROJECT_LINK+link, '777');
	}
	
	var count = fs.readdirSync(PROJECT_LINK+link).length + 1;
	count += sfx ? '_'+sfx : '';
	
	return val === false ? link+sfx : link+count+val.substring(val.lastIndexOf('.'));
}

exports.firstUpper = (str)=>{
	return str ? (str[0]||'').toLocaleUpperCase() + str.slice(1).toLocaleLowerCase() : '';
}

exports.priceToWord = (price)=>{	
	var rubles = require('rubles').rubles;
	return price ? rubles(price) : price;
}

exports.dateToWord = (date)=>{
	
	var d = (date||'').split('.'), r = date;
	
	if(d.length == 3){
		
		r = '';
		
		switch(d[0]){
			case '1': r += 'первое'; break;
			case '2': r += 'второе'; break;
			case '3': r += 'третье'; break;
			case '4': r += 'четвертое'; break;
			case '5': r += 'пятое'; break;
			case '6': r += 'шестое'; break;
			case '7': r += 'седьмое'; break;
			case '8': r += 'восьмое'; break;
			case '9': r += 'девятое'; break;
			case '10': r += 'десятое'; break;
			case '11': r += 'одиннадцатое'; break;
			case '12': r += 'двенадцатое'; break;
			case '13': r += 'тринадцатое'; break;
			case '14': r += 'четырнадцатое'; break;
			case '15': r += 'пятнадцатое'; break;
			case '16': r += 'шестнадцатое'; break;
			case '17': r += 'семнадцатое'; break;
			case '18': r += 'восемнадцатое'; break;
			case '19': r += 'девятнадцатое'; break;
			case '20': r += 'двадцатое'; break;
			case '21': r += 'двадцать первое'; break;
			case '22': r += 'двадцать второе'; break;
			case '23': r += 'двадцать третье'; break;
			case '24': r += 'двадцать четвертое'; break;
			case '25': r += 'двадцать пятое'; break;
			case '26': r += 'двадцать шестое'; break;
			case '27': r += 'двадцать седьмое'; break;
			case '28': r += 'двадцать восьмое'; break;
			case '29': r += 'двадцать девятое'; break;
			case '30': r += 'тридцатое'; break;
			case '31': r += 'тридцать первое'; break;
			default: r += d[0]; break;
		}
		
		r += ' ';
		
		switch(d[1]){
			case '1': r += 'января'; break;
			case '2': r += 'февраля'; break;
			case '3': r += 'марта'; break;
			case '4': r += 'апреля'; break;
			case '5': r += 'мая'; break;
			case '6': r += 'июня'; break;
			case '7': r += 'июля'; break;
			case '8': r += 'августа'; break;
			case '9': r += 'сентября'; break;
			case '10': r += 'октября'; break;
			case '11': r += 'ноября'; break;
			case '12': r += 'декабря'; break;
			default: r += d[1]; break;
		}
		
		r += ' ';
		
		switch(d[2]){
			case '2018': r += 'две тысячи восемнадцаторого'; break;
			case '2019': r += 'две тысячи девятнадцатого'; break;
			default: r += d[2]; break;
		}
		
		r += ' года';
	}
	
	return r;
}

exports.flatten = (objectOrArray, prefix = '', formatter = (k)=>(k))=>{

	const nestedFormatter = (k)=>('.' + k);
	
	const nestElement = (prev, value, key)=>(
		(value && typeof value === 'object' && value._bsontype != 'ObjectID')
		? { ...prev, ...SYS.flatten(value, `${prefix}${formatter(key)}`, nestedFormatter) }
		: { ...prev, ...{ [`${prefix}${formatter(key)}`]: value } }
	);

	return Array.isArray(objectOrArray)
		? objectOrArray.reduce(nestElement, {})
		: Object.keys(objectOrArray).reduce((prev, element)=>nestElement(prev, objectOrArray[element], element), {});
}

exports.objectToUrl = function(obj) {
	var str = [];
	for(var p in obj) if(obj.hasOwnProperty(p)) str.push(encodeURIComponent(p)+"="+encodeURIComponent(obj[p]));
	return str.join("&");
}

exports.or = function(val, alt, sfx, pfx) {
	return val ? (sfx||'')+val+(pfx||'') : alt;
}

exports.setTimeout = function(){
	const arg = [...arguments];
	setTimeout(()=>{try{
		arg[1]();
	}catch(e){ arg[0].db.errorCallback( ERR('Ошибка выполнения действия (setTimeout)', e) ) }}, arg[2]);
}

exports.sendSubscriptions = function(subList)
{
	if(Object.prototype.toString.call( subList ) !== '[object Array]') subList = [subList]; // единичная подписка
	
	if(this.endOfTransaction != undefined)
	{
		if(!this.conn.xaoc.waitForEndOfTransaction) this.conn.xaoc.waitForEndOfTransaction = [];
		if(this.endOfTransaction){
			subList = this.conn.xaoc.waitForEndOfTransaction.concat( subList );
			delete this.conn.xaoc.waitForEndOfTransaction;
		}else{
			this.conn.xaoc.waitForEndOfTransaction = this.conn.xaoc.waitForEndOfTransaction.concat( subList );
			return;
		}
	}
	
	// без sort может прийти saveField, который вызовет reloadItem, что не позволит совершить deleteItem
	const sortPriority = {saveField: 1, addComplex: 2, deleteComplex: 3};
	subList.sort((a,b)=>sortPriority[b.action] - sortPriority[a.action])
	.forEach(sub =>
	{	
		Object.keys(SUB.fields[sub.key]||{}).forEach(xaocID =>
		{
			if(
				xaocID.toString() != sub.owner.toString()
				|| !(sub.config||{}).hideFromOwner 			// актуально для ручного* добавления/удаления complex-item (*самостоятельно пользователем через интерфейс)
			)
			{
				const subData = SUB.users[xaocID][sub.key];
				
				if(subData.code && process.sessions[xaocID])
				{
					if(sub.action == 'addComplex' || sub.action == 'deleteComplex')
					{
						process.sessions[xaocID].user.subIds[subData.code].push(sub.value);
						// в sub.value лежит _id элемента, который на фронте не нужен (хотя если он в формате ObjectId, то он и так не отправится)
						process.sessions[xaocID].sendText({sub: {code: subData.code, action: sub.action, config: sub.config}});
					}
					else
					{
						process.sessions[xaocID].sendText({sub: {code: subData.code, action: sub.action, val: sub.value, config: sub.config}});					
					}
				}
			}
		});
	});
}

exports.dataSearch = function(data, query, type) // универсальный поиск по массиву объектов
{
	return Object.values(data || {})[ type ](item =>
	{
		const queryKeys = Object.keys(query).filter(key => query[key] != undefined);
		
		return queryKeys.filter(key =>
			(item._meta?.[key] || item[key]) != undefined
			&& (key == '_id'
				? (item._meta?.[key] || item[key]).toString() === query[key].toString()
				: (item._meta?.[key] || item[key]) === query[key]
			)
		).length == queryKeys.length;
	});
}
exports.dataFind = function(data, query){ return SYS.dataSearch( data, query, 'find' ) }
exports.dataFilter = function(data, query){ return SYS.dataSearch( data, query, 'filter' ) }