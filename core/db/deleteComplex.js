// !!! при вызове из process.deleteComplex внутри conn нихера не conn (если нужен, то он в conn.conn)
exports.f = function(conn, field, parent, _callback){ return new Promise((resolve, reject)=>
{
	const callback = (err, data)=>
	{
		if(typeof _callback == 'function') _callback( ...(
			DB.asyncCB.indexOf(_callback.toString()) != -1 ? [] : [err, data] // для inline-вызова в async-функциях
		));
		else if(err) reject(err); else resolve(data);
	};
	
	const now = Date.now();
	
	function updateMysql(key, _id, config, cb)
	{
		(conn.mysql ? (cb)=>{cb(conn.mysql)} : DB.getMysql)(mysql=>
		{try{
		
			var sql = '';
			
			if(config.deleteLinks)
			{
				if(DB.KEY_LINKS[key+'_links']){ for(var l in DB.KEY_LINKS[key+'_links']){ if(config.deleteLinks.filter(_=>_.l == l).length){
					sql += "DELETE FROM `"+key+"_links"+l+"` WHERE id = "+mysql.escape(_id+'')+"; ";
				}}}
				
				(config.deleteLinks||[]).forEach((_)=>{
					let ll = _.l;
					if(ll.substr(0,2) == '__') ll = ll.substr(2);
					if(DB.KEY_LINKS[ll+'_links']){ for(var l in DB.KEY_LINKS[ll+'_links']){ if(l == '__'+key){
						sql += "DELETE FROM `"+ll+"_links"+l+"` WHERE id = "+mysql.escape(_._id+'')+" AND p = "+mysql.escape(_id+'')+"; ";
					}}}
				});
			}
			if(!config.deleteLinks || config.deleteMysqlData)
			{
				if(DB.KEY_FIELDS[key]){ for(var field in DB.KEY_FIELDS[key]){ 
					sql += "DELETE FROM `"+key+"__"+field+"` WHERE id = "+mysql.escape(_id+'')+"; ";
				}}
				
				if(DB.KEY_LINKS[key+'_links']){ for(var l in DB.KEY_LINKS[key+'_links']){
					sql += "DELETE FROM `"+key+"_links"+l+"` WHERE id = "+mysql.escape(_id+'')+"; ";
				}}
				
				if(DB.KEY_CHILD[key+'_links']){ DB.KEY_CHILD[key+'_links'].forEach((l)=>{
					if(DB.KEY_LINKS[l+'_links'] && DB.KEY_LINKS[l+'_links']['__'+key]){
						sql += "DELETE FROM `"+l+"_links__"+key+"` WHERE p = "+mysql.escape(_id+'')+"; ";
					}
				})}
			}
			
			if(sql){
				mysql.query(sql, (err, rows, fields)=>{
					if(err) console.log(sql, err);
					if(!conn.mysql) mysql.destroy();
					cb();
				});
			}else{
				if(!conn.mysql) mysql.destroy();
				cb();
			}
		}catch(e){ conn.db.errorCallback( ERR('Ошибка выполнения действия (delete)', e) ) }});
	}
	
	function updateMongoWithSub(conn, f, p, l, nolink){

		var _id = ObjectId(f._id);

		if(p._id && p._id !== true && p.childLink !== false && nolink !== true){
			
			
			var $update = {$pull: {}, $inc: {}, $set: {}}, $fields = {_id: 1};
			var childLink = p.childLink || l;
			
			$update.$pull[childLink+'.l'] = _id;
			$update.$inc[childLink+'.c'] = -1; // нет проверки на то, то ссылка существовала (тогда не делать -1) - поправить
			$update.$set[childLink+'.u'] = now;
			$fields[childLink+'.c'] = 1;

			//conn.db.collection(p.col).findAndModify({_id: ObjectId(p._id)}, [], {$pull: $pull, $inc: $inc, $set: $set}, {new: true, fields: $fields}, (err, data)=>{
			conn.db.findAndModify(p.col, {_id: ObjectId(p._id)}, [], $update, {new: true, fields: $fields}, (err, data)=>
			{
				if(data.ok)
				{
					if(conn.conn)
					{
						SYS.sendSubscriptions([
							{value: f._id, action: 'deleteComplex', key: [p._id.toString(), p.col, link, f._id.toString()].join('-'), config: {hideFromOwner: true}, owner: conn.conn.xaoc.id},
							{value: -1, action: 'saveField', key: [p._id.toString(), p.col, link+'.c'].join('-'), config: {inc: true}, owner: conn.conn.xaoc.id},
							{value: now, action: 'saveField', key: [p._id.toString(), p.col, link+'.u'].join('-'), owner: conn.conn.xaoc.id},
						]);
					}
				}
			});
		}
	}
	
	if(!field._id){
		callback({status: 'err', err: 'Ошибка (отсутствует  field._id)'});
		throw new Error('Ошибка (отсутствует  field._id)');
	}else
	{
		if(!field.name) field.name = field.col;

		var multiParent = ( Object.prototype.toString.call( parent ) === '[object Array]' );
		var parents = multiParent ? parent : [parent];
		var link = (field.link ? (typeof field.link == 'object') ? field.link[0] : field.link : '__'+field.col+(field.sfx||''))+'';
		var linksFunc = field.links ? (typeof field.links=='function' ? field.links : (cb)=>{cb(field.links)}) : (cb)=>{cb()};
		
		linksFunc((links)=>
		{
			if(!links) links = {};
			
			parents.forEach((p)=>{
				if(!p.name) p.name = p.col;
				const l = links && links[p.name+(p.sfx||'')] ? links[p.name+(p.sfx||'')] : link;
				updateMongoWithSub(conn, field, p, l);
			});

			// объект может быть уже удален и находиться в _del, если критично - надо дописать
			conn.db.findOne(field.col, {_id: ObjectId(field._id)}, {}, (err, f)=>
			{
				// сразу не вспомнил, почему по дефолту ссылки не удаляются... при переписывании нужно еще раз проанализировать...
				var deleteLinks = false;
			
				if(!field.deleteLinks){
					if(f && f._id){
						f.delete_time = now;
						conn.db.insert(field.col+'_del', f, err=>{
							conn.db.remove(field.col, {_id: f._id});
						});
					}
				}else{
					deleteLinks = [];
				}
				
				parents.forEach((p)=>{
					
					if(!p.name) p.name = p.col;
					
					if(links[field.name] === false || SYS.get(links, field.name+'.'+p.name+(p.sfx||'')) === false) p.parentLink = false;
					if(SYS.get(links, field.name+'.'+p.name+(p.sfx||'')+'_sql') === true) p.sqlLink = true;

					var l = links[field.name] ? links[field.name][p.name+(p.sfx||'')] : undefined, nolink;
					if(p.sqlLink) l = p.name+(p.sfx||'');
					
					if(l === false) nolink = true;
					const ll = l || (p.parentLink || link.replace(field.col, p.col+(p.sfx||'')));
					
					// см.выше комментарий к var deleteLinks = false;... заменил, чтобы ссылки не удалялись только при явном указании false
					deleteLinks = [];
					if(field.deleteLinks !== false) deleteLinks.push({l: ll, _id: p._id});
					//if(field.deleteLinks) deleteLinks.push({l: ll, _id: p._id});
					updateMongoWithSub(conn, p, field, ll, nolink);
				});
				
				if(field.deleteMysql !== false){
					// c field.name нужно указывать кастомные links в install, заменил на field.col, чтобы работало в общих случаях
					updateMysql(field.col, field._id, {deleteLinks: deleteLinks, deleteMysqlData: field.deleteMysqlData}, ()=>{ callback() });
				}else{
					callback();
				}
			});
		}, conn, field, parents);
	}

}).catch(e=>{ throw e }) }