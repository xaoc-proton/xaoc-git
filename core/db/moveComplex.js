exports.f = function(conn, data, _callback){ return new Promise((resolve, reject)=>
{
	const callback = (err, data)=>
	{
		if(typeof _callback == 'function') _callback( ...(
			DB.asyncCB.indexOf(_callback.toString()) != -1 ? [] : [err, data] // для inline-вызова в async-функциях
		));
		else if(err) reject(err); else resolve(data);
	};

	const result = {};
	let err = null;
	
	//lo('data.to', Object.keys(data.item).filter(_=> data.to[0][_] ).map(_=>[_, data.item[_], '->', data.to[0][_]]));
	//lo('data.from', Object.keys(data.item).filter(_=> data.from[0][_] ).map(_=>[_, data.item[_], '->', data.from[0][_]]));
	data.from[0] = Object.assign({}, data.item, data.from[0], {deleteLinks: true}); // при переносе итема никогда не требуется его удалять
	data.to[0] = Object.assign({}, data.item, data.to[0]);
	
	(async ()=>
	{
		// делать addComplex раньше deleteComplex нельзя, иначе будет перетираться родитель при перемещении ссылки внутри одного объекта
		//lo('move delete', ...([conn].concat(data.from||[]) ).slice(1));
		await DB.deleteComplex( ...([conn].concat(data.from||[]) )).then(res=>{ result.delete = res }).catch(e=>{ err = e });
		//lo('move add', ...([conn].concat(data.to||[]) ).slice(1));
		await DB.addComplex( ...([conn].concat(data.to||[]) )).then(res=>{ result.add = res }).catch(e=>{ err = e });
		callback(err, result);
	})();
	
}).catch(e=>{ conn.db.errorCallback( ERR('Ошибка выполнения действия (move)', e) ) }) }