
exports.f = function(conn, field, parent, out, _callback){ return new Promise((resolve, reject)=>
{
	const callback = (err, data)=>
	{
		if(typeof _callback == 'function') _callback( ...(
			[err, data]
			//DB.asyncCB.indexOf(_callback.toString()) != -1 ? [] : [err, data] // для inline-вызова в async-функциях
		));
		else if(err) reject(err); else resolve(data);
	};
	
	var $id = {}, $update = {}, col = parent.col, keyCol = parent.col, keyName = field.name.replace(/\./g, '_'), keyId = parent._id;

	$id = {_id: ObjectId(parent._id)};
	
	if(out.$inc){
		$update.$inc = {};
		$update.$inc[field.name] = out.value*1;
	}else{
		$update.$set = {};
		$update.$set[field.name] = out.value;
		switch(field.vartype){
			 // все что в монго меняется через $inc нужно приводить к int
			case 'int': $update.$set[field.name] *= 1; break;
		}
	}

	// { upsert: true } лучше не использовать - источник магии
	conn.db.collection(col).update( $id, $update, ()=>{try{
		
		if(field.copyTo && conn.user.forms[parent.form].copyLinks[field.copyTo]){

			DB.session.get(conn.user.session, conn.user.forms[parent.form].copyLinks[field.copyTo], (err, copy)=>{

				if(copy && copy._id && copy._id.length == 24){
					DB.saveField(conn, {name: field.name}, {_id: ObjectId(copy._id), col: copy.col}, out.$inc ? {value: $update.$inc[field.name], $inc: true} : {value: $update.$set[field.name]});
				}
			});
		}

		SYS.sendSubscriptions([
			{value: out.value, action: 'saveField', key: [parent._id.toString(), parent.col, field.name].join('-'), config: {inc: out.$inc}, owner: conn.xaoc.id},
		]);
		
		if(DB.KEY_FIELDS[keyCol] && DB.KEY_FIELDS[keyCol][keyName])
		{
			(conn.mysql ? (cb)=>{cb(conn.mysql)} : DB.getMysql)((mysql) => {
				
				var sql, escape;
				
				if(field.deleteLinks === true){
					sql = "DELETE FROM ?? WHERE id = ?";
					escape = [keyCol+"__"+keyName, keyId+''];
				}else
				{
					if(DB.KEY_FIELDS[keyCol][keyName] == 'reverse'){
						sql = "UPDATE ?? SET f = "+(out.value?mysql.escape(out.value+''):'DEFAULT')+", ff = "+(out.value?mysql.escape((out.value+'').split('').reverse().join('')):'DEFAULT')+" WHERE id = ?";
						escape = [keyCol+"__"+keyName, keyId+''];
					}else
					{
						if(out.$inc){
							sql = "UPDATE ?? SET f = f + ? WHERE id = ?";
							escape = [keyCol+"__"+keyName, out.value, keyId+''];
						}else
						{
							if(Object.prototype.toString.call( out.value ) === '[object Array]')
							{
								sql = "DELETE FROM ?? WHERE id = ?;"
								escape = [keyCol+"__"+keyName, keyId+''];
								if(out.value.length)
								{
									sql += " INSERT INTO ?? (`id`, `f`) VALUES ";
									escape.push(keyCol+"__"+keyName);

									sql += out.value.map(_=>{
										return "("+mysql.escape(keyId+'')+", "+(_.v ?mysql.escape(_.v+'') :'DEFAULT')+")";
									}).join(',');
								}
							}else
							{
								sql = "UPDATE ?? SET f = " + (out.value
										? mysql.escape( typeof out.value == 'object' 
															? ( field.saveObjValue ? out.value.v : JSON.stringify(out.value) )
															: out.value+'' )
										: 'DEFAULT')
									+ " WHERE id = ?";
								escape = [keyCol+"__"+keyName, keyId+''];
							}
						}
					}
				}
				
				mysql.query(sql, escape, (err, rows, fields)=>{
					if(err) console.log(err);
					if(!conn.mysql) mysql.destroy(); 
				});
			});
		}

		(conn.mysqlLogs ? (cb)=>{cb(conn.mysqlLogs)} : DB.getMysqlLogs)((mysql) => {try{
			
			var sql = "";

			if(DB.checkedLogsTable[col] == undefined){
				sql = "CREATE TABLE IF NOT EXISTS `"+col+"` ( `id` int(11) NOT NULL AUTO_INCREMENT, `add_time` bigint(15) NOT NULL, `user` varchar(50) NOT NULL, `parent` varchar(50) NOT NULL, `field` varchar(100) NOT NULL, `value` text NOT NULL, PRIMARY KEY (`id`), KEY `field` (`parent`, `field`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8; ";
				DB.checkedLogsTable[col] = true;
			}
			
			sql += "INSERT INTO `"+col+"` (`add_time`, `user`, `parent`, `field`, `value`)"+
					" VALUES ('"+	Date.now()
							+"', "+	mysql.escape(conn.user && conn.user.key ? conn.user.key+'' : 'false')
							+", "+	mysql.escape(parent._id+'')
							+", "+	mysql.escape(field.name)
							+", "+	(mysql.escape(	typeof out.value == 'object' 
														? JSON.stringify(out.value) 
														: ((out.$inc?'+':'')+(out.value||""))) 
									|| "''" )
							+");";
			
			var fieldCol = col+'';
			var fieldName = field.name+'';
			
			if(
				fieldCol != 'user' && 
				!(DB.__content.fieldKeys[fieldCol] && DB.__content.fieldKeys[fieldCol].indexOf(fieldName) != -1) && 
				fieldCol.length < 30 // временно, пока не уберу поля из redis, чтобы не записывались md5() данные
			){
				if(DB.__content.fieldKeys[fieldCol] == undefined) DB.__content.fieldKeys[fieldCol] = [];
				DB.__content.fieldKeys[fieldCol].push( fieldName );
				var $addToSet = {};
				$addToSet['fieldKeys.'+fieldCol] = fieldName;
				conn.db.collection('__content').update({_id: DB.__content._id}, {$addToSet: $addToSet});
			}
			
			if(sql){
				mysql.query(sql, (err)=>{
					if(err) console.log(err, sql);
					if(!conn.mysqlLogs) mysql.destroy();
					if(typeof callback == 'function') callback(null, out);
				});
			}else{
				if(!conn.mysqlLogs) mysql.destroy();
				if(typeof callback == 'function') callback(null, out);
			}
		}catch(e){ conn.db.errorCallback( ERR('Ошибка выполнения действия (save)', e) ) }});
	}catch(e){ conn.db.errorCallback( ERR('Ошибка выполнения действия (save)', e) ) }});
}).catch(e=>{ conn.db.errorCallback( ERR('Ошибка выполнения действия (save)', e) ) }) }