// !!! при вызове из process.addComplex внутри conn нихера не conn (если нужен, то он в conn.conn)
exports.f = function(conn, field, parent, values, _callback){ return new Promise((resolve, reject)=>
{
	if(values === undefined) values = {};
	if(_callback === undefined && typeof values == 'function'){
		_callback = values;
		values = {};
	}
	
	const callback = (err, data)=>
	{
		if(typeof _callback == 'function') _callback( ...(
			DB.asyncCB.indexOf(_callback.toString()) != -1 ? [] : [err, data] // для inline-вызова в async-функциях
		));
		else if(err) reject(err); else resolve(data);
	};

	if(field._id) field._id = ObjectId(field._id);
	if(!field.name) field.name = field.col;
	
	var multiParent = ( Object.prototype.toString.call( parent ) === '[object Array]' );
	var parents = multiParent ? parent : [parent];
	parents = parents.filter(_=>_.col&&_._id);
	if(field.userLink) parents.push({col: 'user', link: field.userLink, _id: conn.user.key});
	
	var now = Date.now();
	var link = (field.link ? (typeof field.link == 'object') ? field.link[0] : field.link : '__'+field.col+(field.sfx||''))+'';
	var linksFunc = field.links ? (typeof field.links=='function' ? field.links : (cb)=>{cb(field.links)}) : (cb)=>{cb()};
	
	linksFunc((links)=>
	{
		if(!links) links = {};
		
		var processSend = {};
		function sendSub(conn, field, parent, link, data)
		{
			if(conn.conn) // проверка на вызов из install.js (!!! почему то отличается формат conn)
			{
				SYS.sendSubscriptions([
					{value: field._id, action: 'addComplex', key: [parent._id.toString(), parent.col, link].join('-'), config: {hideFromOwner: true}, owner: conn.conn.xaoc.id},
					{value: 1, action: 'saveField', key: [parent._id.toString(), parent.col, link+'.c'].join('-'), config: {inc: true}, owner: conn.conn.xaoc.id},
					{value: now, action: 'saveField', key: [parent._id.toString(), parent.col, link+'.u'].join('-'), owner: conn.conn.xaoc.id},
				]);
			}
		}
		
		function updateKeys(key, _id, parents, data, cb){
			
			(conn.mysql ? cb=>{ cb(conn.mysql) } : DB.getMysql)(mysql=>
			{try{
				var sql = '';
				if(DB.KEY_FIELDS[key] && !data.existId){
					for(var ff in DB.KEY_FIELDS[key])
					{
						var f = ff == 'add_time' ? (values.fake ? -1 : 1)*now : '';
						
						if(DB.KEY_FIELDS[key][ff] == 'reverse'){
							sql += "INSERT INTO `"+key+"__"+ff+"` (`id`, `f`, `ff`) VALUES ("+mysql.escape(_id+'')+", "+(f?mysql.escape(f+''):'DEFAULT')+", "+(f?mysql.escape((f+'').split('').reverse().join('')):'DEFAULT')+"); ";
						}else
						{
							sql += "INSERT INTO `"+key+"__"+ff+"` (`id`, `f`) VALUES ";
							
							if(Object.prototype.toString.call( f ) === '[object Array]'){
								sql += f.map(_=>{
									return "("+mysql.escape(_id+'')+", "+(_.v ?mysql.escape(_.v+'') :'DEFAULT')+")";
								}).join(',')+';';
							}else{
								sql += "("+mysql.escape(_id+'')+", "+(f ?mysql.escape( typeof f == 'object' ?JSON.stringify(f) :f+'') :'DEFAULT')+"); ";
							}
						}
					}
				}

				parents.forEach(p=>
				{
					if(!p.name) p.name = p.col;
					
					if(p._id && p._id !== true && (p.parentLink !== false || p.sqlLink === true))
					{
						// не должно быть ссылок, если это повторная связка объектов - поправить

						var parentLink = p.parentLink || p.col ? '__'+p.col+(p.sfx||'') : link;
						if(links[field.name] && links[field.name][p.name+(p.sfx||'')]) parentLink = links[field.name][p.name+(p.sfx||'')];
						
						if(DB.KEY_LINKS[key+'_links'] && DB.KEY_LINKS[key+'_links'][parentLink]){
							sql += "INSERT INTO `"+key+"_links"+parentLink+"` (`id`, `p`) VALUES ("+mysql.escape(_id+'')+", "+mysql.escape(p._id+'')+"); ";
						}
						
						// var inverseLink = parentLink.replace(/\_\_/,''), inverseKey = '__'+key;
						var inverseLink = p.col, inverseKey = key;
						if(links[p.name]) inverseKey = links[p.name];
						if(DB.KEY_LINKS[inverseLink+'_links'] && DB.KEY_LINKS[inverseLink+'_links'][inverseKey]){
							sql += "INSERT INTO `"+inverseLink+"_links"+inverseKey+"` (`id`, `p`) VALUES ("+mysql.escape(p._id+'')+", "+mysql.escape(_id+'')+"); ";
						}
					}
				});
				
				if(sql){
					mysql.query(sql, (err, rows, fields)=>{ 
						if(err) console.log(err, 'sql='+sql);
						if(!conn.mysql) mysql.destroy(); 
						cb();
					});
				}else{
					if(!conn.mysql) mysql.destroy(); 
					cb();
				}
			}catch(e){ conn.db.errorCallback( ERR('Ошибка выполнения действия (add)', e) ) }});
		}

		var $insert = Object.assign({add_time: (values.fake ? -1 : 1)*now}, values);
		Object.keys($insert).map(_=>
		{
			if(typeof $insert[_] == 'string' && $insert[_].indexOf('json') == 0) // внутри json
				$insert[_] = JSON.parse($insert[_].substr(4));
			
			if(_.indexOf('.') != -1){
				SYS.set($insert, _.split('.'), $insert[_]); // делаю "_.split", потому что просто "_" почему то не сработало
				delete $insert[_];
			}
		});

		parents.forEach(p=>
		{
			if(!p.name) p.name = p.col;
			
			if(links[field.name] === false || SYS.get(links, field.name+'.'+p.name+(p.sfx||'')) === false) p.parentLink = false;
			if(SYS.get(links, field.name+'.'+p.name+(p.sfx||'')+'_sql') === true) p.sqlLink = true;
			
			if(p._id && p._id !== true && p.parentLink !== false)
			{
				var pid = ObjectId(p._id);
				var key = p.parentLink || (typeof p.link == 'object' ? p.link[0] : p.link) || link.replace(field.col, p.col);
				
				if(links[field.name] && links[field.name][p.name+(p.sfx||'')]) key = links[field.name][p.name+(p.sfx||'')];
				
				if($insert[key]){
					if($insert[key].l.filter(id=>((id+'')==(pid+''))).length == 0){
						$insert[key].c++;
						$insert[key].l.push(pid);
					}
				}else{
					$insert[key] = {u: now, c: 1, col: p.col, l: [pid]};
				}
			}
		});
		
		var collection = field._id == undefined ? conn.db.collection(field.col) : {insert: ($insert, cb)=>
		{
			var $set = Object.assign({}, values||{}), $push = {}, $inc = {};
			Object.keys($set).map(_=>{ 
				if(typeof $set[_] == 'string' && $set[_].indexOf('json') == 0) // внутри json
					$set[_] = JSON.parse($set[_].substr(4));
			});
			
			parents.forEach(p=>
			{
				if(!p.name) p.name = p.col;
				
				if(p._id && p._id !== true && p.parentLink !== false){
					
					var key = p.parentLink || (typeof p.link == 'object' ? p.link[0] : p.link) || link.replace(field.col, p.col);
					if(links[field.name] && links[field.name][p.name+(p.sfx||'')]) key = links[field.name][p.name+(p.sfx||'')];

					if($push[key+'.l']){
						$inc[key+'.c']++; // не нужно делать ++, есть ссылка уже есть - поправить
						$push[key+'.l'].$each.push(ObjectId(p._id));
					}else{
						$set[key+'.u'] = now;
						$set[key+'.col'] = p.col;
						$inc[key+'.c'] = 1;
						$push[key+'.l'] = {$each: [ObjectId(p._id)]};
					}
					sendSub(conn, p, field, key, {});
				}
			});
			
			var $update = {};
			if(Object.keys($set).length) $update.$set = $set;
			if(Object.keys($inc).length) $update.$inc = $inc;
			if(Object.keys($push).length) $update.$addToSet = $push;
			
			if(Object.keys($update).length){
				//conn.db.collection(field.col).update({_id: field._id}, $update, (err, data)=>{
				conn.db.update(field.col, {_id: field._id}, $update, err=>{
					cb(null, {insertedIds: [ field._id ], existId: true});
				});
			}else{
				cb(null, {insertedIds: [ field._id ], existId: true});
			}
		}};
		
		// тут нужен try-catch, так как не применяется универсальная обертка
		collection.insert($insert, (err, data)=>
		{try{
			if(err) console.log(err);
		
			field._id = data.insertedIds[0];

			if(process.send) process.send({ // для списков с parentDataNotRequired = true
				type: 'sub', action: 'addComplex', _id: field._id,
				key: {col: field.col, _id: '*'},
				owner: (conn.user && conn.user.forms && field.form && typeof field.form != 'function') ? [conn.user.session, field.code+'.'+field.form+'.'+conn.user.forms[field.form].t] : [], // отсутствует field.form при вызове через DB.addComplex
			});

			if(conn.data){
				if(conn.data[field.parent] == undefined) conn.data[field.parent] = {};
				conn.data[field.parent][link] = {l: [field._id]};
			}

			if(field.code){
				if(conn.queryIds == undefined) conn.queryIds = {};
				conn.queryIds[field.code] = [field._id];
			}
			
			var parallel = [];

			parents.forEach(p=>
			{
				if(!p.name) p.name = p.col;
				if(links[p.name+(p.sfx||'')] === false) p.childLink = false;
				
				if(p._id && p._id !== true && p.childLink !== false)
				{
					parallel.push(cb=>
					{
						var childLink = p.childLink || link;
						if(links[p.name+(p.sfx||'')]) childLink = links[p.name+(p.sfx||'')];

						var $id = {_id: ObjectId(p._id)}, $push = {}, $inc = {}, $set = {}, $fields = {_id: 1};
						$push[childLink+'.l'] = field._id;
						$inc[childLink+'.c'] = 1;
						$set[childLink+'.u'] = now;
						$set[childLink+'.col'] = field.col;
						$fields[childLink+'.c'] = 1;

						//conn.db.collection(p.col).findAndModify($id, [], {$addToSet: $push, $inc: $inc, $set: $set}, {new: true, fields: $fields}, (err, data)=>{
						conn.db.findAndModify(p.col, $id, [], {$addToSet: $push, $inc: $inc, $set: $set}, {new: true, fields: $fields}, (err, data)=>
						{
							if(data.ok) sendSub(conn, field, p, childLink, data);
							cb();
						});
					});
				}
			});
			
			new aW(conn).concat(parallel).run(()=>{
				
				// c field.name нужно указывать кастомные links в install, заменил на field.col, чтобы работало в общих случаях
				//updateKeys(field.name, field._id, parents, data, ()=>{
				updateKeys(field.col, field._id, parents, data, ()=>
				{try{
					if(!Object.keys(values).length){
						callback(null, {_id: field._id, col: field.col});
					}else
					{
						var save = [];
						
						if(!data.existId) save.push({id: field._id, c: field.col, n: 'add_time', v: (values.fake ? -1 : 1)*now});

						var fValues = {};
						for(var key in values){
							if(key.indexOf('__') === 0){ // все ссылки на другие объекты сохраняем в явном виде
								save.push({id: field._id, c: field.col, n: key, v: values[key]});
							}else if( typeof values[key] == 'string' && values[key].indexOf('json') == 0 ){ // внутри json, принудительно сохраняем как объект (скорее всего multiple-select)
								save.push({id: field._id, c: field.col, n: key, v: JSON.parse(values[key].substr(4))});
							}else{ // все остальное прогоняем в flatten-виде через saveFields, чтобы обновить индексы (если они есть)
								fValues[key] = values[key];
							}
						}
						
						fValues = SYS.flatten(fValues);
						for(var key in fValues) save.push({id: field._id, c: field.col, n: key, v: fValues[key]});
						
						DB.saveFields(conn, save, ()=>{
							callback(null, {_id: field._id, col: field.col});
						});
					}
				}catch(e){ conn.db.errorCallback( ERR('Ошибка выполнения действия (add)', e) ) }});
			});
		}catch(e){ conn.db.errorCallback( ERR('Ошибка выполнения действия (add)', e) ) }});
	}, conn, field, parents);

}).catch(e=>{ conn.db.errorCallback( ERR('Ошибка выполнения действия (add)', e) ) }) }