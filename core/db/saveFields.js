
exports.f = function(conn, fields, _callback){ return new Promise((resolve, reject)=>
{
	const callback = (err, data)=>
	{
		if(typeof _callback == 'function') _callback( ...(
			[err, data]
			//DB.asyncCB.indexOf(_callback.toString()) != -1 ? [] : [err, data] // для inline-вызова в async-функциях
		));
		else if(err) reject(err); else resolve(data);
	};
	
	var parents = {}, w = [], sql = '', sqlLogs = '', escape = [], escapeLogs = [], $contentAdd = {};
	
	fields.forEach((field)=>{
		
		const p = field.id+'';
		
		if(p.length == 24){
		
			if(!parents[p]) parents[p] = {_id: p, c: field.c, fields: {}};
			parents[p].fields[field.n] = ( parents[p].fields[field.n] || [] ).concat([ field ]);
			
			if(
				field.c != 'user' && 
				!(DB.__content.fieldKeys[field.c] && DB.__content.fieldKeys[field.c].indexOf(field.c) != -1) && 
				field.c.length < 30 // временно, пока не уберу поля из redis, чтобы не записывались md5() данные
			){
				if(!DB.__content.fieldKeys[field.c]) DB.__content.fieldKeys[field.c] = [];
				DB.__content.fieldKeys[field.c].push( field.n );
				$contentAdd['fieldKeys.'+field.c] = field.n;
			}
		}else{
			console.log('SAVE ERROR :: ', field);
		}
	});
	
	if(Object.keys($contentAdd).length) w.push((cb)=>{
		conn.db.collection('__content').update({_id: DB.__content._id}, {$addToSet: $contentAdd}, ()=>{ cb() });
	});
	
	const subList = {};
	
	Object.keys(parents).forEach((pid)=>{
		
		const parent = parents[pid];
		
		w.push((cb)=>{
			
			var $fields = {}, $update = {$set: {}, $inc: {}, $unset: {}, $addToSet: {}, $push: {}, $pull: {}}, log = [];
			
			Object.keys(parent.fields).forEach((f)=>{
				
				$fields[f] = true;
				
				const values = parent.fields[f];
				const field = values[0];
				if(field.type){
					switch(field.type){
						case 'inc': $update.$inc[f] = field.v*1; field.inc = true; break;
						case 'unset': $update.$unset[f] = ''; field.v = ''; field.unset = true; break;
						case 'addToSet': $update.$addToSet[f] = field.v; break;
						case 'push':
							if($update.$set[f]){
								$update.$set[f] = $update.$set[f].concat( values.map(v=>v.v) );
							}
							else $update.$push[f] = {$each: values.map(v=>v.v)};
							break;
						case 'pull': $update.$pull[f] = field.v; break;
						default:
							delete field.type; // чтобы отработало if(!field.type){...
							break;
					}
				}
				if(!field.type){
					$update.$set[f] = field.v;
					switch(field.vartype){
						 // все что в монго меняется через $inc нужно приводить к int
						case 'int': $update.$set[f] *= 1; break;
					}
				}
				
				const keyCol = parent.c, keyName = field.n.replace(/\./g, '_'), keyId = parent._id;
				
				if(DB.KEY_FIELDS[keyCol] && DB.KEY_FIELDS[keyCol][keyName])
				{
					if(field.delink === true){
						sql += "DELETE FROM `"+keyCol+"__"+keyName+"` WHERE id = '"+keyId+"'; ";
					}else{
						if(DB.KEY_FIELDS[keyCol][keyName] == 'reverse'){
							sql += "UPDATE `"+keyCol+"__"+keyName+"` SET f = "+(field.v?'?':'DEFAULT')+", ff = "+(field.v?'?':'DEFAULT')+" WHERE id = '"+keyId+"'; ";
							escape = escape.concat(field.v ? [field.v+'', (field.v+'').split('').reverse().join('')] : []);
						}else{
							if(field.inc){
								sql += "UPDATE `"+keyCol+"__"+keyName+"` SET f = f + ? WHERE id = '"+keyId+"'; ";
								escape = escape.concat([field.v]);
							}else
							{
								if(Object.prototype.toString.call( field.v ) === '[object Array]')
								{
									sql += "DELETE FROM `"+keyCol+"__"+keyName+"` WHERE id = '"+keyId+"';"
									if(field.v.length)
									{
										sql += " INSERT INTO `"+keyCol+"__"+keyName+"` (`id`, `f`) VALUES ";
										sql += field.v.map(_=>{
											return "('"+keyId+"', "+(_.v?'?':'DEFAULT')+")";
										}).join(',')+'; ';
										escape = escape.concat( field.v.map(_=>_.v) );
									}
								}else{
									sql += "UPDATE `"+keyCol+"__"+keyName+"` SET f = "+(field.v?'?':'DEFAULT')+" WHERE id = '"+keyId+"'; ";
									escape = escape.concat(field.v ? [field.v+''] : []);
								}
							}
						}
					}
				}
				
				log.push("('"+Date.now()+"', '"+(conn.user && conn.user.key ? conn.user.key : 'false')+"', '"+parent._id+"', '"+field.n+"', ?)");
				escapeLogs.push((typeof field.v == 'object' ? JSON.stringify(field.v) : ((field.inc?'+':'')+(field.v||"")))||'');
				
				// !!! не поверял
				SYS.sendSubscriptions([
					{value: field.v, action: 'saveField', key: [parent._id.toString(), parent.c, field.n].join('-'), config: {inc: field.inc}, owner: conn.xaoc.id},
				]);
			});
			
			if(DB.checkedLogsTable[parent.c] == undefined){
				sqlLogs += "CREATE TABLE IF NOT EXISTS `"+parent.c+"` ( `id` int(11) NOT NULL AUTO_INCREMENT, `add_time` bigint(15) NOT NULL, `user` varchar(50) NOT NULL, `parent` varchar(50) NOT NULL, `field` varchar(100) NOT NULL, `value` text NOT NULL, PRIMARY KEY (`id`), KEY `field` (`parent`, `field`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8; ";
				DB.checkedLogsTable[parent.c] = true;
			}
			
			sqlLogs += "INSERT INTO `"+parent.c+"` (`add_time`,`user`,`parent`,`field`,`value`) VALUES "+log.join()+"; ";
			
			if(Object.keys($update.$set).length == 0) delete $update.$set;
			if(Object.keys($update.$inc).length == 0) delete $update.$inc;
			if(Object.keys($update.$unset).length == 0) delete $update.$unset;
			if(Object.keys($update.$addToSet).length == 0) delete $update.$addToSet;
			if(Object.keys($update.$push).length == 0) delete $update.$push;
			if(Object.keys($update.$pull).length == 0) delete $update.$pull;
			//console.log("$fields", $fields);
			//console.log("$update", $update);
			
			if(!Object.keys($update).length){
				console.log('ERR попытка присвоить объекту $update={}', fields);
				cb();
			}else
			{
				fields = []; // не вспомнил зачем - вероятно для обнуления параметра 'save' у родителя
				
				conn.db.collection(parent.c)
				.findAndModify({_id: ObjectId(parent._id)}, [], $update, {new: true, fields: $fields}, (err, data)=>{
					//console.log('findAndModify', data);
					if(err) console.log(err);
					
					Object.keys(parent.fields).forEach(f=>{
						if(subList[parent._id+'_'+f]){
							subList[parent._id+'_'+f].val = SYS.get(data.value, f);
							/*if(f.indexOf('.') != -1){
								let n = f.split('.');
								let key = n.pop();
								n = n.join('.');
								subList[parent._id+'_'+n].val[key] = SYS.get(data.value, f[key]);
							}*/
						}
					});
					
					cb();
				});
			}
		});
	});
	
	async.parallel(w, ()=>{
		
		Object.values(subList).forEach(_=>{ process.send(_) });
		
		if(sqlLogs){
			
			(conn.mysqlLogs ? (cb)=>{cb(conn.mysqlLogs)} : DB.getMysqlLogs)((mysql)=>{
				mysql.query(sqlLogs, escapeLogs, (err)=>{
					
					if(err) console.log(err, sqlLogs);
					if(!conn.mysqlLogs) mysql.destroy();
					
					if(sql){
						
						(conn.mysql ? (cb)=>{cb(conn.mysql)} : DB.getMysql)((mysql)=>{
							mysql.query(sql, escape, (err)=>{
								
								if(err) console.log(err, 'sql='+sql);
								if(!conn.mysql) mysql.destroy();
								
								if(typeof callback == 'function') callback();
							});
						});
					}else{
						if(typeof callback == 'function') callback();
					}
				});
			});
		}else{
			if(typeof callback == 'function') callback();
		}
	});
}).catch(e=>{ conn.db.errorCallback( ERR('Ошибка выполнения действия (save)', e) ) }) }