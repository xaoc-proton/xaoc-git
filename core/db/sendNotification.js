exports.f = function(msg){try{
	
	if(msg.user && msg.notify){
		
		DB.getMongo((db)=>{
		
			var userId = ObjectId(msg.user);
			db.collection('user').findOne(userId, {serviceWorkers: 1}, (err, user)=>{
				
				//console.log("sendNotification", user);
				
				if(user && user.serviceWorkers){
					
					if(user.serviceWorkers.web){
					
						webPush.sendNotification({
							endpoint: user.serviceWorkers.web.endpoint,
							keys: {
								p256dh: user.serviceWorkers.web.p256dh,
								auth: user.serviceWorkers.web.auth,
							  }
						}, JSON.stringify(msg.notify),{
							gcmAPIKey: CONFIG.gcmAPIKey,
							vapidDetails: CONFIG.vapidDetails,
							TTL: msg.ttl || 0,
						}).then(()=>{
							//console.log("sent push")
						}, (err)=>{
							if(err) console.log(err);
						});
					}
					
					if(user.serviceWorkers.mobile){
						
						webPush.sendNotification({
							endpoint: user.serviceWorkers.mobile.endpoint,
							keys: {
								p256dh: user.serviceWorkers.mobile.p256dh,
								auth: user.serviceWorkers.mobile.auth,
							  }
						}, JSON.stringify(msg.notify),{
							gcmAPIKey: CONFIG.gcmAPIKey,
							vapidDetails: CONFIG.vapidDetails,
							TTL: msg.ttl || 0,
						}).then(()=>{
							//console.log("sent push")
						}, (err)=>{
							if(err) console.log(err);
						});
					}
				}
			});
		});
	}
}catch(e){ console.log(e) }}