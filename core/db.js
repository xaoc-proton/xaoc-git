MongoClient = require('mongodb').MongoClient;
ObjectId = require('mongodb').ObjectID;
assert = require('assert');
mysqlLib = require('mysql');
redisLib = require("redis");

mysqlPool = undefined;
mysqlLogsPool = undefined;
mysqlCachePool = undefined;

if(!CONFIG.mysqlLogs) CONFIG.mysqlLogs = Object.assign(Object.assign({}, CONFIG.mysql), {database: CONFIG.mysql.database+'_logs'});
if(!CONFIG.mysqlCache) CONFIG.mysqlCache = Object.assign(Object.assign({}, CONFIG.mysql), {database: CONFIG.mysql.database+'_cache'});

mongoSingle = undefined;
mongoCacheSingle = undefined;

exports.checkedLogsTable = {};

exports.getRedis = function(callback){try{
	callback( redis.createClient(CONFIG.redis) );
}catch(err){ console.log(err) } }

if(CONFIG.redis) try{exports.redis = redisLib.createClient(CONFIG.redis)}catch(err){console.log(err)};

function prepareMongo(db){

	db.close = (cb)=>{ if(cb) cb() };
	
	// если вдруг буду переделывать, то один из вариантов
	/*conn.db = new Proxy(conn.db, {
	  get: function (target, prop, receiver) {
		// console.log(receiver) крашит процесс
		if(!target[prop]) target[prop] = target.collection(prop);
		return Reflect.get(arguments[0], arguments[1], arguments[2]);
	  },
	});*/
	
	Object.values(['find', 'findWithObj']).forEach(_=>
	{
		const key = _;
		
		db[key] = async function()
		{
			const arg = [...arguments];
			const result = await db.collection(arg[0])[key.replace('WithObj', '')](...(arg.slice(1))).toArray()
							.catch(e=>{ this.errorCallback( ERR('Ошибка (db)', e) ) });
			if(!result) result = [];
			if(key == 'findWithObj')
			{
				result.obj = {};
				result.forEach(r=>{
					result.obj[r._id] = r;
				});
			}
			return result;
		}
	});

	Object.values(['update', 'updateOne']).forEach(_=>
	{
		const key = _;
		
		db[key] = function()
		{
			const arg = [...arguments];
			if(typeof arg[1] != 'object') arg[1] = ObjectId( arg[1] );
			let callback = arg.slice(-1)[0];
			if(typeof callback != 'function') arg.push( callback = ()=>{} ); // без этого будет беда, особенно с remove
			
			const oldValue = this.oldValue;
			
			return new Promise((resolve, reject) => // вероятно с какой то версии это лишнее и станет collection.findOne само по себе промисом
			{
				const bulk = db.collection(arg[0]).initializeOrderedBulkOp();
				
				const crossKeys = Object.keys(arg[2].$addToSet||{}).filter(_ => Object.keys(arg[2].$pull||{}).includes(_));
				
				if(key == 'updateOne') // используется в saveTransaction
				{
					const $update = DB.createUpdateMongoObject();
					
					if(Object.entries(arg[2].$addToSet||{}).length)
						for(const [key, value] of Object.entries(arg[2].$addToSet||{}))
							if(key.slice(0,2) == '__') // параметры-связки начинаются с "__"
							{
								SYS.set(arg[2], '$inc["'+key.slice(0, -2)+'.c"]', value.$each.length); 	// count для параметров-связок
								SYS.set(arg[2], '$set["'+key.slice(0, -2)+'.u"]', Date.now());			// update-time для параметров-связок
							}
					if(Object.entries(arg[2].$pull||{}).length)
						for(const [key, value] of Object.entries(arg[2].$pull||{}))
							if(key.slice(0,2) == '__') // параметры-связки начинаются с "__"
							{
								SYS.set(arg[2], '$inc["'+key.slice(0, -2)+'.c"]', 
									-1*value.$in.length + (SYS.get(arg[2], '$inc["'+key.slice(0, -2)+'.c"]')||0)*1); 	// count для параметров-связок
								SYS.set(arg[2], '$set["'+key.slice(0, -2)+'.u"]', Date.now());							// update-time для параметров-связок
							}
					
					if(crossKeys.length)
					{
						// mongo не хочет делать одновременно и $addToSet, и $pull
						bulk.find(arg[1])[key]( Object.fromEntries(Object.entries(arg[2]).filter(e => e[0] != '$pull')) );
						bulk.find(arg[1])[key]( Object.fromEntries(Object.entries(arg[2]).filter(e => e[0] == '$pull')) );
					}else{
						bulk.find(arg[1])[key](arg[2]);
					}
				}
				else // тут только update (в saveTransaction не используется)
				{
					bulk.find(arg[1])[key](arg[2]);
				}
				
				bulk.execute(async (...a)=>
				{try{
					// пока только вывод в консоль, чтобы упростить себе поиск ошибки (нужно будет переделать на подстановку ошибки в a[0])
					if(key == 'update' && !SYS.get(a, '[1].result.nModified')) console.log("\x1b[41m%s\x1b[0m", "DB INSERT ERR :: nModified == 0");
					
					if(a[1]){
						a[1].col = arg[0];
						if(key == 'insert' && a[1].insertedIds) a[1]._id = a[1].insertedIds[0];
					}
					
					if(a[0])
						reject(a[0]);
					else
					{
						if(['insert', 'insertMany', 'remove', 'update', 'updateOne'].includes(key))
							await updateMysqlIndexAndNotificateSub.call(this, key, arg);
						
						resolve(a[1]);
					}
					if(typeof callback == 'function') callback(...a);
				}catch(e){ this.errorCallback( ERR('Ошибка выполнения действия (db)', e) ) }});
			}).catch(e=>{ this.errorCallback( ERR('Ошибка (db)', e) ) });
		}
	});

	Object.values(['aggregate','findOne', 'findAndModify', 'insert', 'insertMany', 'remove']).forEach(_=>
	{
		const key = _;
		
		db[key] = function()
		{
			const arg = [...arguments];
			if(typeof arg[1] != 'object') arg[1] = ObjectId( arg[1] );
			let callback = arg.slice(-1)[0];
			if(typeof callback != 'function') arg.push( callback = ()=>{} ); // без этого будет беда, особенно с remove
			
			const oldValue = this.oldValue;
			
			return new Promise((resolve, reject) => // вероятно с какой то версии это лишнее и станет collection.findOne само по себе промисом
			{
				if(key.indexOf('insert') != -1 && (arg[1]||[]).length)
				{
					for(const item of arg[1])
						for(const [key, value] of Object.entries(item))
							if(key.slice(0,2) == '__' && value.l != undefined) // параметры-связки начинаются с "__"
							{
								value.c = value.l.length;
								value.u = Date.now();
							}
				}
				
				// чтобы arg.slice(1,-1) работало - должно быть больше 2х агрументов
				db.collection(arg[0])[key](...(arg.slice(1,-1).concat([async (...a)=>
				{try{
					// пока только вывод в консоль, чтобы упростить себе поиск ошибки (нужно будет переделать на подстановку ошибки в a[0])
					if(key == 'update' && !SYS.get(a, '[1].result.nModified')) console.log("\x1b[41m%s\x1b[0m", "DB INSERT ERR :: nModified == 0");
					
					if(a[1]){
						a[1].col = arg[0];
						if(key == 'insert' && a[1].insertedIds) a[1]._id = a[1].insertedIds[0];
					}
					
					if(a[0])
						reject(a[0]);
					else
					{
						if(['insert', 'insertMany', 'remove', 'update', 'updateOne'].includes(key))
							await updateMysqlIndexAndNotificateSub.call(this, key, arg); // делаем call ради передачи this.oldValue и this.endOfTransaction
						
						resolve(a[1]);
					}
					if(typeof callback == 'function') callback(...a);
				}catch(e){ this.errorCallback( ERR('Ошибка выполнения действия (db)', e) ) }}])));
			}).catch(e=>{ this.errorCallback( ERR('Ошибка (db)', e) ) });
		}
	});
	
	async function updateMysqlIndexAndNotificateSub(type, mongoData)
	{
		const sub_arr = [];
		
		if(type == 'insertMany')
		{
			const [col, docs] = mongoData;
			
			docs.forEach(doc =>
			{
				const meta = {...doc._meta};
				delete doc._meta;
				
				if(!doc.add_time) doc.add_time = Date.now();
				
				for(const [key, value] of Object.entries(doc))
				{
					if(key.slice(0,2) == '__') // проверяем добавление связки с другими сущностями
					{
						if(!((value||{}).l||[]).length) continue; // тут может быть пустая связка (добавленная и удаленная в рамках одной транзакции)

						for(const id of value.l)
						{
							sub_arr.push({
								action: 'addComplex', 
								key: [doc._id.toString(), col, key].join('-'), // тут "оригинальный" ключ (без суффикса ".l")
								owner: this.conn.xaoc.id,
								value: id,
							});
						}
					}else // проверяем остальные параметры (не связки) добавленного объекта
					{
						// тут не может быть уведомления о подписке, так как объекта гарантировано нет ни у кого в интерфейсе
					}
				}
			});
			
		}
		else
		{
			const [col, search, value, oldValue] = mongoData;
			
			for(const [updateKey, updateValue] of Object.entries(value))
			{
				switch(updateKey)
				{
					case '$set':
					case '$inc':
					
						for(const [key, value] of Object.entries(updateValue))
						{	
							sub_arr.push({
								action: 'saveField', 
								key: [search._id.toString(), col, key].join('-'), 
								owner: this.conn.xaoc.id,
								value,
								config: {inc: updateKey == '$inc'},
							});
							
							if(typeof value === 'object' && value != undefined && value._bsontype != 'ObjectID')
							{
								// обработка ключей N-уровня
								for(const [k, v] of Object.entries(SYS.flatten(value))) // в mysql* и для sub используются flatten-ключи (*не используется)
								{
									if(v != SYS.get(this.oldValue[updateKey][key], k)) // внутри значения-объекта могли поменяться не все поля
									{
										sub_arr.push({
											action: 'saveField', 
											key: [search._id.toString(), col, key+'.'+k].join('-'), 
											owner: this.conn.xaoc.id,
											value: v,
										});
									}
								}
							}
						}
						
						break;
						
					case '$addToSet':
					
						for(const [key, value] of Object.entries(updateValue))
						{
							const items = value.$each || [value];
							for(const id of items)
							{
								sub_arr.push({
									action: 'addComplex', 
									key: [search._id.toString(), col, key.slice(0,-2)].join('-'), // тут ключ с суффиксом ".l" (из-за синтаксиса mongo для обновления объекта)
									owner: this.conn.xaoc.id,
									value: id
								});
							}							
						}
					
						break;
						
					case '$pull':
					
						for(const [key, value] of Object.entries(updateValue))
						{
							const items = value.$in || [value];
							for(const id of items)
							{
								sub_arr.push({
									action: 'deleteComplex', 
									key: [search._id.toString(), col, key.slice(0,-2), id.toString()].join('-'), // тут ключ с суффиксом ".l" (из-за синтаксиса mongo для обновления объекта)
									owner: this.conn.xaoc.id,
									value: id
								});
							}
						}						
					
						break;
				}
			}
		}

		SYS.sendSubscriptions.call(this, sub_arr); // делаем call ради передачи this.endOfTransaction
	}
}

// !!! переписать на человеческие классы
const updateObjectProto = function(){};
exports.createUpdateMongoObject = function()
{
	const updateObject = new updateObjectProto();
	
	updateObject.__proto__.checkAndDeleteEmpty = function()
	{
		if(Object.keys(this.$set).length == 0) delete this.$set;
		if(Object.keys(this.$inc).length == 0) delete this.$inc;
		if(Object.keys(this.$addToSet).length == 0) delete this.$addToSet;
		if(Object.keys(this.$push).length == 0) delete this.$push;
		if(Object.keys(this.$pull).length == 0) delete this.$pull;
		if(Object.keys(this.$unset).length == 0) delete this.$unset;
		
		return Object.keys(this).length;
	}
	
	updateObject.$set = {};
	updateObject.$inc = {};
	updateObject.$addToSet = {};
	updateObject.$push = {};
	updateObject.$pull = {};
	updateObject.$unset = {};
	
	return updateObject;
}

exports.repairObjectId = function(o = {}, parent = {}, keys = []){
	if(typeof o != 'object')
	{
		if(
			['_id'].includes(keys[keys.length-1]) || 		// {_id: 'xxx'}
			['$each', '$in'].includes(keys[keys.length-2])	// {$each: []}, {$in: []}
		) parent[keys[keys.length-1]] = ObjectId(o);
	}
	else for(let key of Object.keys(o)) if(o[key] && o[key]._bsontype != "ObjectID") DB.repairObjectId(o[key], o, keys.concat(key));
}

var getMongo = function(callback){try{
	if(mongoSingle == undefined){
		MongoClient.connect(CONFIG.mongo.url, function(err, db) {try{
			assert.equal(null, err);
			mongoSingle = db.db(PROJECT);
			prepareMongo(mongoSingle);
			if(typeof callback == 'function') callback( mongoSingle );
		}catch(err){ console.log(err) } });
	}else{
		if(typeof callback == 'function') callback(mongoSingle);
	}
}catch(err){ console.log(err) } }
exports.getMongo = getMongo;

var getMongoCache = function(callback){try{
	if(mongoCacheSingle == undefined){
		MongoClient.connect(CONFIG.mongoCache.url, function(err, db) {try{
			assert.equal(null, err);
			mongoCacheSingle = db.db(PROJECT);
			mongoCacheSingle.close = (cb)=>{ if(cb) cb() };
			if(typeof callback == 'function') callback(mongoCacheSingle);
		}catch(err){ console.log(err) } });
	}else{
		if(typeof callback == 'function') callback(mongoCacheSingle);
	}
}catch(err){ console.log(err) } }
exports.getMongoCache = getMongoCache;

var dropMysqlDB = function(callback){
	DB.getMysql((mysql)=>{
		mysql.query("DROP DATABASE IF EXISTS `"+PROJECT+"`; CREATE DATABASE IF NOT EXISTS `"+PROJECT+"`;", ()=>{
			mysql.destroy();
			mysqlPool.end();
			mysqlPool = undefined;
			callback();
		});
	});
}
exports.dropMysqlDB = dropMysqlDB;

var getMysqlConn = function(callback){try{
	callback( mysqlLib.createConnection(CONFIG.mysql) );
}catch(err){ console.log(err); callback( false ); } }
exports.getMysqlConn = getMysqlConn;

var getMysql = function(callback){try{
	
	if(mysqlPool == undefined) mysqlPool = mysqlLib.createPool({...CONFIG.mysql, multipleStatements: true});
	
	mysqlPool.getConnection(function(err, connection){
		if(err){
			console.log('mysqlPool.getConnection err', err);
			callback(false);
		}else{
			connection.destroy = connection.release;
			if(typeof callback == 'function') callback(connection);
		}
	});	
}catch(err){ console.log('getMysql err', err); callback( false ); } }
exports.getMysql = getMysql;

var getMysqlAsync = function(){ return new Promise((resolve, reject)=>
{
	if(mysqlPool == undefined) mysqlPool = mysqlLib.createPool({...CONFIG.mysql, multipleStatements: true});
	
	mysqlPool.getConnection(function(err, connection){
		if(err){
			console.log('mysqlPool.getConnection err', err);
			reject(err);
		}else{
			connection.destroy = connection.release;
			
			connection.q = function(...arg){ return new Promise((resolve, reject)=>
			{
				arg.push((err,res)=>{
					if(err)
						reject(err);
					else
						resolve(res);
				})
				connection.query(...arg);
			}).catch(err=>{ throw err }) }
			
			resolve(connection);
		}
	});	
}).catch(err=>{ throw err }) }
exports.getMysqlAsync = getMysqlAsync;

var getMysqlLogs = function(callback){try{

	if(mysqlLogsPool == undefined) mysqlLogsPool  = mysqlLib.createPool(CONFIG.mysqlLogs);
	
	mysqlLogsPool.getConnection(function(err, connection){
		if(err){
			console.log(err);
			callback(false);
		}else{
			connection.destroy = connection.release;
			if(typeof callback == 'function') callback(connection);
		}
	});	
}catch(err){ console.log(err); callback( false ); } }
exports.getMysqlLogs = getMysqlLogs;

var getMysqlCache = function(callback){try{
	
	if(mysqlCachePool == undefined) mysqlCachePool  = mysqlLib.createPool(CONFIG.mysqlCache);
	
	mysqlCachePool.getConnection(function(err, connection){
		if(err){
			console.log(err);
			callback(false);
		}else{
			connection.destroy = connection.release;
			if(typeof callback == 'function') callback(connection);
		}
	});	
}catch(err){ console.log(err); callback( false ); } }
exports.getMysqlCache = getMysqlCache;

var getStaticMysql = function(conn, list, callback){try{

	var parallel = [];
	
	list.forEach((l)=>{
		const ll = l;
		parallel.push((cb)=>{ switch(ll){
			case 'mysql':
				if(conn.mysql){
					conn.mysql.staticCount = conn.mysql.staticCount + 1;
					cb()
				}else{
					DB.getMysql((mysql)=>{
						conn.mysql = mysql;
						conn.mysql.staticCount = 1;
						cb()
					});
				}
				break;
			case 'mysqlLogs':
				if(conn.mysqlLogs){
					conn.mysqlLogs.staticCount = conn.mysqlLogs.staticCount + 1;
					cb();
				}else{
					DB.getMysqlLogs((mysqlLogs)=>{
						conn.mysqlLogs = mysqlLogs;
						conn.mysqlLogs.staticCount = 1;
						cb()
					});
				}
				break;
			case 'mysqlCache':
				if(conn.mysqlCache){
					conn.mysqlCache.staticCount = conn.mysqlCache.staticCount + 1;
					cb()
				}else{
					DB.getMysqlCache((mysqlCache)=>{
						conn.mysqlCache = mysqlCache;
						conn.mysqlCache.staticCount = 1;
						cb()
					});
				}
				break;
			default: cb(); break;
		} });
	});
	
	async.parallel(parallel, ()=>{ callback() });
}catch(err){ console.log(err); callback( false ); } }
exports.getStaticMysql = getStaticMysql;

var destroyStaticMysql = function(conn, list, callback){try{

	var parallel = [];
	
	list.forEach((l)=>{
		const ll = l;
		parallel.push((cb)=>{ switch(ll){
			case 'mysql':
				conn.mysql.staticCount = conn.mysql.staticCount - 1;
				if(!(conn.mysql.staticCount > 0)){
					conn.mysql.destroy();
					conn.mysql = false;
				}
				cb();
				break;
			case 'mysqlLogs':
				conn.mysqlLogs.staticCount = conn.mysqlLogs.staticCount - 1;
				if(!(conn.mysqlLogs.staticCount > 0)){
					conn.mysqlLogs.destroy();
					conn.mysqlLogs = false;
				}
				cb();
				break;
			case 'mysqlCache':
				conn.mysqlCache.staticCount = conn.mysqlCache.staticCount - 1;
				if(!(conn.mysqlCache.staticCount > 0)){
					conn.mysqlCache.destroy();
					conn.mysqlCache = false;
				}
				cb();
				break;
			default: cb(); break;
		} });
	});
	
	async.parallel(parallel, ()=>{ callback() });
}catch(err){ console.log(err); callback( false ); } }
exports.destroyStaticMysql = destroyStaticMysql;

var accessLog = function(req, msg, alert, callback){try{
	
	getMysqlLogs((mysql)=>{try{
	
		var sql = '';
		
		if(req.headers == undefined) req.headers = 'no-data';
		
		if(msg){ // сокеты
			if(msg.data && msg.data.pswd) msg.data.pswd = '***';
			sql = "INSERT INTO `__access_socket` (add_time, ip, msg, headers, user, auth, session, alert) VALUES ('"+Date.now()+"', "+mysql.escape(req.headers['x-forwarded-for'] || '-')+", "+mysql.escape(JSON.stringify(msg.data))+", "+mysql.escape(JSON.stringify(req.headers))+", "+mysql.escape(msg.user||msg.data.user)+", "+mysql.escape(msg.auth||msg.data.auth)+", "+mysql.escape(msg.session||msg.data.session)+", "+mysql.escape(alert||'')+" )";
		}else{		
			sql = "INSERT INTO `__access_web` (add_time, ip, url, headers, alert) VALUES ('"+Date.now()+"', "+mysql.escape(req.headers['x-forwarded-for'] || req.connection.remoteAddress || '-')+", "+mysql.escape(req.url)+", "+mysql.escape(JSON.stringify(req.headers))+", "+mysql.escape(alert||'')+" )";
		}
		
		if(sql){
			mysql.query(sql, (err)=>{try{
				if(err) console.log(err, sql);
				mysql.destroy();
				if(typeof callback == 'function') callback();
			}catch(err){ console.log(err); if(typeof callback == 'function') callback() }});
		}else{
			if(typeof callback == 'function') callback();
		}
	}catch(err){ console.log(err); if(typeof callback == 'function') callback() }});
}catch(err){ console.log(err); if(typeof callback == 'function') callback() } }
exports.accessLog = accessLog;

exports.select = function(__, data, _callback, sql, escape){ return new Promise((resolve, reject)=>
{
	const callback = (err, data)=>
	{
		if(typeof _callback == 'function') _callback( ...(
			DB.asyncCB.indexOf(_callback.toString()) != -1 ? [] : [err, data] // для inline-вызова в async-функциях
		));
		else if(err) reject(err); else resolve(data);
	};

	var code = data;
	if(typeof data == 'object'){
		code = data.code;
		sql = data.sql;
		escape = data.escape;
	}
	
	DB.getMysql((mysql) => {
		
		if(!escape) escape = [];

		mysql.query(sql, escape, (err, rows, fields)=>{
			mysql.destroy();
			if(err){
				console.log(err);
				if(code){
					__.queryIds[code] = [];
					callback();
				}else{
					callback(null, []);
				}
			}else{
				var res = rows.map(r=>{
					var rr = r;
					if(rr._id == undefined){ // не забываем явно объявлять _id, например id as _id
						rr._id = true;
					}else{
						rr._id = ObjectId(rr._id);
					}
					return rr;
				});
				if(code){
					__.queryIds[code] = res;
					callback();
				}else{
					callback(null, res);
				}
			}
		});
	})
})}

exports.selectWithFilter = function(__, code, callback, sql, escape)
{
	const _ = __.fields[code];
	if(!_.filter) _.filter = {};
	
	if(_.filter.emptyOnLoad && _.filter.o == undefined){ // для показа hidden-списков, которые подгружаются только после снятия hidden
		__.queryIds[code] = [];
		return callback();
	}
	
	DB.getMysql(mysql=>{
		
		if(!escape) escape = [];
		
		if(_.filter.l < 0 && sql.toUpperCase().indexOf('ORDER BY') != -1) sql += ' DESC';
		if(_.filter.o == undefined) _.filter.o = 0;
		sql += ' LIMIT '+parseInt(_.filter.o)+','+Math.abs(_.filter.l||1);
		
		mysql.query(sql, escape, (err, rows, fields)=>{ mysql.destroy();
			if(err){
				console.log(err);
				__.queryIds[code] = [];
			}else{
				__.queryIds[code] = rows.map(r=>{
					var rr = r;
					if(rr._id == undefined) rr._id = true;
					return rr;
				});
				if(_.filter && _.filter.l < 0) __.queryIds[code].reverse();
			}
			callback();
		});
	})
}

exports.selectLogsWithFilter = function(__, code, callback, sql, escape){
	
	DB.getMysqlLogs((mysql) => {
		
		if(!escape) escape = [];
		
		var _ = __.fields[code];
		if(_.filter){
			if(_.filter.l < 0 && sql.toUpperCase().indexOf('ORDER BY') != -1) sql += ' DESC';
			if(_.filter.o == undefined) _.filter.o = 0;
			sql += ' LIMIT '+parseInt(_.filter.o)+','+Math.abs(_.filter.l||1);
		}
		
		mysql.query(sql, escape, (err, rows, fields)=>{
			mysql.destroy();
			if(err){
				console.log(err);
				__.queryIds[code] = [];
			}else{
				__.queryIds[code] = rows.map(r=>{
					var rr = r;
					if(rr._id == undefined) rr._id = true;
					return rr;
				});
				if(_.filter && _.filter.l < 0) __.queryIds[code].reverse();
			}
			callback();
		});
	})
}

exports.createColKeys = function(conn, data, callback){
	
	if(data.col)
	{
		DB.updateKeyFields(()=>{
			DB.getMysql(mysql=>
			{
				var created = {fields: {}, links: {}}, fieldsSQL = '', linkSQL = '';
				
				if(data.fields) data.fields.forEach(f=>
				{
					var ff = f.split('|');
					f = ff[0].replace(/\./g, '_');
					
					var ftype = "varchar(100) NOT NULL DEFAULT ''";
					if(ff[1] == 'int') ftype = "bigint(20) NOT NULL DEFAULT '0'";
					if(ff[1] == 'fulltext') ftype = "text";
					
					if(!(DB.KEY_FIELDS[data.col] && DB.KEY_FIELDS[data.col][f]))
					{
						if(!DB.KEY_FIELDS[data.col]) DB.KEY_FIELDS[data.col] = {};
						DB.KEY_FIELDS[data.col][f] = ff[2] == 'reverse' ? 'reverse' : true;
						created.fields[f] = true;
						
						fieldsSQL += "CREATE TABLE IF NOT EXISTS `"+data.col+"__"+f+"` ( `id` varchar(24) NOT NULL, ";
						fieldsSQL += "`f` "+ftype+", ";
						if(ff[2] == 'reverse') fieldsSQL += "`ff` "+ftype+", ";
						var indexSQL = "KEY `f` (`f`)";
						if(ff[1] == 'fulltext') indexSQL = "FULLTEXT `f` (`f`)";
						fieldsSQL += " KEY (`id`), "+indexSQL;
						if(ff[2] == 'reverse') fieldsSQL += ", KEY `ff` (`ff`)";
						fieldsSQL += ") ENGINE=InnoDB DEFAULT CHARSET=utf8; ";
					}
				});
				if(data.links) data.links.forEach((l)=>{ if(!(DB.KEY_LINKS[data.col+'_links'] && DB.KEY_LINKS[data.col+'_links'][l])){
					if(!DB.KEY_LINKS[data.col+'_links']) DB.KEY_LINKS[data.col+'_links'] = {};
					DB.KEY_LINKS[data.col+'_links'][l] = true;
					created.links[l] = true;
					linkSQL += "CREATE TABLE IF NOT EXISTS `"+data.col+'_links'+l+"` ( `id` varchar(24) NOT NULL, `p` varchar(24) NOT NULL DEFAULT '', KEY `id` (`id`), KEY `p` (`p`) ) ENGINE=InnoDB DEFAULT CHARSET=utf8; " 
				}});
						
				conn.db.collection(data.col).find().toArray((err, res)=>
				{
					if(res.length){
						if(data.fields){
							data.fields.forEach(f=>
							{
								var ff = f.split('|');
								f = ff[0];
								var fieldKey = f.split('.'), fieldVal;
								f = f.replace(/\./g, '_');
								
								if(created.fields[f])
								{
									fieldsSQL += "INSERT INTO `"+data.col+"__"+f+"` (`id`, `f`";
									if(ff[2] == 'reverse') fieldsSQL += ",`ff`";
									fieldsSQL += ") VALUES";
									
									res.forEach(r=>
									{
										fieldVal = r;
										for(var i = 0; i < fieldKey.length; i++) if(fieldVal) fieldVal = fieldVal[fieldKey[i]] || '';
										
										if(Object.prototype.toString.call( fieldVal ) === '[object Array]'){
											fieldVal.forEach(_=>{
												fieldsSQL += " ("+mysql.escape(r._id+'')+", "+ (_.v?mysql.escape(_.v+''):'DEFAULT')+"),";
											});
										}else{
											fieldsSQL += " ("+mysql.escape(r._id+'')+", "
												+ (fieldVal ? mysql.escape( typeof fieldVal == 'object' ? JSON.stringify(fieldVal) : fieldVal+'') : 'DEFAULT');
											if(ff[2] == 'reverse') fieldsSQL += ", "+(fieldVal?mysql.escape((fieldVal+'').split('').reverse().join('')):'DEFAULT');
											fieldsSQL += "),";
										}
									});
									
									fieldsSQL = fieldsSQL.substring(0, fieldsSQL.length-1) + '; ';
								}
							});
						}
						
						if(data.links){
							data.links.forEach((l)=>{
								if(created.links[l]){
									var insertSQL = '';
									res.forEach((r)=>{
										if(r[l]){
											r[l].l.forEach((p)=>{
												insertSQL += " ("+mysql.escape(r._id+'')+", "+mysql.escape(p+'')+"),";
											});
										}
									});
									if(insertSQL){
										linkSQL += "INSERT INTO `"+data.col+"_links"+l+"` (`id`, `p`) VALUES"+insertSQL;
										linkSQL = linkSQL.substring(0, linkSQL.length-1) + '; ';
									}
								}
							})
						}

					}

					if(data.force){
						
						var parallel = [];
						if(fieldsSQL) parallel.push((cb)=>{
							mysql.query(fieldsSQL, (err)=>{
								if(err && err.errno != 1062) console.log(err, 'fieldsSQL='+fieldsSQL);
								cb();
							});
						});
						if(linkSQL) parallel.push((cb)=>{
							mysql.query(linkSQL, (err)=>{
								if(err) console.log(err, 'linkSQL='+linkSQL);
								cb();
							});
						});
						
						async.parallel(parallel, (err, res)=>{
							mysql.destroy();
							callback({msg: JSON.stringify(created)});
						});
						
					}else{
						mysql.destroy();
						if(typeof callback == 'function') callback({err: fieldsSQL + ' ' + linkSQL});
					}
				});
			});
		});
	}else{
		if(typeof callback == 'function') callback({data: data, err: "Не указана коллекция"});
	}
}

exports.getIdFromParent = function(__, code, callback){try{ // callback от async - НЕ ДОЛЖЕН ВОЗВРАЩАТЬ ДАННЫЕ

	var field = __.fields[code];
	var parent = field.parent;
	var data = __.data[parent];
	var _id = undefined;
	
	if(data){
		if(field.link){
			 var a = {};
			 field.link.forEach((l)=>{
				 var d = data[l] ? data[l].l || data[l] : [], dd;
				 if(dd = (data[l]||{}).data){ // подставляем данные из data-хранилища внутри __[child_name]
					/*Object.keys(dd).forEach(key=>{
						Object.keys(dd[key]).forEach(id=>{
							d = d.map(_=>{ if(_+''==id){
								_ = {_id: _};
								_['>'+key] = dd[key][id];
							} return _ });
						});
					});*/
					Object.keys(dd).forEach(id=>{
						Object.keys(dd[id]).forEach(key=>{
							d = d.map(_=>{ if(_+''==id){
								_ = {_id: _};
								_['>'+key] = dd[id][key];
							} return _ });
						});
					});
				 }
				 if(d.length){
					 if(_id == undefined) _id = [];
					 d.forEach((i)=>{
						if(!a[i] || a[i] === true){ // без второго условия нельзя будет подставить несколько фейковых значений
							a[i] = true;
							_id.push(i);
						}
					 });
					 // _id = _id.concat(); если понадобятся неуникальные значения
				 }
			 });
		}
	}
	
	// добавление связанной сущности, если она обязательна
	if(_id == undefined && field.add == undefined && data && data._id != undefined 
		&& data._id != true // запрещает добавлять сущности к "зарезервированным" ID
		// можно попробовать заменить на false, но надо во всем движке это значение отслеживать, по аналогии с true
	){
		
		// "резервирует" ID под новую сущность
		if(field.link) field.link.forEach((l)=>{
			if(!data[l]) data[l] = {};
			if(!data[l].l) data[l].l = [];
			data[l].l.push(true);
		});
		
		if(__.process[field.linecode]){
			field.links = __.process[field.linecode].links;
			field.userLink = __.process[field.linecode].userLink;
		}
		
		DB.addComplex(__, field, {
			_id: data._id,
			col: __.fields[field.parent].col,
			link: __.fields[field.parent].link,
		}, (err, complex)=>{

			if(field.link) field.link.forEach((l)=>{
				if(!data[l]) data[l] = {l: []};
				data[l].l.push(complex._id);
			});
			
			if(typeof __.process[field.linecode].add == 'function'){
				__.process[field.linecode].add(__, field, parent, {_id: complex._id}, ()=>{ callback() });
			}else{ callback() }
		});
	}else{
		if(field.link && data[field.link]){
			__.queryIds[code] = _id || [];
		}else{
			__.queryIds[code] = (typeof _id != 'object') ? (_id ? [ _id ] : []) : _id;
		}
		callback();
	}	
}catch(e){console.log(e); callback();}}

exports.getCollection = function(__, code, callback){
	var _ = __.fields[code];
	if(_.itemCodes){
		__.queryIds[code] = Object.values(_.itemCodes);
		callback();
	}else{
		var q = __.db.collection(_.col).find( this.customFilter || {}, {fields: {_id: 1}});
		if(_.filter){
			if(_.filter.l) q = _.filter.l < 0 ? q = q.sort({add_time : -1}).limit(_.filter.l*1) : q = q.limit(_.filter.l);
			if(_.filter.o) q.skip(_.filter.o);
		}
		q.toArray((err, res)=>{
			__.queryIds[code] = (_.filter && _.filter.l && _.filter.l < 0) ? res.reverse() : res;
			callback();
		});
	}
}

exports.getContent = function(__, code, callback){
	__.fields[code].col = '__content';
	__.fields[code].link = ['__'+__.fields[code].col];
	__.db.collection(__.fields[code].col).find({}, __.queryFields[__.fields[code].linecode]).toArray((err, res)=>{
		__.queryIds[code] = res;
		callback();
	});
}

exports.addComplex = async function(){
	
	var actionLink = './db/addComplex.js';
	if(CONFIG.DEBUG) delete require.cache[require.resolve(actionLink)];
	var action = require(actionLink);
	
	if(action && typeof action.f == 'function'){
		return await action.f(...arguments);
	}else{
		callback();
	}
}

exports.deleteComplex = async function(){
	
	var actionLink = './db/deleteComplex.js';
	if(CONFIG.DEBUG) delete require.cache[require.resolve(actionLink)];
	var action = require(actionLink);
	
	if(action && typeof action.f == 'function'){
		return await action.f(...arguments);
	}else{
		callback();
	}
}

exports.moveComplex = async function(){
	
	var actionLink = './db/moveComplex.js';
	if(CONFIG.DEBUG) delete require.cache[require.resolve(actionLink)];
	var action = require(actionLink);
	
	if(action && typeof action.f == 'function'){
		return await action.f(...arguments);
	}else{
		callback();
	}
}

exports.saveField = function(){

	var actionLink = './db/saveField.js';
	if(CONFIG.DEBUG) delete require.cache[require.resolve(actionLink)];
	var action = require(actionLink);
	
	if(action && typeof action.f == 'function'){
		action.f(...arguments);
	}else{
		callback();
	}
}

exports.saveFields = function(){

	var actionLink = './db/saveFields.js';
	if(CONFIG.DEBUG) delete require.cache[require.resolve(actionLink)];
	var action = require(actionLink);
	
	if(action && typeof action.f == 'function'){
		action.f(...arguments);
	}else{
		callback();
	}
}

exports.redisKeys = function(show, showValues){
	DB.redis.keys('*', function (err, keys) {
		if(show){
			console.log('REDIS KEYS => '+keys.length);
			if(showValues){
				keys.forEach(key=>DB.redis.get(key, (err, val)=>{ console.log(key, '->', val) }));
			}else if(showValues === false){
				console.log(keys);
			}
		}
	});
}

cssList = {};
cssIndex = 0;
exports.__content = {};
exports.KEY_FIELDS = {};
exports.KEY_LINKS = {};
exports.KEY_CHILD = {};

exports.updateKeyFields = function(callback){
	getMysql((mysql)=>{
		var sql = "SELECT DISTINCT TABLE_NAME, count(*) count FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = ? GROUP BY TABLE_NAME";
		mysql.query(sql, [PROJECT], (err, rows, fields)=>{
			if(err) console.log(err);
			if(rows.length){
				rows.forEach((r)=>{
					if(r.TABLE_NAME.indexOf('_links') !== -1){
						var table = r.TABLE_NAME.split('__');
						if(!table[1] && table[2]) table[1] = '__'+table[2];
						if(exports.KEY_LINKS[table[0]] == undefined) exports.KEY_LINKS[table[0]] = {}
						exports.KEY_LINKS[table[0]]['__'+table[1]] = true;
						if(exports.KEY_CHILD[table[0]] == undefined) exports.KEY_CHILD[table[0]] = [];
						exports.KEY_CHILD[table[0]].push(table[1]);
					}else{
						var table = r.TABLE_NAME.split('__');
						if(!table[1] && table[2]) table[1] = '__'+table[2];
						if(table[0] && table[1]){
							if(exports.KEY_FIELDS[table[0]] == undefined) exports.KEY_FIELDS[table[0]] = {};
							exports.KEY_FIELDS[table[0]][table[1]] = r.count > 2 ? 'reverse' : true;
						}
					}
				});
			}
			mysql.destroy();
			callback();
		});
	});
}

exports.prepareGlobalData = function(callback){
	
	DB.getMongo((db)=>{
		
		db.collection('__content').findOne({}, {}, (err, __content)=>{
			
			if(!__content) __content = {};
			if(!__content.roleGroups) __content.roleGroups = {};
			if(!__content.fieldKeys) __content.fieldKeys = {};
			
			__content.col = '__content';
			if(!__content.lstBranch) __content.lstBranch = {};
			exports.__content = __content;
			
			SYS.updateBLOCK();
			SYS.updateLST();
			
			// для переиспользования функций фронта на бэке
			if(!CONFIG.prodcache){
				(fs.readdirSync(PROJECT_LINK+'/forms')||[]).filter(_=>_.indexOf('form') == 0 && _.indexOf('.js') != -1)
				.concat(Object.keys(BLOCK.form)||[]).forEach(_=>{
					SYS.requireFile("cache", _.replace('.js', '')+'_func'+'_admin'+'.js', true);
				});
			}
			
			// для определения параметров callback в DB.addComplex и DB.deleteComplex (для inline-вызова в async-функциях)
			// два вызова потому что внутри waterfall и parallel колбеки разные, но статичные
			async.waterfall([
				cb=>{ exports.asyncCB = []; exports.asyncCB.push(cb.toString()); cb(); },
				cb=>{ async.parallel([cb=>{ exports.asyncCB.push(cb.toString()); cb(); }], ()=>{ cb() }) },
				cb=>{ exports.updateKeyFields(()=>{ cb() }) },
			], callback);
		});
	});	
}

exports.subMarkup = function(w, msg){
	
	var actionLink = './db/subMarkup.js';
	if(CONFIG.DEBUG) delete require.cache[require.resolve(actionLink)];
	var action = require(actionLink);
	
	if(action && typeof action.f == 'function') action.f(w, msg);
}

exports.subMasterAction = function(w, msg){
	
	if(msg.waitForAccept){
		DB.session.set(msg.owner[0], 'waitForAccept_'+msg.waitForAccept, msg);
	}else{
		var actionLink = './db/subMasterAction.js';
		if(CONFIG.DEBUG) delete require.cache[require.resolve(actionLink)];
		var action = require(actionLink);
		if(msg.acceptCode){
			DB.session.get(msg.session, 'waitForAccept_'+msg.acceptCode, function(err, msg){
				if(action && typeof action.f == 'function') action.f(w, msg);
			});
		}else{
			if(action && typeof action.f == 'function') action.f(w, msg);
		}
	}
}

exports.subProcessAction = function(msg){
	
	var actionLink = './db/subProcessAction.js';
	if(CONFIG.DEBUG) delete require.cache[require.resolve(actionLink)];
	var action = require(actionLink);
	
	if(action && typeof action.f == 'function') action.f(msg);
}

exports.mongoClone = function(data, callback){
	
	var db = data.db;
	if(!data.newCol) data.newCol = data.col;
	
	if(data._id){
		db.collection(data.col).findOne({_id: ObjectId(data._id)}, (err, cloneData)=>{
			cloneData._id = ObjectId();
			db.collection(data.newCol).insert(cloneData, (err, newData)=>{
				callback(cloneData);
			});
		});
	}else{
		if(data.clone){
			var cloneData = Object.assign({}, data.clone);
			cloneData._id = ObjectId();
			db.collection(data.newCol).insert(cloneData, (err, newData)=>{
				callback(cloneData);
			});
		}else{
			callback();
		}
	}

}

exports.selectWithMultiParent = function(__, code, callback){

	var field = __.fields[code];
	if(field.itemCodes){
		__.queryIds[code] = Object.values(field.itemCodes);
		callback();
	}else{
		if(field.filter.multiParent){
			DB.getMysql((mysql) => {
				
				var from = [], where = [];
				
				field.filter.multiParent.forEach((p, i)=>{
					from.push("`"+field.col+"_links__"+p.col+"`");
					if(i != 0) where.push("`"+field.col+"_links__"+field.filter.multiParent[0].col+"`.id = `"+field.col+"_links__"+p.col+"`.id");
					where.push("`"+field.col+"_links__"+p.col+"`.p = "+mysql.escape(p._id+''));
				})
				
				var sql = "SELECT `"+field.col+"_links__"+field.filter.multiParent[0].col+"`.id _id FROM "+from.join(', ')+" WHERE "+where.join(' AND ');
				
				mysql.query(sql, (err, rows, fields)=>{
					mysql.destroy();
					if(rows && rows.length){
						__.queryIds[code] = rows;
						callback();
					}else{
						if(!err){
							if(field.add == undefined){
								DB.getMongo((db)=>{
									DB.addComplex({db: db, user: __.user},{col: field.col}, field.filter.multiParent, (err, complex)=>{
										__.queryIds[code] = [{_id: complex._id}];
										callback();
									});
								});
							}else{
								__.queryIds[code] = [];
								callback();
							}
						}else{
							console.log(err);
							__.queryIds[code] = [];
							callback();
						}
					}
				});
			});
		}else{
			__.queryIds[code] = [];
			callback();
		}
	}
}

exports.replaceRoleGroups = function(__, users, val, callback){
	
	var waterfall = [];
	
	for(var r in DB.__content.roleGroups){
		
		const u = DB.__content.roleGroups[r]+'';
		
		if(users[u] != undefined){
			
			delete users[u];
			
			waterfall.push((cb)=>{
				__.db.collection('user').findOne({_id: ObjectId(u)}, {__group_users: 1}, (err, group)=>{
					if(group && group.__group_users) group.__group_users.l.forEach(u=>(users[u] = val));
					cb();
				});
			});
		}
	}
	
	async.waterfall(waterfall, ()=>{ callback() });
}

function searchLinkParents(conn, managers, parents, p, col, id, cb){
	
	if(p && p[col] && p[col] !== true){
		
		var fields = {__manager: 1};
		for(var key in p[col]) fields['__'+key.split('|')[0]] = 1;
		
		var realCol = col.split('|');

		conn.db.collection(realCol[1] || realCol[0]).findOne({_id: ObjectId(id)}, fields, (err, field)=>{ if(field){
			var pp = [];
			for(var key in p[col]){
				var k = key.split('|');
				if(field['__'+k[0]] && field['__'+k[0]].l.length == 1){
					pp.push({key: key, col: k[1] || k[0], _id: field['__'+k[0]].l[0]});
				}
			}
			
			if(managers.length == 0 && field.__manager && field.__manager.l.length){
				field.__manager.l.forEach(m=>managers.push(m));
			}

			var parallel = [];
			
			pp.forEach((ppp)=>{
				const parent = ppp;
				parents.push({
					_id: parent._id,
					col: parent.col,
					link: '__'+parent.col,
					childLink: '__task',
				});
				parallel.push((cb)=>{
					searchLinkParents(conn, managers, parents, p[col], parent.key, parent._id, ()=>{ cb() });
				});
			});
			
			async.parallel(parallel, ()=>{ cb() });
			
			/*if(pp.length == 1){
				parents.push({
					_id: pp[0]._id,
					col: pp[0].col,
					link: '__'+pp[0].col,
					childLink: '__task',
				});
				searchLinkParents(conn, managers, parents, p[col], pp[0].key, pp[0]._id, cb);
			}else{ cb() }*/
		}else{ cb() } });
	}else{ cb() }
}
exports.searchLinkParents = searchLinkParents;

exports.requireFile = SYS.requireFile;
exports.getUploadLink = SYS.getUploadLink;

var session = {};
fs.readdirSync(__dirname+'/session').map((file)=>{
	session[file.replace('.js','')] = require(__dirname+'/session/'+file)[CONFIG.sessionCache || 'files'];
});
exports.session = session;

var contextProto = function(conn)
{	
	const self = this;
	self.conn = conn;
	
	self.save = [];
	self.saveAll = function (callback)
	{
		DB.saveFields(self.conn, self.save, () => {
			self.save = [];
			if (typeof callback == "function") callback();
		});
	};
	self.getParent = function (callback)
	{
		var res = {};

		DB.session.getParent(self.conn, SYS.get(self, this[0]), res, () =>
		{
			SYS.set(self, this[1], res.field);
			SYS.set(self, this[1] + ".parent", res.parent);

			if (typeof callback == "function") callback();
		});
	};
	
	self.find = async function(col, query, projection)
	{	
		const docs = await self.conn.db.find(col, query, projection) || [], result = [];
		
		docs.forEach(doc =>
		{
			if(self.data[doc._id])
			{
				Object.assign(self.data[doc._id]._meta.initial, SYS.cloneDeep(doc));
				Object.assign(self.data[doc._id], doc, {...self.data[doc._id]}); // мы не должны перетереть локальные изменения	
				// * при записи Object.assign(a,b,a) аргументы из второго "a" почему то менее приоритетны чем из "b"
			}else
			{
				doc._meta = {_id: doc._id, col: col, initial: SYS.cloneDeep(doc)};
				doc._meta.update = true; 
				self.data[doc._id] = doc;
			}
			
			result.push( self.data[doc._id] );
		});
		
		return result; // в docs не будет _meta, поэтому возвращаю новый массив
	}
	
	/*
		- не известно, есть ли объект в this.data (col и query указываются в явном виде)
		:: await this.findOne( [name], [_id], {...} );
		- объект уже добавлен в this.data и требуется только дополнить его из бд (col и query парсятся автоматически)
		:: await this.findOne.call(this.data[...], null, null, {...})
			* не убираю "null, null", так как в будущем может понадобится передача параметров для Object.assign()
		- объекта нет либо в this.data, либо в бд
		:: await this.findOne.call(this.data[...], [name], [_id], {...});
	*/
	self.findOne = async function(col, query, projection = {})
	{
		// наполняем существующий объект
		if(this !== self && this._meta){
			col = this._meta.col;
			query = this._meta._id;
		}

		// если вылезла ошибка "MongoError: collection name must be a String", то скорее всего метод должен быть вызван через findOne.call(...)
		const doc = await self.conn.db.findOne(col, query, projection);

		if(!(doc||{})._id) // объект может быть еще не создан
		{
			if(this._meta) // например, при наполнении объекта, добавленного в рамках этой же бизнес-транзации
				return this;
			else	
				throw new Error(`${col} not found with query=${query}`);
		}else
		{
			if(self.data[doc._id])
			{
				Object.assign(self.data[doc._id]._meta.initial, SYS.cloneDeep(doc));
				Object.assign(self.data[doc._id], doc, {...self.data[doc._id]}); // мы не должны перетереть локальные изменения
				// * при записи Object.assign(a,b,a) аргументы из второго "a" почему то менее приоритетны чем из "b"
			}else
			{
				doc._meta = {_id: doc._id, col: col, initial: SYS.cloneDeep(doc)};
				doc._meta.update = true; // объект можно только обновлять (иначе дефолтное событие - добавление)
				self.data[doc._id] = doc;
			}
			
			return self.data[doc._id];
		}
	}
	
	self.add = function(meta = null, parents = [])
	{
		// вызываем функцию через call(...), если хотим наполнить готовый объект метаданными
		const doc = this !== self ? this : {};
		
		if(!meta) throw new Error('empty meta');
		if(typeof meta == 'string') doc._meta = {col: meta};
		
		if(!doc._meta) doc._meta = meta;
		else if(meta.links) doc._meta.links = meta.links;
		if(typeof doc._meta != 'object') throw new Error('bad meta type');
		if(!doc._meta.initial) doc._meta.initial = {};
		if(!doc._meta._id) doc._meta._id = ObjectId();
		if(!doc._meta.update) doc._meta.insert = true; // нельзя добавлять объекты, которые уже есть в бд

		if(parents)
		{	
			if( !Array.isArray( parents ) ) parents = [ parents ];
			const f_name = doc._meta.name || doc._meta.col;
			const links = doc._meta.links || {};
			
			parents.filter(p=>p).forEach(p=>
			{	
				const p_meta = p._meta || p;
				const p_name = p_meta.name || p_meta.col;
				
				let p_key = (links[f_name]||{})[p_name];
				// ! - дефолтная ссылка, false - ссылку не делаем, true - дефолтная ссылка (добавлена в links для визуальной наглядности)
				if( (!p_key && p_key !== false) || p_key === true ) p_key = '__'+p_name;
				if(p_key)
				{
					if(!doc[p_key]) doc[p_key] = {col: p_meta.col, l: []};
					doc[p_key].l.push( p_meta._id );
				}
				
				let f_key = links[p_name];
				// ! - дефолтная ссылка, false - ссылку не делаем, true - дефолтная ссылка (добавлена в links для визуальной наглядности)
				if( (!f_key && f_key !== false) || f_key === true ) f_key = '__'+f_name;
				if(f_key)
				{
					if(!self.data[p_meta._id]) self.data[p_meta._id] = {_meta: p_meta};
					if(!self.data[p_meta._id][f_key]) self.data[p_meta._id][f_key] = {col: doc._meta.col, l: []};
					self.data[p_meta._id][f_key].l.push( doc._meta._id );
				}
				
				self.data[p_meta._id]._meta.update = true;
			});
		}
		
		self.data[doc._meta._id] = doc;
		
		return doc;
	}

	self.saveTransaction = async function(config = {})
	{
		const $db = {insert: {}, update: []};
		for(const value of Object.values(self.data))
		{
			const meta = {...value._meta};
			
			if(meta.insert)
			{
				delete value._meta; // удаляем только в этой ветке if, чтобы _meta не попада в БД (для $db.update _meta удалится в соответствующем for)

				value._id = meta._id;
				if(!$db.insert[meta.col]) $db.insert[meta.col] = [];
				$db.insert[meta.col].push( value );
			}else if(meta.update){
				$db.update.push( value );
			}
		}
		
		const mongo_arr = [], log_arr = [];
		
		for await(const [col, docs] of Object.entries($db.insert))
		{
			docs.forEach(doc =>
			{
				if(doc.add_time == undefined) doc.add_time = Date.now();
			});
			
			mongo_arr.push(["insertMany", col, docs, {}]);
		}
		
		for await(const doc of $db.update)
		{
			const 	meta = {...doc._meta}, 
					$update = DB.createUpdateMongoObject(),
					$reverseUpdate = DB.createUpdateMongoObject();
			
			delete doc._meta;

			for await(const [key, value = {}] of Object.entries(doc))
			{
				if(key.slice(0,2) == '__') // проверяем изменение связок с другими сущностями
				{
					const 	initialValue = (meta.initial||{})[key] !== undefined ? (meta.initial||{})[key] : {},
							curIds = (value.l||[]).join()||'',
							initialIds = (initialValue.l||[]).join()||'';
					
					if(curIds != initialIds)
					{
						const added = (value.l||[]).filter(id => !initialIds.includes(id.toString())) || [];
						const deleted = (initialValue.l||[]).filter(id => !curIds.includes(id.toString())) || [];
						
						if(added.length)
						{								
							$update.$addToSet[key+'.l'] = {$each: added};
							$reverseUpdate.$pull[key+'.l'] = {$in: added};
						}
						if(deleted.length)
						{
							$update.$pull[key+'.l'] = {$in: deleted};
							$reverseUpdate.$addToSet[key+'.l'] = {$each: deleted};
						}
					}
				}
				else // проверяем остальные параметры (не связки) измененного объекта
				{	
					const initialValue = (meta.initial||{})[key] !== undefined ? (meta.initial||{})[key] : null;
					
					if(JSON.stringify(value) != JSON.stringify(initialValue)) // значение поля изменилось
					{ 
						$update.$set[key] = value;
						$reverseUpdate.$set[key] = initialValue;
					}
				}
			}
			if(meta.initial) for await(const [key, value = {}] of Object.entries(meta.initial))
			{
				if(doc[key] == undefined)
				{
					if(key.slice(0,2) == '__')  // параметры-связки начинаются с "__"
					{
						$update.$pull[key+'.l'] = {$in: value.l};
						$reverseUpdate.$addToSet[key+'.l'] = {$each: value.l};
					}
					else
					{
						$update.$set[key] = null; // не проверял
						$reverseUpdate.$set[key] = value;
					}
				}
			}

			if($update.checkAndDeleteEmpty())
			{
				mongo_arr.push(["updateOne", meta.col, {_id: meta._id}, $update, $reverseUpdate]);
			}
		}
		
		if(config.debug)
		{
			console.log("save debug", moment().format(), self, {$db, log_arr, mongo_arr, KEY_FIELDS: DB.KEY_FIELDS, KEY_LINKS: DB.KEY_LINKS});
			return;
		}
		
		let endOfTransactionCounter = 0;
		for(const [type, ...arg] of mongo_arr)
		{
			endOfTransactionCounter++;
			
			const $reverseUpdate = arg.pop();
			if(self.conn.db[type])
			{
				await self.conn.db[type].call({conn: this.conn, oldValue: $reverseUpdate, endOfTransaction: mongo_arr.length == endOfTransactionCounter}, ...arg ).catch(e=>{
					throw e;
				});
			}
			if($reverseUpdate && $reverseUpdate.checkAndDeleteEmpty && $reverseUpdate.checkAndDeleteEmpty())
			{
				//arg[arg.length - 1] = $reverseUpdate;
				arg.splice(2, 0, $reverseUpdate);
				arg.unshift(type);
				log_arr.push(arg);
			}
		}
		
		if(config.log)
		{
			if(config.log.target._id)
			{
				const updateLog = [config.log.target.col, {_id: config.log.target._id}, {$addToSet: {[config.log.path]: JSON.stringify(log_arr)}}];
				await self.conn.db.updateOne.call(this, ...updateLog ).catch(e=>{
					throw e;
				});
			}
		}
	}
	
	self.dataFind = function(query)
	{
		return SYS.dataFind(this.data, query);
	}
	self.dataFilter = function(query)
	{
		return SYS.dataFilter(this.data, query);
	}
	
	self.data = {};
	if(!self.transaction) self.transaction = [];
	self.contextPrepared = true;
}
function createContext(conn)
{
	const context = new contextProto(conn);
	return context;
}
exports.createContext = createContext;