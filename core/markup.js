
var formProto = function() {}
var complexProto = function() {}
var fieldProto = function() {}

form = function(data, callback){

	var __ = new formProto, result = false;

	__.theme = SYS.get(data, 'conn.user.config.theme') || CONFIG.theme;// || PROJECT;
	__.form = data.name.replace('__tpl~', '__tpl'+(__.theme?('/'+__.theme):'')+'~');

	if(CONFIG.DEBUG && !(BLOCK.form[__.form+'.js'] || fs.existsSync(PROJECT_LINK+'forms/'+__.form+'.js')))
		result = {err : 'Отсутствует шаблон '+__.form};
	
	if(this.__){
		var code = __nextId(this.__);
		DB.session.set(this.__.user.session, code, {form: __.form});
		return {type: 'subform', code: code, form: __.form, history: data.history};
	}else{
		__.conn = data.conn;
		__.db = data.db;
	}
	
	if(CONFIG.DEBUG){
		if(!__.conn.user) __.conn.user = {};
		if(!__.conn.user.access) __.conn.user.access = {};
	}
	
	__.user = __.conn.user;

	if(__.conn.user.editMode) __.editMode = true;
	__.user.forms[__.form] = {form: __.form, count: 1, code: data.code, t: Date.now(), sub: {}, copyLinks: {}};

	__.codePrefix = data.code ? data.code+'_' : '';
	
	__.lvl = {0:[__.codePrefix+1]};
	__.parents = {};
	__.fields = {0:{}};

	__.process = {el:{}};
	__.queryIds = {};
	__.queryFields = {};
	__.queryFieldsCustom = {};
	__.front = {};
	__.frontKeys = {};
	__.script = [];
	__.style = [];
	//__.lst = {addobj: new Date().toISOString(), labels: new Date().toISOString()};
	__.lst = {};
	__.global = {};
	__.html = {el:{}};

	try{

		var sfx = __.user.role ? '_'+__.user.role : '';
		if(__.theme) sfx += '_'+__.theme;
		
		__.filename = PROJECT_LINK+'forms/cache/'+__.form+sfx+'.js';
		var newCache = false, debugRequire = false;
		
		if(CONFIG.DEBUG){			
			try{
				delete require.cache[require.resolve(__.filename)];
			}catch(err){
				if(err.code == 'MODULE_NOT_FOUND'){
					newCache = true;
				}else{
					result = {err: 'Ошибка загрузки модуля '+__.filename};
					console.log(err);
				}
			}
			debugRequire = true;
		}
		
		if(result === false && (newCache || debugRequire || CONFIG.prodcache)){
			
			function replaceNewStyleFunction(s){
				var k = s.indexOf(')');				
				if(s[k+1]+s[k+2] == '=>'){
					if(s[k+3] == '{'){ // тут надо предусмотреть тот случай, когда скобку перенесли на новую строчку
						s = 'function'+s.substr(0,k+1)+s.substr(k+3);
					}else{
						s = 'function'+s.substr(0,k+1)+'{ return '+s.substr(k+3)+' }';
					}
				}
				return s;
			}
			
			function escapeRegExp(str) { return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1") }
			
			function tplToCache(proc, key){
				var result = '', style = '';
				
				
				const tmpProc = Object.assign({}, proc);
				if(key) // такое себе решение, но работает...
					if(!loadedHTML[key]) loadedHTML[key] = true;
					else{
						delete tmpProc['style'];
						delete tmpProc['script'];
						delete tmpProc['func'];
						delete tmpProc['stringFunc'];
					}
				for(var j in tmpProc){
					
					if(j == 'style' || j == 'script' || j == 'func' || j == 'stringFunc'){
						if(j == 'stringFunc') funcFile += tmpProc[j].trim()+"\n\n";
						if(j == 'style') style += tmpProc[j].toString().trim().substr(7).slice(0, -3).trim()+"\n\n";
						if(j == 'script' || j == 'func'){
							
							result += "exports.process['"+i+"']['"+j+"'] = {}\n";
							
							if(typeof tmpProc[j] == 'function') tmpProc[j] = [tmpProc[j]];
							tmpProc[j].forEach((f)=>{
								if(j == 'func'){
									funcFile += f.toString().trim().substr(5).slice(0, -1).trim()+"\n\n";
								}else
								{
									let scriptText = f.toString().trim().match(/{([\s\S]*)}/gm)[0].trim();
									let scriptCode = md5(scriptText);
									if((f.config||{}).sfx) scriptCode += '_'+f.config.sfx;
									
									scriptFile += `
										((data) => ${scriptText}).call(null,
											JSON.parse(document.querySelector('[script_code="${scriptCode}"]')?.dataset?.script || '{}'), 
											JSON.parse(document.querySelector('[script_code="${scriptCode}"]')?.dataset?.config || '{}')
										);\n\n
									`;
								}
							});
						}
					}else{ // тут можно генерить css-классы налету

						var s = tmpProc[j] ? tmpProc[j].toString() : '', p, p1, p2, css, code;

						if(j == 'tpl'){
							
							p = s.indexOf("`css");
							while ( p != -1 ) {
								
								p2 = s.indexOf("`", p+4);
								
								code = md5(s.substring(p+4, p2));
								
								if(cssList[code] == undefined){
									cssIndex++;
									cssList[code] = cssIndex*1;
								}
								
								code = '_'+cssList[code]+'_';
								css = s.substring(p+4, p2).replace(/\*css\*/g, code);
								
								if(styleIndex[code] == undefined){
									
									styleIndex[code] = css;
									
									if(css.indexOf(code) === -1){ // упрощенная запись css
										style += '.'+code+' {'+css+"\n}\n\n";
									}else{
										style += css+"\n\n";
									}
								}
								s = s.substr(0, p) + '" '+code+'"' + s.substr(p2+1);

								p = s.indexOf("`css", p+1);
							}
						
							p = s.indexOf("*css*");
							
							while ( p != -1 ) {

								p1 = s.indexOf("/*css", p);
								p2 = s.indexOf("css*/", p1);
								code = md5(s.substring(p1+5, p2));
								if(cssList[code] == undefined){
									cssIndex++;
									cssList[code] = cssIndex*1;
								}
								code = '_'+cssList[code]+'_';
								css = s.substring(p, p2).replace(/\*css\*/g, code);
								if(styleIndex[code] == undefined){
									styleIndex[code] = css;
									if(css.indexOf(code, 4) === -1){ // упрощенная запись css
										style += '.'+code+' {'+css.substr(css.indexOf('/*css')+5)+"\n}\n\n";
									}else{
										style += css.substr(css.indexOf('/*css')+5)+"\n\n";
									}
								}
								s = s.substr(0, p) + css + s.substr(p2);
								p = s.indexOf("*css*", p2+1);
							}
							
						}else{
							// theme, config, links, custom actions and etc
							if(typeof tmpProc[j] == 'object') s = JSON.stringify(tmpProc[j]);
						}
						
						result += "exports.process['"+i+"']['"+j+"'] = "+s+"\n";
					}
				}
				
				// эксперимент с именованием внутренних путей внутри темы
				//if(proc.theme) style = style.replace(/\/BLOCK\//g, '/blocks/'+proc.theme[0]+'/');

				styleFile += style;
				
				return result;
			}
			
			__.pre = true;
			
			c(Object.assign({__: __, name: __.form, type: 'form'}, (BLOCK.form[__.form+'.js']||{}).complexData||{} ));
			
			function pushTplToFront(key){
				
				key = key.replace('++', '+');
				
				// передаем hideError только для базовых элементов, отсутствие элементов внутри тем - нормальное явление
				if(__.html.el[key] = DB.requireFile('html', key, key.indexOf('__tpl/') != -1)){

					if(SYS.get(__.html.el[key], 'config.depend.length')){
						__.html.el[key].config.depend.forEach(k=>{ pushTplToFront(k) });
					}
					
					if(__.html.el[key].empty){
						delete __.html.el[key];
					}else{
						if(!__.html.el[key].front) __.html.el[key].front = {};
						__.html.el[key].front.tpl = __.html.el[key].tpl;
						
						/* 
							не уверен, ради каких случаев было сделано добавление к строке,
							но такое решение точно ломает prodcache (спам лишних блоков)
						*/
						//__.html.el[key].stringFunc = (__.html.el[key].stringFunc||'')+`\n\n
						__.html.el[key].stringFunc = `
							window.el['`+key+`'] = {`+
							Object.keys(__.html.el[key].front).map(
								k => k+':'+__.html.el[key].front[k].toString()
							)+`}
						`;
						
						// если элемент переиспользует часть функционала другого элемента, то его тоже нужно подргузить
						(( __.html.el[key].front.tpl.toString() + (__.html.el[key].front.prepare||'').toString() )
						.match(/window\.el\[[',"]__tpl(.*)[',"]\]\./g)||[])
						.forEach(k=>{
							const includeKey = k.substr(11).substr(0, k.length - 11 - 3);
							if(key != includeKey) // иначе может возникнуть рекурсия (см. prepareCustom в el~token для core_game)
								pushTplToFront( includeKey );
						});
					}
				}
			}
			
			Object.keys(__.html.el).forEach(key=>{ pushTplToFront(key) });
			
			var cacheFile = '', funcFile = '', scriptFile = '', styleFile = '', styleIndex = {}, loadedHTML = {};

			cacheFile += "exports.config = "+JSON.stringify( SYS.get(__, "process['.'].config") );
			cacheFile += "\n\n";
			cacheFile += "exports.fields = "+JSON.stringify(__.queryFields);
			cacheFile += "\n\n";
			cacheFile += "exports.lst = "+JSON.stringify(__.lst);
			cacheFile += "\n\n";
			cacheFile += "exports.process = {}\n";
			cacheFile += "exports.html = {}\n";
			for(var i in __.process) cacheFile += "exports.process['"+i+"'] = {}\n";

			cacheFile += "\n";
			
			for(var i in __.process)
			{
				cacheFile += tplToCache(__.process[i]);
				
				if(__.html[i])
				{
					cacheFile += "exports.html['"+i+"'] = {}\n";
					for(var ii in __.html[i])
					{
						cacheFile += "exports.html['"+i+"']['"+ii+"'] = {}\n";
						cacheFile += tplToCache(__.html[i][ii], ii).replace(new RegExp(escapeRegExp("exports.process['"+i+"']"), "g"), "exports.html['"+i+"']['"+ii+"']");
					}
				}
				cacheFile += "\n";
			}
			
			for(var s in __.script){
				var ss = __.script[s].toString();
				cacheFile = cacheFile.replace(new RegExp(escapeRegExp(ss), "g"), '"f_'+s+'"');
				funcFile += 'window.f_'+s+' = '+replaceNewStyleFunction(ss).trim()+"\n\n";
			}

			for(var s in __.style){
				styleFile += __.style[s].toString().trim().substr(7).slice(0, -3).trim()+"\n\n";
			}

			if(__.form.indexOf('/') != -1){ // blocks
				var dir = __.form.split('/');
				if(!fs.existsSync(PROJECT_LINK+'forms/cache/'+dir[0])) fs.mkdirSync(PROJECT_LINK+'forms/cache/'+dir[0]);
			}

			styleFile += "#style_catcher_"+(data.code||'')+" { height: 1px; }"
			
			fs.writeFileSync(PROJECT_LINK+'forms/cache/'+__.form+'_func'+sfx+'.js', funcFile);
			fs.writeFileSync(PROJECT_LINK+'forms/cache/'+__.form+sfx+'.js', cacheFile);			
			fs.writeFileSync(PROJECT_LINK+'forms/cache/'+__.form+'_script'+sfx+'.js', scriptFile);
			fs.writeFileSync(PROJECT_LINK+'forms/cache/'+__.form+'_style'+sfx+'.css', styleFile);
			
			// для переиспользования функций фронта на бэке (дубль того что в вызывается на старте сервера в SYS.prepareGlobalData)
			SYS.requireFile("cache", __.form+'_func'+sfx+'.js');

			delete __.pre;
			
			// если что, тут данных для замены DB.__content.fieldKeys нет :(
			// внутри "__" только то что отрисовано в шаблонах, а есть данные, которые видны только на уровне бизнес-логики
			if(CONFIG.prodcache) result = {msg: 'prodcache ready'};
		}
		// убрал из под предыдущего if, иначе на бэке пропадают скрипты фронта
		SYS.requireFile("cache", __.form+'_func'+sfx+'.js'); // для переиспользования функций фронта на бэке

		var cache = require(__.filename);
		
		__.access = cache.access;
		__.process = cache.process;
		__.html = cache.html;
		__.lst = cache.lst;
		__.queryFields = cache.fields;
		__.queryFieldsCustom = {};

		if(SYS.get(cache, 'config.theme')) __.theme = cache.config.theme;

	}catch(err){
		result = {err: 'Ошибка обработки cache-файла '+(__.filename||'').split('/').pop()};
		console.log(err);
	}

	__.data = {};
	
	function sendResult(result){
		if(typeof callback == 'function' && result){
			__.db.close();
			callback(result);
		}		
	}

	if(result === false)
	{
		if(__.process['.'] && __.process['.'].tpl)
		{
			function show()
			{
				c(Object.assign({__: __, name: __.form, type: 'form', theme: __.theme}, (BLOCK.form[__.form+'.js']||{}).complexData||{} ));
				sendForm(__, callback);
			}
			
			if(typeof __.process['.'].access == 'function')
			{
				__.process['.'].access(__, {}, (err, data)=>{
					if(err){
						sendResult(Object.assign({badForm: true}, err === true ? {err : 'Недостаточно прав для работы с формой '+__.form} : err));
					}else{
						if((data||{}).editMode) __.editMode = true;
						show();
					}
				});
			}else{
				show();
			}
		}else{
			sendResult({err : 'Отсутствует шаблон '+__.form});
		}
	}else{
		sendResult(result);
	}
}
exports.form = form;

c = function(data, tpl, config){
	
	if(typeof data != 'object') data = {name: data};
	if(typeof tpl != 'function') config = tpl;
	if(config == undefined) config = {};
	
	var complex = new complexProto;
	
	complex.__ 		= data.__ || this.__;	
	complex.editMode = data.editMode || this.editMode || complex.__.editMode;
	
	if(complex.editMode){
		for(var key in data.edit) data[key] = data.edit[key];
	}

	complex.name 	= data.type == 'form' ? data.col : (data.name || data.col);
	complex.col 	= data.col || data.name;
	complex.parent 	= this.code || data.code;
	complex.title	= data.title;
	complex.type 	= data.type || 'complex';
	complex.code 	= data.itemCode || __nextId(complex.__);
	
	// наполняем именно data.process, так как он дополняет cache формы своими данными
	if(!data.process) data.process = {};
	if(!data.process.links) data.process.links = {
		[complex.name]: {[this.name]: '__'+this.name}, 
		[this.name]: '__'+complex.name
	};
	complex.links	= data.links || data.process.links;
	
	// !!! может сломать старые проекты (массово не тестировал)
	data.link = data.type == 'form' ? '__'+complex.col : complex.links[(complex.__.fields[complex.parent]||{}).name] || data.link;
	
	complex.link 	= data.link ? typeof data.link == 'object' ? data.link : [data.link] : ['__'+complex.name]; // link для формы переустанавливается  там же, где и col (в exports.id)

	complex.localConfig = config;
	complex.config = data.config||{}; // config должен быть всегда
	complex.filter 	= Object.assign({}, data.filter);
	if(Object.keys(complex.filter) == 0) delete complex.filter; // для простой записи проверки в шаблонах (complex.filter || {...})

	complex.theme = data.theme || complex.__.theme;
	if(complex.theme){
		complex.__.html.el['__tpl/'+complex.__.theme+'~complex_block'] = true;
		complex.__.html.el['__tpl/'+complex.__.theme+'~complex_item'] = true;
	}
	complex.__.html.el['__tpl~complex_block'] = true;
	complex.__.html.el['__tpl~complex_item'] = true;

	if(data.deleteMysqlData || complex.deleteMysqlData) complex.deleteMysqlData = true; // удалять данные из mysql (кроме ссылок)
	if(data.deleteLinks || complex.deleteLinks) complex.deleteLinks = true; // удалять только ссылки на сущности
	if(data.deleteNotDB || complex.deleteNotDB) complex.deleteNotDB = true; // удалять ссылку, если сущность удалена
	
	if(data.front != undefined) complex.front = data.front;
	if(data.add != undefined) complex.add = data.add;
	if(data.sub != undefined) complex.sub = data.sub;
	if(complex.sub && !complex.add) complex.add = 'sub';
	//if(data.controls != undefined || complex.add) complex.controls = data.controls || (complex.add ? 'delete' : '');
	if(data.controls != undefined || complex.add) complex.controls = data.controls || (complex.add ? {delete: true} : '');
	
	complex.nullParent = SYS.get(data, 'process.parentDataNotRequired');
	
	if((complex.add||{}).lst || (complex.add+'').indexOf('lst|') == 0){
		var lst = complex.add.lst || (complex.add+'').split('|')[1];
		var l = SYS.getList(complex.__, lst);
		if(l) complex.__.lst[lst] = l.date;
	}

	complex.__.fields[complex.code] = complex;
	
	complex.form 	= form;
	complex.c 		= c;
	
	complex.func = (f)=>{
		if(complex.__.pre === true){
			if(complex.__.process[complex.linecode].func == undefined) complex.__.process[complex.linecode].func = [];
			if(typeof complex.__.process[complex.linecode].func == 'function') complex.__.process[complex.linecode].func = [complex.__.process[complex.linecode].func];
			complex.__.process[complex.linecode].func.push(f);
			f(); // внутри f могут быть объявлены переиспользуемые функции, которые будут вызваны внутри шаблона
		}
		return [];
	};
	complex.script = (f, outData, config) =>
	{
		const data = {name: 'script', type: 'json'};
		data.themeType = data.type+'';
		
		if(complex.editMode){
			if(data.themeType[data.themeType.length-1] == '-'){
				data.themeType = data.themeType.substring(0, data.themeType.length - 1);
			}else{
				if(data.themeType[data.themeType.length-1] != '+') data.themeType += '+';
			}
		}
		
		if(!data.theme) data.theme = complex.__.theme;
		data.themePath = '__tpl'+(data.theme?('/'+data.theme):'')+'~el_'+data.themeType;
		
		// тут в data.themePath все еще есть -/+, то есть должен быть соответствующий файл/блок файла в __tpl
		if(BLOCK.html[data.themePath+'.js'] === undefined) data.themePath = '__tpl~el_'+data.themeType;
		
		if(complex.__.pre === true)
		{
			complex.__.html.el[data.themePath] = true;
			
			// примерное решение для подгрузки шаблонов с учетом editMode - желательно переделать
			// >>>
				if(data.themePath[data.themePath.length-1] != '-'){
					complex.__.html.el[data.themePath+'+'] = true;
				}else{
					complex.__.html.el[data.themePath.substring(0, data.themePath.length - 1)] = true;
				}
			// <<<
			
			if(complex.__.process[complex.linecode].script == undefined) complex.__.process[complex.linecode].script = [];
			if(typeof complex.__.process[complex.linecode].script == 'function') complex.__.process[complex.linecode].script = [complex.__.process[complex.linecode].script];
			f.config = config;
			complex.__.process[complex.linecode].script.push(f);
		}
		else
		{
			field = fieldBase(complex, data);
			field.config = config;
			const sfx = (field.config||{}).sfx ? '_'+field.config.sfx : '';

			complex.__.fields[field.code] = field;
			complex.__.lvl[complex.__.sendLvl].push(field.code);
			
			if(complex.__.parents[complex.code] == undefined) complex.__.parents[complex.code] = [];
			complex.__.parents[complex.code].push(field.code);
			
			complex.__.data[complex.code+'_script'+sfx] = outData;
			let scriptCode = md5(f.toString().trim().match(/{([\s\S]*)}/gm)[0].trim()) + sfx;
			
			return ['div', {script_code: scriptCode}, [
				['div', {code: field.code}]
			]];
		}
	};
	
	complex.if = xIF.bind(complex);
	
	complex.html = xHTML.bind(complex);

	complex.f = xFIELD.bind(complex);
	
	if(complex.__.pre === true){

		complex.linecode = this.code == undefined ? '.' : complex.__.fields[this.code].linecode;
		if(complex.__.queryFields[complex.linecode] == undefined) complex.__.queryFields[complex.linecode] = {};
		complex.__.queryFields[complex.linecode]._id = 1; // без этого не воспринимает slice и забирает весь объект
		
		var slice = complex.filter && complex.filter.l < 0 ? {$slice: [complex.filter.l, -1*complex.filter.l]} : 
					complex.filter && complex.filter.l > 0 ? {$slice: [0, complex.filter.l]} : 
					false;
		
		if(!complex.nullParent){
			complex.link.forEach(l=>{
				complex.__.queryFields[complex.linecode][l+'.l'] = slice || 1;
				complex.__.queryFields[complex.linecode][l+'.data'] = 1;
			});
		}
		
		if(this.code != undefined)
		{
			const linecodeSfx = '__' + complex.name;
			if(!complex.__.process[complex.linecode].childLinecode) complex.__.process[complex.linecode].childLinecode = {};
			complex.__.process[complex.linecode].childLinecode[ complex.name ] = complex.linecode + linecodeSfx;
			complex.linecode += linecodeSfx;
		}
		if(complex.__.queryFields[complex.linecode] == undefined) complex.__.queryFields[complex.linecode] = {};

		if(complex.__.process[complex.linecode] == undefined) complex.__.process[complex.linecode] = {};
		if(typeof tpl == 'function') complex.__.process[complex.linecode].tpl = tpl;
		
		if(complex.filter) complex.__.process[complex.linecode].filter = JSON.stringify(complex.filter);
		
		if(complex.__.process[complex.linecode].loaded !== true){
			
			complex.__.process[complex.linecode].loaded = true;
			if(data.type) Object.assign( complex.__.process[complex.linecode], DB.requireFile(data.type, data.type == 'form' ? data.name : complex.name) );
			
			// без этого при предварительном проходе theme в дочерних блоках не поменяется
			if(complex.type == 'form' && SYS.get(complex.__.process[complex.linecode], 'config.theme')){
				complex.__.theme =  complex.__.process[complex.linecode].config.theme;
			}
		}
		
		if(data.process) Object.assign( complex.__.process[complex.linecode], data.process );

		if(data.front){
			if(complex.__.front[complex.linecode] == undefined) complex.__.front[complex.linecode] = {};
			var funcList = {};
			for(var key in data.front){
				var s = data.front[key].toString();
				var ss = md5(s);
				complex.__.script[ss] = data.front[key];
				funcList[key] = ss;
			}
			complex.__.front[complex.linecode][complex.name] = funcList;
		}
		
		var proc = complex.__.process[complex.linecode];
		if(proc.func) proc.func(); // внутри func могут быть объявлены переиспользуемые функции, которые будут вызваны внутри шаблона
		
		if(tpl != 'function' && proc && typeof proc.tpl == 'function') tpl = proc.tpl;
		if(typeof tpl == 'function') runTplFuncInNewContext(complex, tpl, complex, {}, {runTplFuncInNewContext_caller: 'complex'});
		
		return false;
		
	}else{
		
		if(complex.parent){
			if(complex.__.parents[complex.parent] == undefined) complex.__.parents[complex.parent] = [];
			complex.__.parents[complex.parent].push(complex.code);
		}
		complex.linecode = this.code ? complex.__.fields[this.code].linecode : '.';
		
		if(this.code != undefined && !data.recursive) complex.linecode += '__'+complex.name;
		if(data.code != undefined) complex.linecode = complex.__.fields[data.code].linecode;
		
		if(data.__ == undefined)
		{ // вызов _.c({...}) внутри tpl() (не для form, и не для complex-item)
			if(data.sendLvl)
			{ // двигает обработчик на уровени вверх (чтобы совместить однородные запросы в базу)
				if(!complex.__.lvl[complex.__.sendLvl+data.sendLvl*1]) complex.__.lvl[complex.__.sendLvl+data.sendLvl*1] = [];
				complex.__.lvl[complex.__.sendLvl+data.sendLvl*1].push(complex.code);
			}else{
				complex.__.lvl[complex.__.sendLvl+(data.sendLvl||0)].push(complex.code);
			}
		}
		
		// config.tag не будет работать, если в item'е массив элементов, обертка итема должна быть одинарной
		var result = [config.tag || 'div', {name: complex.name, code: complex.code, title: complex.title, complex: config.item ? 'item' : 'block', add: complex.add, controls: complex.controls, front: complex.front, fi: config.fi, li: config.li, lli: config.lli, editMode: complex.editMode, theme: complex.theme}];
		
		if(complex.filter){
			if(complex.filter.l != undefined) result[1].l = complex.filter.l+'';
			result[1].showOnButton = complex.filter.showOnButton;
			result[1].showOnScroll = complex.filter.showOnScroll;
		}
		
		return result;
	}
}
exports.c = c;

function fieldBase(complex, data){

	var field 		= new fieldProto;
	//field.type 		= 'input';
	
	for(var k in data) field[k] = data[k];

	field.parent 	= complex.code;
	field.code 		= __nextId(complex.__);
	
	if(field.keyvalue == undefined) field.keyvalue = field.name;
	
	// надо включить, если отключу передачу field.name на front
	// !field.label для случаев, если name содержит ObjectId и нужно его подменить
	// if((field.type == '*' || field.type == '*+') && !field.label) field.label = field.name;
	
	if(complex.editMode && field.stype == undefined){
		if(field.type[field.type.length-1] == '-'){
			if(field.type != 'json-') field.type = field.type.substring(0, field.type.length - 1);
		}else{
			if(field.type[field.type.length-1] != '+') field.type += '+';
		}
	}

	return field;
}

function __nextId(__){try{
	
	// __.user.forms[__.form] не всегда существует - надо проверить 
	//		- 	один из выявленных случаев, когда головную форму начинают рисовать раньше, чем закончила рисоваться первая (в рамках одного conn). Может быть вызвано спамом reloadForm фронте.
	//		- второй случай, когда прямо перед locationQuery (к новой форме) приходит sub (addComplex для блока старой формы), при этом внутри process.js эта форма все еще видна, а внутри markup.js уже нет (по итогам добавил проверку "if(!__.user.forms[__.form])" в function sendForm)
		
	var id = __.codePrefix + (__.user.forms[__.form].count++);
	__.fields[id] = {};
	return id;
}catch(e){ return null }}

function sendForm(__, callback)
{
	if(!__.user.forms[__.form]){
		if(typeof callback == 'function') callback({err: 'Изменилась форма'});
		return;
	}
	
	if(callback && !callback.codes) callback.codes = [];
	
	var lvl = __.sendLvl || 0;
	var funcId = [];
	
	__.lvl[lvl].forEach((code)=>{
		
		var complex = __.fields[code];
		
		if(complex.c != undefined){

			var proc = __.process[complex.linecode], f;

			if(proc && typeof proc.id == 'function') f = (__, code, callback)=>{ proc.id(__, code, callback) };
			
			if((proc && !f) || __.addComplex) f = (__, code, callback)=>{ DB.getIdFromParent(__, code, callback) };
			
			// reloadItem с фронта - мы уже забрали _id из кэша в showComplex (process.js)
			if(complex.itemCodes) f = (__, code, callback)=>{
				for(var c in __.fields[code].itemCodes) __.queryIds[code] = [ __.fields[code].itemCodes[c] ];
				callback();
			}
			if(f) funcId.push( f.bind( null, __, code ) );
			
			// комплексный блок
			
			// _id: null :: при проходе через addComplex _id станет заполненным - нужно обнулить (чтобы второй addComplex не был с existId)
			DB.session.set(__.user.session, code, Object.assign({}, complex, {__: undefined, _id: undefined, itemCodes: undefined, form: __.form}));
		}
	});
	//console.time('funcId lvl='+lvl);
	async.parallel(funcId, (err, res_funcId)=>{try{
		//console.timeEnd('funcId lvl='+lvl);
		__.sendLvl = lvl + 1;
		if(!__.lvl[__.sendLvl]) __.lvl[__.sendLvl] = [];
		
		var send = {}, funcTpl = [], getData = {};

		for(const code of __.lvl[lvl])
		{
			var field = __.fields[code];

			if(field.c == undefined){
				
				var data = __.data[field.parent];
				var parent = __.fields[field.parent];

				send[field.code] = sendField(field, data, __);

				if(field.sub)
				{
					if(__.user.forms[__.form].sub[field.code] == undefined) __.user.forms[__.form].sub[field.code] = {};

					const subKey = [
						data._id.toString(), parent.col, field.name
					].join('-');
					if(!SUB.fields[ subKey ]) SUB.fields[ subKey ] = {};
					SUB.fields[ subKey ][__.conn.xaoc.id] = true;
					SUB.users[ __.conn.xaoc.id ][ subKey ] = {code: field.code, form: __.form};
				}
			}else{
				
				const proc = __.process[field.linecode];
				var items = [];
				
				if(field.sub)
				{
					if(!__.user.subIds) __.user.subIds = {};
					if(__.user.subIds[field.code] == undefined) __.user.subIds[field.code] = [];
					
					const subKey = [
						__.data[field.parent]._id, (__.fields[field.parent]||{}).col, (proc.links||{})[(__.fields[field.parent]||{}).name]
					].join('-');
					if(!SUB.fields[ subKey ]) SUB.fields[ subKey ] = {};
					SUB.fields[ subKey ][__.conn.xaoc.id] = true;
					SUB.users[ __.conn.xaoc.id ][ subKey ] = {code: code, id: [], form: __.form};
				}
				
				if(__.queryIds[code] && __.queryIds[code].filter(_=>_).length){
					
					for(const [i, _id] of Object.entries(__.queryIds[code]))
					{
						var config = Object.assign({item: true}, field.localConfig||{});
						var fi = (i == 0), li = (i == __.queryIds[code].length - 1);
						
						if(fi) config.fi = true;
						if(li){
							config.li = true;
							
							// не поможет только если весь список кончается ровно на последнем элементе в этой порции
							// если делать совсем правильно, то нужно брать из базы на один элемент больше
							if(field.filter && field.filter.l && i+1 < Math.abs(field.filter.l)) config.lli = true;
						}

						// reloadItem с фронта - подставляем code, который ждет фронт для перерисовки item
						if(field.itemCodes) Object.keys(field.itemCodes).forEach(code=>
						{
							if( field.itemCodes[code]._id+'' == _id._id+'' ){
								field.itemCode = code;
								field.itemCodes[code] = undefined;
							}
						});
						
						var item = c(field, config);
						items.push( item );
						
						__.data[item[1].code] = typeof _id == 'object' && _id._id != undefined ? _id : {_id: _id};
						
						if(fi) __.data[item[1].code]._FI = true;
						if(li) __.data[item[1].code]._LI = true;
						
						if(field.sub)
						{
							if(!__.user.subIds) __.user.subIds = {};
							if(__.user.subIds[item[1].code] == undefined) __.user.subIds[item[1].code] = [];

							const subKey = [
								__.data[field.parent]._id, (__.fields[field.parent]||{}).col, (proc.links||{})[(__.fields[field.parent]||{}).name], __.data[item[1].code]._id.toString()
							].join('-');
							if(!SUB.fields[ subKey ]) SUB.fields[ subKey ] = {};
							SUB.fields[ subKey ][__.conn.xaoc.id] = true;
							SUB.users[ __.conn.xaoc.id ][ subKey ] = {code: item[1].code, id: [], form: __.form};
						}
					}
					
					items.forEach((item)=>{
						
						var code = item[1].code;
						var field = __.fields[code];
						var parent = __.fields[field.parent];
						var key = md5(field.col+JSON.stringify(__.queryFields[field.linecode])); // можно еще оптимизировать, чтобы отличающиеся на 1-2 поля выборки считались как одинаковые

						if(getData[key] == undefined) getData[key] = {
							col: field.col,
							linecode: field.linecode,
							fields: __.queryFields[field.linecode],
							fieldsCustom: __.queryFieldsCustom[field.linecode],
							ids: [],
							links: {},
						};
						
						// из-за __.data[code]._id !== true внутри dataReady не будет элементов с _id == true
						// если хочется, то нужно будет создать отдельный массив с ключем "true" и пихать элементы в него
						if(__.data[code]._id && __.data[code]._id !== true && (__.data[code]._id+'').length == 24){
							getData[key].ids.push( ObjectId(__.data[code]._id) );
							if(getData[key].links[__.data[code]._id] == undefined) getData[key].links[__.data[code]._id] = []; // тут массив, если отрисовано 2+ сущностей с одним _id
							getData[key].links[__.data[code]._id].push(code);
						}
						
						funcTpl.push(function(callback){ try{
							
							if((__.data[code]._id || field.type != 'complex') && (!parent.deleteNotDB || __.data[code]._db)){
								
								// элемент комплексного блока
								DB.session.set(__.user.session, code, Object.assign({}, field, {__: undefined, form: __.form, _id: __.data[code]._id, itemCodes: undefined}));
								
								// !!! родителем в runTplFuncInNewContext (первый параметр) принципиально должен быть field (на него завязан this в xCOMPLEX = c(...))
								return [field.code, runTplFuncInNewContext(field, proc.tpl, field, __.data[field.code], {runTplFuncInNewContext_caller: 'item'})];
							}else{
								if(parent.deleteNotDB && !__.data[code]._db && __.fields[parent.parent].col && __.data[parent.parent]._id && __.data[code]._id){ // удаляем мертвую ссылку
									DB.deleteComplex(__, {col: field.col, _id: __.data[code]._id, deleteLinks: true}, {
										col: __.fields[parent.parent].col, _id: __.data[parent.parent]._id,
									}, ()=>{
										return [field.code, []];
									});
								}else{
									return [field.code, []];
								}
							}
						}catch(err){ console.log(err) } });
					});
				}else{
					if(proc && proc.tpl0) items.push( proc.tpl0( {__: __} ) );
				}

				send[code] = items;
			}
		}
		
		if(Object.keys(send).length){
			callback.codes = callback.codes.concat(Object.keys(send));
			__.conn.sendText(JSON.stringify({tpls : send}));
		}
		
		var funcData = [];
		for(var d in getData){
			if(getData[d].ids.length){
				var getDataFunc = function(data, callback){
					
					
					// скрипт не пойдет дальше, если внутри комплексного блока нет ни одного поля, которое попало бы в data.fields. Из-за этого также не будет вызываться функция dataReady.
					// проверка не учитывает data.fieldsCustom... но не факт, что он где-то используются - это надо выяснить
					if(data.fields && Object.keys(data.fields).length)
					{
						//console.time(lvl+data.col);
						__.db.collection(data.col)
						.find({_id:{$in: data.ids}}, data.fieldsCustom || data.fields)
						.toArray((err, res)=>{try{
							//console.timeEnd(lvl+data.col);
							
							if(err) console.log(err);
							
							if(data.linecode == '.' && res.length == 0){
								callback( {err: 'Не найдена основная сущность формы', badForm: true} );
								return;
							}

							var dataReady = {}; // элементы могут быть в неправильном порядке
							res.forEach((r)=>{ if(r._id && data.links[r._id]) data.links[r._id].forEach((l)=>{ 
								__.data[l]._meta = {
									col: data.col,
									_id: r._id,
								};
								__.data[l]._db = true; // есть данные из db
								Object.assign(__.data[l], r); // тут есть недочет, что данные их data-хранилищ (то что внутри __[child_name]) имеют меньший приоритет (они внутри __.data[l]) и будут перетираться (если кто-то начнет именовать переменные с ">" или "<")
								dataReady[l] = __.data[l];
							}) });
							__.process[data.linecode].data = data.links;
							if(typeof __.process[data.linecode].dataReady == 'function'){
								// внутри функции нельзя переназначать саму dataReady (например, вот такая конструкция не будет работать : data[Object.keys(data)[0]] = Object.assign(res.result, data[Object.keys(data)[0]]) ), оперируем только с ее параметрами
								__.process[data.linecode].dataReady(__, dataReady, callback);
							}else{
								callback();
							}
						}catch(e){ console.log(e); callback(); }});
					}else{
						__.process[data.linecode].data = data.links;
						callback();
					}
				}
				funcData.push(getDataFunc.bind(null, getData[d]));
			}
		}
		
		//console.time('funcData lvl='+lvl);
		async.parallel(funcData, (err, res_funcData) => {
			//console.timeEnd('funcData lvl='+lvl);
			if(err){
				callback(err);
				return;
			}
			
			const res_funcTpl = [];
			for(const f of funcTpl)
			{
				res_funcTpl.push( f() );
			}
			const tpls = {};
			
			for(var t in res_funcTpl)
			{
				tpls[res_funcTpl[t][0]] = res_funcTpl[t][1];
			}
			
			if(Object.keys(tpls).length)
			{
				callback.codes = callback.codes.concat(Object.keys(tpls));
				__.conn.sendText(JSON.stringify({tpls : tpls}));
			}
			
			if(__.lvl[__.sendLvl].length)
			{
				sendForm(__, callback);
			}
			else
			{
				__.conn.sendText(JSON.stringify({lst : __.lst}));

				__.user.forms[__.form].parents = __.parents;
				DB.session.set(__.user.session, 'forms', __.user.forms);
				//DB.redisKeys(true);

				if(typeof callback == 'function') callback({load: true, editMode: __.user.editMode, theme: __.theme, codes: callback.codes});
			}
		});
	}catch(e){console.log(e)}});
}
exports.sendForm = sendForm;

function sendField(field, data = {}, __){
	
	if(field.name[0] == '>')
	{
		const parent = __.fields[__.fields[field.parent].parent].parent;
		field.customSave = {
			parent: {
				_id: __.data[parent]._id,
				col: __.fields[parent].col,
			},
			field: {
				name: SYS.get(__.fields[field.parent], 'link[0]')+'.data.'+data._id+'.'+field.name.slice(1),
			},
		}
	}
	
	if(field.keyvalue.indexOf('.') == -1){
		field.value = data[field.keyvalue] != undefined ? data[field.keyvalue] : field.value;
	}else{
		var key = field.keyvalue.split('.');
		if(data[key[0]]){
			var defValue = field.value;
			field.value = data[key[0]];
			for(var i = 1; i < key.length; i++){
				field.value = (field.value != undefined && field.value[key[i]] != undefined) ? field.value[key[i]] : defValue;
			}
		}
	}
	if(field.name == 'script')
	{
		const sfx = (field.config||{}).sfx ? '_'+field.config.sfx : '';
		field.value = __.data[field.parent + '_script' + sfx] || {};
	}
	if(field.type.indexOf('select-') != -1 || field.type.indexOf('radio-') != -1){try{
		if(field.value && LST[field.lst] && LST[field.lst].list.obj[field.value].l){
			field.value = LST[field.lst].list.obj[field.value].l;
		}
	}catch(e){ console.log('ERROR :: Ошибка получения lst ('+field.lst+','+field.value+')') }}
	
	//if(field.type.indexOf('img') != -1 && SYS.get(field, 'value.l')) field.value = field.value.l;

	if(field.fvalue != undefined) field.value = field.fvalue;	
	
	//if(typeof field.value == 'object') field.value = JSON.stringify(field.value);
	
	var sendField = {type: field.type, stype: field.stype, value: field.value, front: field.front, label: field.label, lst: field.lst, ajax: field.ajax, prepare: field.prepare, sfx: field.sfx, config: field.config, theme: field.theme};
	sendField = field;

	field.showName = true;
	if(sendField.stype || field.showName || sendField.type.indexOf('radio') != -1) sendField.name = field.name;
	
	if(field.type != 'json' && field.type.indexOf('-') == -1){
		DB.session.set(__.user.session, field.code, Object.assign({}, field, {__: undefined, value: field.type.indexOf('slink') == 0 ? field.value : undefined}));
	}
	
	return sendField.type.indexOf('slink') == 0 ? null : [sendField];
}


markupTags = {};

[
	'DIV', 
	'SPAN', 
	'I', 
	'A', 
	'IMG', 
	'UL', 
	'LI',
	'BUTTON'
].forEach(tag => {
	require('vm').runInThisContext(`
		${tag} = function(){
			if(Object.prototype.toString.call( arguments[0].class ) === '[object Array]') arguments[0].class = arguments[0].class.join(' ');
			return [arguments.callee.name, arguments[0], Object.values(arguments).slice(1)];
		}
		markupTags['${tag}'] = ${tag};
	`, {});
});

function runTplFuncInNewContext(contextComplex, tplFunc, ...arg)
{
	const contextObject = Object.assign({
		/*	результат выполнения tpl-функции		*/	tplResult: [],
		/* 	аргументы tpl-функции 					*/	arg,
// !!! тут надо заменить подгрузку глобальных переменных одной функцией
		/* 	глобальные переменные 					*/	SYS, DB, PROJECT, PROJECT_LINK, CONFIG, BLOCK, LST, SUB, 
														window, fs, moment, console, lo, le, lw,
		/* 	markup-функции c правильным родителем	*/	xCOMPLEX: xCOMPLEX.bind(contextComplex),
														xFIELD: xFIELD.bind(contextComplex),
														xHTML: xHTML.bind(contextComplex),
														xIF: xIF.bind(contextComplex),
		/* 	разное									*/	__: arg[0].__,
	}, 
		/* 	функции markup-верстка					*/ markupTags
	);
	
	require('vm').runInNewContext(`
		f = ${tplFunc.toString()};
		tplResult = f( ...arg );
	`, contextObject, {
		filename: contextComplex.__.filename // !!! правильный offset я так и не подобрал
	});
	
	return contextObject.tplResult;
}

xCOMPLEX = c;
xFIELD = function(d)
{
	// чтобы не засорять данные в тех местах, где шаблоны создаются через конфиги (например, __tpl html~table.js)
	var data = Object.assign({}, d);
	if(typeof data != 'object') data = {name: data, keyvalue: data};
	if(!data.type) data.type = 'input';
	if(data.type.indexOf('*') != -1) data.type = data.type.replace('*', 'json');
	
	// первая версия решения - желательно переделать
	// >>>
	
		data.themeType = data.type+'';
		
		if(this.editMode){
			if(data.themeType[data.themeType.length-1] == '-'){
				data.themeType = data.themeType.substring(0, data.themeType.length - 1);
			}else{
				if(data.themeType[data.themeType.length-1] != '+') data.themeType += '+';
			}
		}
		
		if(!data.theme) data.theme = this.__.theme;
		data.themePath = '__tpl'+(data.theme?('/'+data.theme):'')+'~el_'+data.themeType;
		
		// тут в data.themePath все еще есть -/+, то есть должен быть соответствующий файл/блок файла в __tpl
		if(BLOCK.html[data.themePath+'.js'] === undefined) data.themePath = '__tpl~el_'+data.themeType;
	
	// <<<
	
	var linecode = this.linecode;
	
	if(data.lst){
		var l = SYS.getList(this.__, data.lst);
		if(l) this.__.lst[data.lst] = (l.path||'').replace(PROJECT_LINK, ''); // на клиенте по этому path напрямую выкачивается js-файл с сервера
	}
	if(this.__.pre === true){

		this.__.html.el[data.themePath] = true;
		
		// примерное решение для подгрузки шаблонов с учетом editMode - желательно переделать
		// >>>
			if(data.themePath[data.themePath.length-1] != '-'){
				this.__.html.el[data.themePath+'+'] = true;
			}else{
				this.__.html.el[data.themePath.substring(0, data.themePath.length - 1)] = true;
			}
		// <<<
		
		if(this.__.queryFields[linecode] == undefined) this.__.queryFields[linecode] = {};
		this.__.queryFields[linecode][data.name] = 1;

		if(data.front){
			if(this.__.front[linecode] == undefined) this.__.front[linecode] = {};
			var funcList = {};
			for(var key in data.front){
				var f = data.front[key], s, ss;
				if(typeof f == 'function'){
					s = f.toString();
					ss = md5(s);
					this.__.script[ss] = f;
					funcList[key] = ss;
				}
			}
			this.__.front[linecode][data.name+(data.sfx?'__'+data.sfx:'')] = funcList;
		}
		
		for(var p in data.process){
			this.__.process[this.linecode][p+'__'+data.name+(data.sfx?'__'+data.sfx:'')] = data.process[p];
		}
		
	}else{
		
		var field = fieldBase(this, data);
		this.__.fields[field.code] = field;
		
		this.__.lvl[this.__.sendLvl].push(field.code);
		
		if(this.__.parents[this.code] == undefined) this.__.parents[this.code] = [];
		this.__.parents[this.code].push(field.code);

		return field.type.indexOf('slink') == 0 ? '/link/'+this.__.user.session+'/'+field.code : ['div', {code: field.code}];
	}
}

xHTML = function(name, _, d, config = {})
{	
	name += name.indexOf('.js') == -1 ? '.js' : '';
	
	if(config){
		if(!config.theme) config.theme = this.__.theme || this.theme;
		config.__tpl = name.replace('__tpl~', '__tpl'+(config.theme?('/'+config.theme):'')+'~');
		if(BLOCK.html[config.__tpl] !== undefined) name = config.__tpl;
	}
	
	if(this.__.pre === true)
	{
		if(this.__.html[this.linecode] == undefined) this.__.html[this.linecode] = {};
		if(this.__.html[this.linecode][name] == undefined){
			if(d.recursive){
				this.__.html[this.linecode][name] = {tpl: ()=>{return []}};
			}else{
				this.__.html[this.linecode][name] = DB.requireFile('html', name);
			}
		}
		
		if(config) this.__.html[this.linecode][name].theme = config.__tpl.split('~');
	}

	config.runTplFuncInNewContext_caller = 'html';
	return runTplFuncInNewContext(this, this.__.html[this.linecode][name].tpl, this, d, config);
}

xIF = function(check, result){
		// !!! не очень мне нравится реализация
		// если нужно, то используем _.if.call({nopre: true}, ...)
		//return (!this.nopre && complex.__.pre) || check ? typeof result == 'function' ? result() : result : [];
	return this.__.pre || check ? typeof result == 'function' ? result() : result : [];
};