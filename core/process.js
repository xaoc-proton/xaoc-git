
if(CONFIG.DEBUG) delete require.cache[require.resolve('./markup.js')];
markup = require('./markup.js');

//exports.form  = function(){};
//exports.session = function(){};
exports.action = async function(meta, msg)
{try{
	/* для отладки старых функций без рефакторинга
	exports.f = async function(msg) { // нужно заменить объявление функции
	const conn = this.conn;
	const data = {msg: msg, save: []};
	data.field = this.actionField.field;
	data.field.parent = this.actionField.parent;
	callback = (answer)=>{
		console.log('answer', {data, answer});
	}
	 */
	if(typeof meta == 'string') meta = {name: meta};
	
	let result = ERR('Операция не выполнена');
	
	// если не стоит contextPrepared, значит это запрос с фронта, иначе - вызов внутри бизнес-транзации (с уже созданным контекстом)
	const self = this.contextPrepared ? this : DB.createContext( this );
	
	if(self.transaction.length == 0)
	{
		self.user = ((self.conn||{}).user||{}).key ? await self.findOne('user', ObjectId( self.conn.user.key )) : {};
		if(self.user._meta) self.user._meta.update = true; // пусть юзера всегда проверяют на изменения
		
		self.conn.dataUser = self.user; // временная история, пока не будет переписан старый функционал создания юзера

		await DB.session.getParent(self.conn, msg.code).then(async getParent =>
		{
			if(!meta.name) meta.name = (getParent.field||{}).name;
			self.actionField = getParent;
			self.actionField.parents = [];
			
			if((getParent.field||{})._id && (getParent.field||{}).col)
				await self.findOne(getParent.field.col, ObjectId(getParent.field._id));
			if((getParent.parent||{})._id && (getParent.parent||{}).col)
				if((getParent.parent||{})._id !== true) // в markup допускается передача _id == true - просто игнорируем их
					await self.findOne(getParent.parent.col, ObjectId(getParent.parent._id));
			
			// рекурсивный обход всех родителей
			let parent = getParent.parent;
			while((parent||{}).parent)
			{
				await DB.session.getParent(self.conn, parent.parent).then(async getParent =>
				{
					self.actionField.parents.push(getParent);
					
					if((getParent||{}).field && getParent.field._id && getParent.field.col)
					{
						await self.findOne(getParent.field.col, ObjectId(getParent.field._id));
						parent = getParent.parent;
					}else if((getParent||{}).parent && getParent.parent._id && getParent.parent.col)
					{
						await self.findOne(getParent.parent.col, ObjectId(getParent.parent._id));
						parent = getParent.parent;
					}else
					{
						parent = undefined;
					}
				}).catch(err =>
				{
					parent = undefined;
				});
			}
			
			if(self.actionField.field.form) self.form = SYS.requireFile('cache', self.actionField.field.form+(self.conn.user.role?'_'+self.conn.user.role:'')+(self.actionField.field.theme?'_'+self.actionField.field.theme:''));
			if(!self.form) self.form = SYS.requireFile('cache', self.actionField.parent.form+(self.conn.user.role?'_'+self.conn.user.role:'')+(self.actionField.parent.theme?'_'+self.actionField.parent.theme:''));
		}).catch(err =>
		{
			if(!msg.code === true) // это допустимая ситуация, когда событие на фронте вызывается без привязки к элементу верстки
				throw err;
		});
	}
	self.transaction.push([meta.name, msg]);
	
	if(meta.name == 'custom')
	{
		const customPath = 'process["' + self.actionField.parent.linecode + '"]["action__custom__' + self.actionField.field.sfx + '"]';
		self.actionScript = {f: SYS.get(self.form, customPath)};
		
		if (typeof self.actionScript.f != 'function')
			return ERR('Действие "' + customPath + '" не найдено');
	}
	else if(meta.name == 'add')
	{
		self.actionScript =
		{
			f: async function(msg)
			{
				if((this.conn.user.subIds[this.actionField.field.code]||[]).length)
				{
					const res = await new Promise((resolve, reject) => {
						addComplex(this.conn, msg, res =>
						{
							resolve(res);
						});
					});
					return OK(res);
				}
				
				this.add({
					col: this.actionField.field.col, 
					links: this.actionField.field.links
				}, [
					this.dataFind({_id: ObjectId(this.actionField.parent._id) })
				]);
				
				return OK();
			}
		}
	}
	else if(meta.name == 'delete')
	{
		self.actionScript =
		{
			f: async function(msg)
			{
				if((this.conn.user.subIds[this.actionField.field.code]||[]).length)
				{
					const res = await new Promise((resolve, reject) => {
						deleteComplex(this.conn, msg, res =>
						{
							resolve(res);
						});
					});
					return OK(res);
				}
				
				const markupParent = this.actionField.parents.find(p => p.field.code == this.actionField.parent.parent).field;
				const parent = this.dataFind({ _id: ObjectId(markupParent._id) });
				const links = this.actionField.field.links;
				
				parent[ links[markupParent.name] ].l = parent[ links[markupParent.name] ].l.filter(id => id.toString() != this.actionField.field._id);

				return OK();
			}
		}
	}
	else
		self.actionScript = SYS.requireFile("action", meta.name, true);
	
	await new Promise(
		async function(resolve, reject){try{
			// если тут ошибка отсутствия actionScript.f, то скорее всего это дефолтный (не переписанный) стандартный обработчик для complex (ищи baseAction в server.js)
			result = await self.actionScript.f.call(self, msg);
			resolve(result);
		}catch(err){ reject(err) }}
	)
	.then(async function(answer)
	{
		const action = self.transaction.pop();
		//console.log("action", action, answer);
		if(self.transaction.length == 0)
		{ // окончание бизнес-транзакции
			if(answer.status == 'ok' || answer.debugSave) // сохраняем только при успешном завершении транзакции
				await self.saveTransaction({ log: answer.log, debug: answer.debugSave });
			result = {req: msg, answer: answer};
		}
	})
	.catch(function(err)
	{
		const action = self.transaction.pop();
		//console.log("action", err, answer);
		if(self.transaction.length == 0)
		{ // окончание бизнес-транзакции
			console.log('err', err);
			result = {req: msg, answer: ERR('Системная ошибка')};
		}else
			throw err; // передаем ошибку в try-catch функции
	});
	//.finally(function(){ console.log("finally", arguments) });

	return result;
/* 	
		(conn.mysqlLogs 
			? cb => { cb(conn.mysqlLogs) } 
			: DB.getMysqlLogs)(mysql => { try {
				
			var sql = "";
			sql =
				"INSERT INTO `__access_actions` (add_time, action, msg, user, auth, session, alert) VALUES ('" + Date.now() + "', '" + (actionName || "") + "', " +
				mysql.escape( (JSON.stringify(data.msg) || "").slice(0, 1000) ) + ", " +
				mysql.escape(conn.user.key + "" || "") + ", " +
				mysql.escape(conn.user.authKey || "") + ", " +
				mysql.escape(conn.user.session || "") + ", " +
				mysql.escape(error ? JSON.stringify(error) : "") + " )";

			if (sql) {
				mysql.query(sql, err => {
					if (err) console.log(err, sql);
					if (!conn.mysqlLogs) mysql.destroy();
				});
			} else {
				if (!conn.mysqlLogs) mysql.destroy();
			}
		} catch (e) {
			mysql.destroy();
			console.log("Ошибка записи логов :: ", e);
			console.log( "data.msg, error", process.pid, data.msg, error );
		} });
 */
}catch(err){ // сюда попадет throw err из Promise.catch()
	throw err; // проталкиваем ошибку к инициирующему событию бизнес-транзакции
}}

exports.route = function(conn, msg, _callback, systemCall){ return new Promise(async (resolve, reject)=>
{	
	// cb() из async ждет в первом параметре ошибку, поэтому не передаем туда ничего
	// передавать ответ вторым параметром тоже нельзя, так как это сместит позицию cb() в последующих вызовах внутри async
	// у этого cb() нет параметра name, однако его нет в том числе и у функций, создаваемых "на лету" при вызове route
	// при этом у cb() по какой то причине параметр length === 0
	// upd: при последовательном вызове (например, async.waterfall) все равно сдвигается cb(), 
	// так что пока приходится пользоваться конструкциями "(...cb)=>{" и "DB.addComplex(..., cb.pop())"
	// upd 071120: для колбеков async нельзя сделать _callback = function(){ _callback() }
	
	// if (!_callback) _callback = function () {};
	//const callback = !_callback.name && _callback.length === 0 ? function () { _callback() } : _callback;
	
	const timelog = { msg: JSON.stringify(msg), time: Date.now() }
	const callback = data=>
	{
		timelog.time = Date.now() - timelog.time;
		if(SYS.get(data, 'req.x'))
			data.answer.logtime = timelog.time;
		else
			console.log(timelog);
		
		if(_callback){
			(!_callback.name && _callback.length === 0 ? function () { _callback() } : _callback)(data);
		}else{
			if(data && data.err) reject(data);
			else resolve(data);
		}
	};
	
	let actionName;
	
	const callbackWithSaveAll = function(ANSWER){
		data.saveAll(()=>{
			if (conn.processInstall) console.log("install action end", actionName);
			callback({ req: data.msg, answer: ANSWER });
		});
	}
	const errorCallback = (err)=>{ callback({req: data.msg, answer: err}) };
	
	const data = Object.assign(DB.createContext(conn), {
		msg: (typeof msg == "object" ? msg : JSON.parse(msg)) || {},
		systemCall: systemCall,
	});
	
	DB.getMongo(db => 
	{try{
		// вероятно есть баг с переписываем errorCallback при вложенных друг в друга вызовах роутера
		db.errorCallback = errorCallback;
		conn.db = db;

		if (data.msg.session !== undefined) { // doLogin с клиента

			if (data.msg.session == "close") {
				
				// это работает неправильно, если открывать несколько окон в одном браузере (удаляет после закрытия первого)

				SYS.requireFile("func", "user~session_close").f.call(data, conn, data.msg, ANSWER => {
					data.saveAll(() => { try {
						DB.session.del("_auth", [ conn.user.key + "_" + conn.user.authKey ]);
						DB.session.clearForm(conn);
						callback();
					} catch (e) {
						console.log(e);
						callback();
					} });
				});
			} else {
				SYS.requireFile("func", "user~session").f.call(data, conn, data.msg, ANSWER => {
					data.saveAll(() => {
						callback({ req: data.msg, answer: ANSWER });
					});
				});
			}
		}

		if (data.msg.action !== undefined) {
			
			switch (data.msg.action) {
				
				case "form":
					getForm(conn, data.msg, function (ANSWER) {
						callback({ req: data.msg, answer: ANSWER });
					});
					break;

				case "link":
					getSubForm(conn, data.msg, function (ANSWER) {
						callback({ req: data.msg, answer: ANSWER });
					});
					break;

				case "show":
					showComplex(conn, data.msg, function (ANSWER) {
						callback({ req: data.msg, answer: ANSWER });
					});
					break;

				default:

					new aP(conn).concat(
						[data.getParent.bind(["msg.code", "field"])]
					).run(()=>
					{
						if ( data.msg.action && !/^[0-9a-z_~\/]+$/i.test(data.msg.action) ) {
							console.log("ERR bad action: " + data.msg.action.toString());
							data.msg.action = "___bad_action___";
						}

						// проверка на наличие спецсимволов в имени
						if (!/^[0-9a-z_~\/]+$/i.test(SYS.get(data, "field.name") || ""))
							SYS.set(data, "field.name", "");

						var action;
						actionName = (data.msg.action && data.msg.action !== true) ? data.msg.action : data.field.name;

						if ( data && data.field.name == "custom" && data.field.parent.form )
						{
							var form = SYS.requireFile("cache", data.field.parent.form + (conn.user.role?"_"+conn.user.role:"")+(data.field.parent.theme?"_"+data.field.parent.theme:''));

							var customPath = 	'process["' + data.field.parent.linecode +
												'"]["action__custom__' + data.field.sfx + '"]';
							var customFunc = SYS.get(form, customPath);

							if (typeof customFunc == "function") {
								if(customFunc[Symbol.toStringTag] === 'AsyncFunction'){
									(customFunc(conn, data, callbackWithSaveAll))
										.catch(e=>conn.db.errorCallback( ERR('Ошибка (cfunc)', e) ));
								}else{
									customFunc(conn, data, callbackWithSaveAll);
								}
							} else {
								callback({ req: data.msg, answer: { err: 'Действие "' + customPath + '" не найдено (3)' } });
							}
						} else if (
							(action = SYS.requireFile(data.msg.actionType || "action", actionName, true))
						) {
							
							var error;

							if (action && typeof action.f == "function") {
								
								if (action.system !== true || data.systemCall) {
									
									if ( data.systemCall || conn.user.auth || action.auth === false ) {
										
										if ( data.msg.code == undefined || SYS.get(data, "field.name") != undefined) {
											
											if (conn.processInstall)
												console.log("install action start", actionName);

											if(action.f[Symbol.toStringTag] === 'AsyncFunction'){
												(action.f.call(data, conn, data, callbackWithSaveAll))
													.catch(e=>conn.db.errorCallback( ERR(`Ошибка func-${actionName}`, e) ));
											}else{
												action.f.call(data, conn, data, callbackWithSaveAll);
											}
										} else {
											// вероятно обнулился fields cache - для начала перезагрузи страницу
											error = { err: "Ошибка получения базовых сущностей" };
										}
									} else {
										error = { err: "Действие запрещено (2)" };
									}
								} else {
									error = { err: "Действие запрещено (1)" };
								}
							} else {
								error = { err: 'Действие "' + actionName + '" не найдено (2)' };
							}

							if (error) callback({ req: data.msg, answer: error });

							(conn.mysqlLogs 
								? cb => { cb(conn.mysqlLogs) } 
								: DB.getMysqlLogs)(mysql => { try {
									
								var sql = "";
								sql =
									"INSERT INTO `__access_actions` (add_time, action, msg, user, auth, session, alert) VALUES ('" + Date.now() + "', '" + (actionName || "") + "', " +
									mysql.escape( (JSON.stringify(data.msg) || "").slice(0, 1000) ) + ", " +
									mysql.escape(conn.user.key + "" || "") + ", " +
									mysql.escape(conn.user.authKey || "") + ", " +
									mysql.escape(conn.user.session || "") + ", " +
									mysql.escape(error ? JSON.stringify(error) : "") + " )";

								if (sql) {
									mysql.query(sql, err => {
										if (err) console.log(err, sql);
										if (!conn.mysqlLogs) mysql.destroy();
									});
								} else {
									if (!conn.mysqlLogs) mysql.destroy();
								}
							} catch (e) {
								mysql.destroy();
								console.log("Ошибка записи логов :: ", e);
								console.log( "data.msg, error", process.pid, data.msg, error );
							} });
						} else {
							
							if (conn.user.auth || CONFIG.disableAuth) {
								
								switch (data.msg.action) {
									
									case "logout":
										
										DB.session.del("_auth", [conn.user.key + "_" + conn.user.authKey], err => { try {
											
											DB.accessLog(conn, {
												data: data.msg,
												user: conn.user.key,
												auth: conn.user.authKey,
												session: conn.user.session,
											}, "logout");

											conn.db.collection("user").update(
												{ _id: ObjectId(conn.user.key) },
												{ $pull: { authKeys: conn.user.authKey } }
											);

											conn.user.auth = false;
											conn.user.roles = undefined;
											conn.user.role_key = undefined;
											conn.user.role_link = undefined;
											conn.user.access = undefined;
											conn.user.authKey = undefined;

											// без них не корректно отработает session == 'close'
											//conn.user.role = undefined;
											//conn.user.key = undefined;

											callback({ req: data.msg, answer: { reload: true } });
										} catch (e) {
											console.log(e);
											callback({ req: data.msg, err: "Ошибка закрытия сессии пользователя" });
										}});

										break;

									case "toggleeditmode":
										
										if (conn.user.access && conn.user.access.edit) {
											
											var ANSWER = { answer: { reload: true } };
											
											if (conn.user.config.editMode) {
												
												conn.user.config.editMode = undefined;
												
												DB.session.get( "_auth", conn.user.key + "_" + conn.user.authKey, (err, s) => {
													s.config.editMode = undefined;
													DB.session.set( "_auth", conn.user.key + "_" + conn.user.authKey, s );
												});
												
												ANSWER.answer.msg = "Режим редактирования выключен";
											} else {
												
												conn.user.config.editMode = true;
												
												DB.session.get( "_auth", conn.user.key + "_" + conn.user.authKey, (err, s) => {
													s.config.editMode = true;
													DB.session.set( "_auth", conn.user.key + "_" + conn.user.authKey, s );
												});
												
												ANSWER.answer.msg = "Режим редактирования включен";
											}
										} else {
											ANSWER.answer.msg = "Доступ к функционалу запрещен";
										}

										callback(ANSWER);
										break;

									case "snapshot":
										
										if (conn.user.access && conn.user.access.edit) {
											
											if ( data.msg.url && conn.user.query && conn.user.query.form ) {
												
												var path = require("path");
												var childProcess = require("child_process");
												var phantomjs = require("phantomjs-prebuilt");
												var link = "/forms/snapshot/" + conn.user.query.form + ".html";
												
												var program = childProcess.execFile(phantomjs.path, [
													path.join(XAOC_LINK, "/libs/phantom/snapshot.js"),
													data.msg.url, PROJECT_LINK + link, 3000,
												], function (err, stdout, stderr) { try {
													
													var files = fs.readdirSync( PROJECT_LINK + "/forms/snapshot" );
													
													if (files && files.length) {
														
														var sitemap = '<?xml version="1.0" encoding="UTF-8"?>\n';
														sitemap += '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">\n';
														
														files.forEach(l => {
															var path = PROJECT_LINK + "/forms/snapshot/" + l;
															var stats = fs.statSync(path);
															sitemap += "<url>\n";
															sitemap += "<loc>" + CONFIG.server_url + ("/forms/snapshot/" + l).replace(/"/g,"&quot;") + "</loc>\n";
															sitemap += "<lastmod>" + new Date(stats.mtime).toISOString() + "</lastmod>\n";
															sitemap += "</url>\n";
														});
														sitemap += "</urlset>";

														fs.writeFileSync(PROJECT_LINK + "sitemap.xml", sitemap);
													}

													callback({ req: data.msg, answer: {msg: "Снимок страницы создан", open: CONFIG.server_url + link} });
												} catch (e) {
													console.log(e);
													callback({ req: data.msg, answer: {msg: "Ошибка создания снимка страницы"} });
												} });
												
												setTimeout(() => { program.kill() }, 5000);
											
											} else {
												callback({ req: data.msg, answer: { msg: "Не указан URL" } });
											}
										} else {
											callback({ req: data.msg, answer: { msg: "Доступ к функционалу запрещен" } });
										}

										break;

									case "add":
										addComplex(conn, data.msg, function (ANSWER) {
											callback({ req: data.msg, answer: ANSWER });
										});
										break;

									case "delete":
										deleteComplex(conn, data.msg, function (ANSWER) {
											callback({ req: data.msg, answer: ANSWER });
										});
										break;

									case "control":
										controlComplex(conn, data.msg, function (ANSWER) {
											callback({ req: data.msg, answer: ANSWER });
										});
										break;

									case "fieldlog":
										if (conn.user.access && conn.user.access.edit) {
											getFieldLog(conn, data.msg, function (ANSWER) {
												callback({ answer: ANSWER });
											});
										} else {
											callback({ req: data.msg, answer: { err: "Доступ к функционалу запрещен" } });
										}
										break;

									case "save":
										saveField(conn, data.msg, function (ANSWER) {
											callback({ req: data.msg, answer: ANSWER });
										});
										break;

									case "newkeyfield":
										DB.createKeyField(conn, data.msg, function (ANSWER) {
											callback({ req: data.msg, answer: ANSWER });
										});
										break;

									default:
										callback({ req: data.msg, answer: {err:'Действие "' + data.msg.action + '" не найдено (1)'} });

										break;
								}
							} else {
								callback({ req: data.msg, answer: { err: "Пользователь не авторизован" } });
							}
						}
					});

					break;
			}
		}
	}catch(err){
		console.log(err);
		errorCallback('Ошибка выполнения действия', err)
	}});
}
).catch(e=>{ _callback({req: msg, answer: ERR('Ошибка выполнения действия (route)', e) }) })}

function getForm(conn, formData, callback){try{
	DB.session.clearForm(conn, {}, ()=>{try{
			
		// есть проблема с sub-событиями, которые были запущены незадолго до открытия новой формы (см. __nextId в markup.js)
		conn.user.forms = {};
		conn.user.query = JSON.parse(JSON.stringify(formData.query));
		markup.form({conn: conn, db: conn.db, name: formData.query.form}, callback);
			
	}catch(e){ console.log(e); callback({err: 'Ошибка создания формы(2)'}); }});
}catch(e){ console.log(e); callback({err: 'Ошибка создания формы(1)'}); }}

function getSubForm(conn, formData, callback){try{

	if(formData.query && formData.query.code){
		
		DB.session.get(conn.user.session, formData.query.code, (err, field)=>{try{
			
			if(field){

				DB.session.clearForm(conn, {form: field.form}, ()=>{try{
				
					DB.session.set(conn.user.session, formData.query.code, Object.assign({linecode: '.'}, formData.query), ()=>{try{
					
						delete conn.user.forms[field.form];

						if(conn.user.query && conn.user.query.filter) conn.user.query.filter.edit = false;
						if(formData.query.id == undefined) delete conn.user.query.id;
						Object.assign(conn.user.query, formData.query); // пробрасываем фильтр от основной формы
							
						markup.form({
							conn: conn, db: conn.db, 
							name: formData.query.form, code: formData.query.code
						}, callback);
							
					}catch(e){ callback({err: 'Ошибка открытия формы(6)'}) }});
				}catch(e){ callback({err: 'Ошибка открытия формы(5)'}) }});
			}else{ console.log("ERR getSubForm field", field); callback({err: 'Ошибка открытия формы(4)'}) }
		}catch(e){ callback({err: 'Ошибка открытия формы(3)'}) }});
	}else{ console.log('ERR getSubForm formData', formData); callback({err: 'Ошибка открытия формы(2)'}) }
}catch(e){ callback({err: 'Ошибка открытия формы(1)'}) }}

function getFieldLog(conn, logData, callback){
	
	if(logData.code){
		
		var res = {};
		
		DB.session.getParent(conn, logData.code, res, ()=>{

			if(res.field && res.parent){
				
				DB.getMysqlLogs((db)=>{
					
					var sql = "SELECT add_time, user, value FROM ?? WHERE parent = ? AND field = ?";
					db.query(sql, [res.parent.col, res.parent._id, res.field.name], (err, log)=>{
						
						if(err) console.log(err);
						db.destroy();
						
						if(log.length){
							var users = [];
							log.forEach((r)=>{ if(r.user) users.push(ObjectId(r.user)) });

							conn.db.collection('user').find({_id: {$in: users}}, {login: 1}).toArray((err, user)=>{
								var u = [];
								user.forEach((p)=>{ u[p._id] = p.login });
								log.forEach((r)=>{
									if(r.user) r.user = u[r.user];
									r.add_time = new Date(r.add_time);
								});
								callback(log);
							});

						}else{
							callback({err: 'Лог изменений поля пустой'});
						}
					});
				});
			}else{
				callback({err: 'Ошибка поиска код элемента'});
			}
		});
	}else{
		callback({err: 'Не указан код элемента'});
	}
}

function saveField(conn, saveData, callback){
	
	if(saveData.code){

		DB.session.get(conn.user.session, saveData.code, (err, field)=>{
			
			if(!(field && field.name && field.parent)){
				callback({err: 'Ошибка сохранения поля (1)', reloadItem: true});	
			}else
			{
				if(field.name == '_id'){
					callback({err: 'Запрещено редактировать системные поля'});
				}else if( (valid = (SYS.get( window,`el.${field.themePath}.valid`) || (()=>true))(saveData)) !== true){
					callback(valid);
				}else{
					DB.session.get(conn.user.session, field.parent, (err, parent)=>{try{

						if(parent._id != undefined){
								
							var out = {};
							
							out.value = saveData.value;

							var proc = SYS.requireFile("cache", parent.form + (conn.user.role?"_"+conn.user.role:"")+(parent.theme?"_"+parent.theme:'')).process;
							var waterfall = [];
							
							if(proc[parent.linecode]['save__'+field.name+(field.sfx?'__'+field.sfx:'')]) waterfall.push((cb)=>{
								proc[parent.linecode]['save__'+field.name+(field.sfx?'__'+field.sfx:'')].call(this, conn, field, parent, saveData, cb);
							});
							
							waterfall.push((res, cb)=>
							{
								if(typeof res == 'function')
								{
									cb = res;
									
									if(field.customSave){ // то что хранится в data-хранилищах внутри __[child_name]
										DB.saveField(conn,
											Object.assign({}, field, field.customSave.field || {}),
											Object.assign({}, parent, field.customSave.parent || {}),
										out, cb);
									}else{
										DB.saveField(conn, field, parent, out, cb);
									}
								}else if(res){
									cb(null, res);
								}else{
									cb( ERR('Ошибка сохранения поля') );
								}
							});
							
							async.waterfall(waterfall, (err, data)=>{
								callback(err || data);
							});
						}else{
							callback({err: 'Отсутствует ID для сохранения'});
						}
					}catch(e){
						console.log(e);
						callback({err: 'Ошибка сохранения поля (2)', reloadItem: true});
					}});
				}
			}
		});
	}else{
		callback({err: 'Не указан код элемента'});
	}
}

function showComplex(conn, showData, callback){try{

	if(showData.code){
		
		var force = showData.filter && showData.filter.force;
		
		DB.session.mget(conn.user.session, [showData.code].concat( 
			showData.itemCodes && showData.itemCodes.length && !force ? showData.itemCodes : []
		), (err, fields)=>{
			
			var field = fields.shift() || {};
			
			DB.session.get(conn.user.session, field.parent, (err, parent)=>{try{
					
				if(!parent) parent = {};

				// надо перепроверить, в filter может быть не только пагинация
				if(showData.filter == undefined || showData.filter.o == undefined){ // пагинация
					if(showData.replace){
						if(showData.itemCodes && !force){
							showData.itemCodes.forEach((code)=>{ 
								DB.session.clearForm(conn, {complex: code, form: field.form})
							});
						}else{
							DB.session.clearForm(conn, {complex: showData.code, form: field.form});
						}
					}
				}

				parent.code = field.parent;
				field.code = showData.code;
				field.filter = showData.filter || {};
				
				if(showData.itemCodes && !force){ // reloadItem c фронта (обновляем конкретный item)
					field.itemCodes = {};
					// рискованная конструкция, которая может поломаться, если из session.mget элементы вернутся в другом порядке
					fields.forEach((f, i)=>{ field.itemCodes[showData.itemCodes[i]] = {_id: f._id} });
				}

				field.__ = function() {};
				
				field.__.form = field.form;
				field.__.theme = field.theme;
				if(conn.user.config.editMode) field.__.editMode = true;
				
				field.c = markup.c;
						
				field.__.conn = conn;
				field.__.user = conn.user;
				//field.__.sub = conn.sub;
				field.__.db = conn.db;

				field.__.codePrefix = '';
				if(conn.user.forms[field.form].code){ // conn.user.forms[field.form] не всегда существует - нужно проверить
					field.__.codePrefix = conn.user.forms[field.form].code+'_';
				}
				
				field.__.lvl = {0:[showData.code]};
				field.__.parents = conn.user.forms[field.form].parents;
				field.__.fields = {0:{}};
						
				field.__.fields[field.parent] = parent;
				field.__.fields[showData.code] = field;

				var cache = SYS.requireFile("cache", field.form + (conn.user.role ? "_" + conn.user.role : "") + (field.theme?"_" + field.theme:''));

				field.__.process = cache.process;
				field.__.html = cache.html;
				field.__.lst = cache.lst;
				field.__.queryIds = {};
				field.__.queryFields = cache.fields;
				field.__.queryFieldsCustom = {};
				field.__.funcKeys = cache.funcKeys;
						
				field.__.func = {};
				field.__.script = [];
				field.__.style = [];
				field.__.data = {};
				field.__.lst = {};
				field.__.global = {};
				
				field.__.data[field.parent] = {_id: parent._id};
				if(field.__ && field.__.process[field.linecode]){
					field.links = field.__.process[field.linecode].links;
					field.userLink = field.__.process[field.linecode].userLink;
				}
				
				if(force && field.__.process[field.linecode].filter){
					field.filter = Object.assign(field.filter, field.__.process[field.linecode].filter);
					field.filter.o = 0;
				}
				
				function sendForm(__, callback){
					if(typeof field.__.process['.'].access == 'function'){
						field.__.process['.'].access(__, {}, (err, data)=>{
							if(err){
								callback(Object.assign({badForm: true}, err === true ? {err : 'Недостаточно прав для работы с формой '+__.form} : err));
							}else{
								if((data||{}).editMode) field.__.editMode = true;
								markup.sendForm(__, callback);
							}
						});
					}else{
						markup.sendForm(__, callback);
					}
				}
				
				if(parent._id === true || field.__.process[field.linecode].parentDataNotRequired){
					sendForm(field.__, callback);
				}else{
					
					if(showData.parentData){
						field.__.data[field.parent] = showData.parentData;
						sendForm(field.__, callback);
					}else{					
						
						var ff = Object.assign({_id: 1}, field.__.queryFields[parent.linecode]);
						var ll = field.filter && field.filter.l ? Math.abs(field.filter.l) : false;
						
						field.link.forEach((l)=>{
							if(ll){
								ff[l+'.l'] = { $slice: 
									field.filter.l > 0 ? 
									[field.filter.o, ll] :
									[(field.filter.l/ll)*(field.filter.o+ll), ll]
								};
							}else{
								ff[l+'.l'] = 1;
							}
							ff[l+'.data'] = 1; // не факт что нужно, потому что в field.__.queryFields[parent.linecode] оно уже есть 
						});

						conn.db.collection(parent.col).findOne({_id: ObjectId(parent._id)}, ff, (err, res)=>{
							
							field.__.data[field.parent] = res;
							field.link.forEach(l=>
							{
								if(!field.__.data[field.parent][l]) field.__.data[field.parent][l] = {l: []};
								if(!field.__.data[field.parent][l].l) field.__.data[field.parent][l].l = [];
								
								const dd = field.__.data[field.parent][l].data || {}; // подставляем данные из data-хранилища внутри __[child_name]
								/*Object.keys(dd).forEach(key=>{
									Object.keys(dd[key]).forEach(id=>{
										field.__.data[field.parent][l].l = field.__.data[field.parent][l].l.map(_=>{ if(_+''==id){
											_ = {_id: _};
											_['>'+key] = dd[key][id];
										} return _ });
										
										Object.values(field.itemCodes).forEach(_=>{ // reloadItem c фронта
											if(_._id+''==id) _['>'+key] = dd[key][id];
										});
									});
								});*/
								Object.keys(dd).forEach(id=>{
									Object.keys(dd[id]).forEach(key=>{
										field.__.data[field.parent][l].l = field.__.data[field.parent][l].l.map(_=>{ if(_+''==id){
											_ = {_id: _};
											_['>'+key] = dd[id][key];
										} return _ });
										
										Object.values(field.itemCodes).forEach(_=>{ // reloadItem c фронта
											if(_._id+''==id) _['>'+key] = dd[id][key];
										});
									});
								});
							});
							
							if(ll && !force){ // это для вложенных списков, которые идут без parentDataNotRequired

								DB.session.mget(conn.user.session, field.__.parents[field.code], (err, items)=>{

									items = items.map(i=>i._id);
									
									// может не быть связи в mongo (??? не вспомнил про что речь)
									field.link.forEach((l)=>{ if(res[l]){
										// это видимо на случай, что итем уже отрисован
										field.__.data[field.parent][l].l = res[l].l.filter(_=>(items.indexOf(_+'') == -1));
									} });
									
									sendForm(field.__, callback);
								
								});
							}else{
								sendForm(field.__, callback);
							}
						});
					}
				}
			}catch(e){
				console.log(e);
				callback({ err: 'Ошибка отображения полей' });
			}});
		});
	}
}catch(e){console.log(e)}}

function addComplex(conn, addData, callback){
	
	if(addData.process == undefined) addData.process = {};
	
	if(addData.code){
		
		DB.session.get(conn.user.session, addData.code, (err, field)=>{try{

			if(field.add){

				DB.session.get(conn.user.session, field.parent, (err, parent)=>{try{
					
					if(parent._id){

						parent.code = field.parent;
						field.code = addData.code;
						field.filter = addData.filter || {};
						//field.filter.actionAdd = true;

						field.__ = function() {};
						
						field.__.form = field.form;
						field.__.theme = field.theme;
						if(conn.user.config.editMode) field.__.editMode = true;
						
						field.c = markup.c;

						field.__.conn = conn;
						field.__.user = conn.user;
						field.__.db = conn.db;
						
						field.__.codePrefix = '';
						if(conn.user.forms[field.form].code){
							field.__.codePrefix = conn.user.forms[field.form].code+'_';
						}
						
						field.__.lvl = {0:[addData.code]};
						field.__.parents = conn.user.forms[field.form].parents;
						field.__.fields = {0:{}};

						field.__.fields[field.parent] = parent;
						field.__.fields[addData.code] = field;
						
						var cache = SYS.requireFile("cache", field.form + (conn.user.role ? "_" + conn.user.role : "") + (field.theme?"_" + field.theme:''));
						
						field.__.process = cache.process;
						field.__.html = cache.html;
						field.__.lst = cache.lst;
						field.__.queryIds = {};
						field.__.queryFields = cache.fields;
						field.__.queryFieldsCustom = {};
						field.__.funcKeys = cache.funcKeys;
						
						field.__.func = {};
						field.__.script = [];
						field.__.style = [];
						field.__.data = {};
						field.__.lst = {};
						field.__.global = {};
						
						field.__.data[field.parent] = {_id: parent._id};
						if(field.__ && field.__.process[field.linecode]){
							field.links = field.__.process[field.linecode].links;
							field.userLink = field.__.process[field.linecode].userLink;
						}
						
						const subId = (field.__.user.subIds[field.code]||[]).shift();
						
						var addFunc = addData.sub !== true 
						? 	(typeof field.__.process[field.linecode].add == 'function' 
							? 	(__, field, parent, values, cb)=>{
									field.__.process[field.linecode].add(__, field, parent, addData, cb);
								}
							: 	typeof field.__.process[field.linecode].beforeAdd == 'function' 
								? 	(__, field, parent, values, cb)=>{
										const parents = [parent];
										field.__.process[field.linecode].beforeAdd(__, field, parents, values, (err)=>{
											if(err){
												cb(err);
											}else{
												DB.addComplex(__, field, parents, values, cb);
											}
										});
									}
								: 	DB.addComplex
							) 
						: 	!subId 
							? 	(__, field, parent, {}, cb)=>{ cb({err: 'Не найден subId'}) }
							: 	(__, field, parent, {}, cb)=>{ 
								
								// !!! тут не подставляются данные из data-хранилища внутри __[child_name]
								
								var _id = ObjectId(subId);
								if(__.data){
									if(__.data[field.parent] == undefined) __.data[field.parent] = {};
									__.data[field.parent][(typeof field.link == 'object') ? field.link[0] : field.link] = [_id];
								}
								if(field.code) __.queryIds[field.code] = [_id];
								cb(null, {_id: _id});
							};
						
						if(addData.values && addData.values.existId){
							if(addData.values.existId == '__myself') addData.values.existId = conn.user.key;
							field._id = ObjectId(addData.values.existId);
							delete addData.values.existId;
						}
						
						addFunc(field.__, field, parent, addData.values || {}, (err, data)=>{try{

							if(field.__.user.forms[field.__.form].sub[field.code] == undefined) field.__.user.forms[field.__.form].sub[field.code] = {};
							
							if(err || data.err){
								callback(err || data);
							}else{
								
								function sendForm(__, callback){
									if(typeof field.__.process['.'].access == 'function'){
										field.__.process['.'].access(__, {}, (err, data)=>{
											if(err){
												callback(Object.assign({badForm: true}, err === true ? {err : 'Недостаточно прав для работы с формой '+__.form} : err));
											}else{
												if((data||{}).editMode) field.__.editMode = true;
												markup.sendForm(__, callback);
											}
										});
									}else{
										markup.sendForm(__, callback);
									}
								}
								
								field.__.data[field.code] = {_id: data._id};
								if(field.__.process[field.linecode].parentDataNotRequired) field.filter = {l: -1};
								
								if(addData.process == undefined) addData.process = {};
								addData.process._id = data._id;
								
								if(typeof field.__.process[field.linecode].afterAdd == 'function'){
									field.__.process[field.linecode].afterAdd(conn, field, parent, Object.assign({}, addData.values||{}, addData.process), err=>{
										if(err) callback(err);
										else sendForm(field.__, callback);
									});
								}else{
									sendForm(field.__, callback);
								}
							}
						}catch(e){
							console.log(e);
							callback({ err: 'Ошибка отображения полей (3)' });
						}});
					}else{
						callback({err: 'Отсутствует parentID'});
					}
				}catch(e){
					console.log(e);
					callback({ err: 'Ошибка отображения полей (2)' });
				}});
			}else{
				callback({err: 'Действие запрещено'});
			}
		}catch(e){
			console.log(e);
			callback({ err: 'Ошибка отображения полей (1)' });
		}});
	}
}

function deleteComplex(conn, deleteData, callback){
	
	if(deleteData.code){
		
		DB.session.get(conn.user.session, deleteData.code, (err, field)=>{try{
			
			DB.session.clearForm(conn, {complex: deleteData.code, form: field.form});

			DB.session.get(conn.user.session, field.parent, (err, parent)=>{try{

				field.code = deleteData.code;
				parent.code = field.parent;

				if(deleteData.sub || SYS.get(parent, 'controls.delete')){
				
					field.__ = function() {};
					field.__.db = conn.db;
					
					field.__.form = field.form;
					field.__.theme = field.theme;
							
					field.__.conn = conn;
					field.__.user = conn.user;

					var cache = SYS.requireFile("cache", field.form + (conn.user.role ? "_" + conn.user.role : "") + (field.theme?"_" + field.theme:''));
					
					field.__.process = cache.process;
					field.__.lst = cache.lst;
					field.links = field.__ && field.__.process[field.linecode].links;
					
					const subId = (field.__.user.subIds[field.code]||[]).shift();
					
					var deleteFunc = deleteData.sub !== true ?
					((typeof field.__.process[field.linecode].beforeDelete == 'function') ?
					(__, field, parent, cb)=>{ 
						const parents = [parent];
						field.__.process[field.linecode].beforeDelete(__, field, parents, (err)=>{
							if(err){
								cb(err);
							}else{
								DB.deleteComplex(__, field, parents, cb);
							}
						});
					} :
					DB.deleteComplex) :
					!subId ? 
					(__, field, parent, cb)=>{ cb({err: 'Не найден subId'}) } :
					(__, field, parent, cb)=>{ cb({}) };
					
					if(parent && parent._id){
						
						deleteFunc(field.__, field, parent, (err)=>{ callback(err || {status: 'ok'}) });
					
					}else{
						
						var p = parent.parent;
						
						DB.session.get(conn.user.session, p, (err, parent)=>{
							deleteFunc(field.__, field, parent, (err)=>{ callback(err || {status: 'ok'}) });
						});
					}
				}else{
					callback({err: 'Действие запрещено'});
				}
			}catch(e){
				console.log(e);
				callback({err: 'Ошибка удаления сущностей (2)'});
			}});
		}catch(e){
			console.log(e);
			callback({err: 'Ошибка удаления сущностей (1)'});
		}});
	}
}

function controlComplex(conn, controlData, callback){
		
	if(controlData.code){
		
		DB.session.get(conn.user.session, controlData.code, (err, field)=>{
				
			var cache = require(PROJECT_LINK+'/forms/cache/'+field.form+(conn.user.role?'_'+conn.user.role:'')+'.js');
			if(cache.process[field.linecode] && cache.process[field.linecode][controlData.type]){
				cache.process[field.linecode][controlData.type](conn, field, controlData, callback);
			}else{
				callback({err: 'Действие "'+controlData.type+'" не найдено'});
			}
		});
	}else{
		callback({err: 'Отсутствует идентификатор блока'});
	}
}